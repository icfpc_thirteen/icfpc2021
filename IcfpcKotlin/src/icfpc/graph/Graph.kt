package icfpc.graph

import icfpc.geom.EdgeI
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class Graph(val vertCnt: Int, val incidence: List<Set<Int>>, val distances: List<Map<Int, Double>>) {
    companion object {
        fun fromEdges(vertCnt: Int, edges: List<EdgeI>): Graph {
            val incidence: MutableList<MutableSet<Int>> = MutableList(vertCnt) { mutableSetOf<Int>() }
            val distances: MutableList<MutableMap<Int, Double>> = MutableList(vertCnt) { mutableMapOf<Int, Double>() }
            for (e in edges) {
                incidence[e.i1].add(e.i2)
                incidence[e.i2].add(e.i1)
                distances[e.i1][e.i2] = e.length;
                distances[e.i2][e.i1] = e.length;
            }
            return Graph(vertCnt, incidence, distances)
        }
    }

    fun topologicalOrder(start: Int): List<Int> {
        val res = mutableListOf<Int>(start)
        val q = ArrayDeque<Int>()
        q.add(start)
        val visited = BitSet(vertCnt)
        visited[start] = true
        while (!q.isEmpty()) {
            val cur = q.poll()
            for (n in incidence[cur]) {
                if (visited[n])
                    continue
                visited[n] = true
                res.add(n)
                q.add(n)
            }
        }

        return res
    }


    data class MinPath(val dist: Double, val path: List<Int>)

    val maxDistanceCache: List<List<MinPath>> by lazy { calcMaxDistanceCache() }

    fun getMaxDistance(i1: Int, i2: Int): Double = maxDistanceCache[i1][i2].dist;

    private fun calcMaxDistanceCache(): List<List<MinPath>> {
        val result = MutableList(vertCnt) { MutableList<MinPath?>(vertCnt) { null } }

        for (vert in 0 until vertCnt) {
            val current = result[vert]
            current[vert] = MinPath(0.0, listOf())
            dijkstra(vert, current)
        }
        return result.map { l -> l.map { it!! } }
    }

    private fun dijkstra(start: Int, result: MutableList<MinPath?>) {
        val pq = PriorityQueue<Node>()
        val calculated = BitSet(vertCnt)
        pq.add(Node(start, 0.0))
        while (!pq.isEmpty()) {
            val node = pq.remove()
            calculated.set(node.vertex)
            visitNeighbour(node.vertex, pq, calculated, result)
        }
    }

    private fun visitNeighbour(i1: Int, pq: PriorityQueue<Node>, calculated: BitSet, result: MutableList<MinPath?>) {
        val minPath = result[i1]!!
        for (adj in distances[i1]) {
            if (calculated[adj.key])
                continue
            val newDistance = minPath.dist + adj.value
            val tgtPath = result[adj.key]
            if (tgtPath == null || newDistance < tgtPath.dist) {
                val newPath = MinPath(newDistance, minPath.path + adj.key)
                result[adj.key] = newPath
                pq.add(Node(adj.key, newPath.dist))
            }
        }
    }

    class Node(val vertex: Int, val distance: Double) : Comparable<Node> {
        override fun compareTo(other: Node): Int = distance.compareTo(other.distance)
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    val flipDisconnectedCache: MutableMap<Pair<Int, Int>, FlipDir?> = HashMap()

    fun getFlipDisconnected(removeI1: Int, removeI2: Int, disabledPoints: HashSet<Int> = HashSet()): FlipDir? {
        val p = if (removeI1 < removeI2) Pair(removeI1, removeI2) else Pair(removeI2, removeI1)
        if (flipDisconnectedCache.containsKey(p))
            return flipDisconnectedCache[p]
        else {
            val res = calcFlipDisconnected(p.first, p.second, disabledPoints)
            flipDisconnectedCache[p] = res
            return res
        }
    }

    private fun calcFlipDisconnected(removeI1: Int, removeI2: Int, disabledPoints: HashSet<Int> = HashSet()): FlipDir? {
        val c1 = mutableListOf<Int>()
        val q = ArrayDeque<Int>()
        var first: Int = 0
        while (first == removeI1 || first == removeI2 || disabledPoints.contains(first))
            first++
        if (first >= vertCnt) {
            return null;
        }
        if (!disabledPoints.contains(first))
            c1.add(first)
        q.add(first)
        val visited = BitSet(vertCnt)
        visited[removeI1] = true
        visited[removeI2] = true
        visited[first] = true
        while (!q.isEmpty()) {
            val cur = q.poll()
            for (n in incidence[cur]) {
                if (disabledPoints.contains(n))
                    continue
                if (visited[n])
                    continue
                visited[n] = true
                c1.add(n)
                q.add(n)
            }
        }
        // still connected
        if (c1.size == vertCnt - 2)
            return null

        val c2 = mutableListOf<Int>()
        for (i in 0 until vertCnt) {
            if (!visited[i] && !disabledPoints.contains(i))
                c2.add(i)
        }
        return FlipDir(removeI1, removeI2, c1, c2)
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    val legs: List<Leg> by lazy { findAllLegs() }

    private fun findAllLegs(): List<Leg> {
        val legs = mutableListOf<Leg>()
        for (vi in 0 until vertCnt) {
            if (incidence[vi].size <= 2)
                continue
            val leg = checkLeg(vi)
            if (leg != null)
                legs.add(leg)
        }
        val nl = legs.filter { l ->
            val ols = legs.filter { it.startIndex in incidence[l.startIndex] }
            return@filter !ols.any { it.legVerts.size > l.legVerts.size && it.legVerts.containsAll(l.legVerts) }
        }

//        return legs
        return nl
    }


    private fun checkLeg(candidate: Int): Leg? {
        val c1 = mutableSetOf<Int>()
        val q = ArrayDeque<Int>()
        var first: Int = 0
        while (first == candidate)
            first++
        if (first >= vertCnt) {
            return null
        }
        q.add(first)
        c1.add(first)
        val visited = BitSet(vertCnt)
        visited[candidate] = true
        visited[first] = true
        while (q.isNotEmpty()) {
            val cur = q.poll()
            for (n in incidence[cur]) {
                if (visited[n])
                    continue
                visited[n] = true
                c1.add(n)
                q.add(n)
            }
        }
        // still connected
        if (c1.size == vertCnt - 1)
            return null

        val s2 = vertCnt - c1.size - 1
        if (c1.size < s2)
            return Leg(candidate, c1, c1.map { maxDistanceCache[candidate][it] }.maxBy { it.dist }!!)

        val c2 = mutableSetOf<Int>()
        for (i in 0 until vertCnt) {
            if (!visited[i])
                c2.add(i)
        }
        return Leg(candidate, c2, c2.map { maxDistanceCache[candidate][it] }.maxBy { it.dist }!!)
    }

}

data class Leg(val startIndex: Int, val legVerts: Set<Int>, val maxPath: Graph.MinPath) {
}

data class FlipDir(val i1: Int, val i2: Int, val c1: List<Int>, val c2: List<Int>) {
    fun switch(): FlipDir = FlipDir(i1, i2, c2, c1)
}