package icfpc

import icfpc.api.Api
import org.apache.hc.client5.http.config.RequestConfig
import org.apache.hc.client5.http.fluent.Request
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.ClassicHttpResponse
import org.apache.hc.core5.http.ContentType
import org.apache.hc.core5.http.HttpStatus
import org.apache.hc.core5.http.HttpVersion
import java.io.ByteArrayOutputStream
import java.io.File
import java.lang.StringBuilder
import java.math.BigInteger


fun sendHttpPost(fullServerUrl: String, body: String): String? {
    val request = Request.post(fullServerUrl)
        .useExpectContinue()
        .version(HttpVersion.HTTP_1_1)
        .bodyString(body, ContentType.TEXT_PLAIN)

    val requestConfig = RequestConfig.custom().setCircularRedirectsAllowed(true).build()
    val client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build()

    val response = request.useExpectContinue()
        .execute(client)
        .returnResponse()

    val status = response.code

    if (status != HttpStatus.SC_OK) {
        throw RuntimeException("Failed to send request. Status code: $status")
    }
    val out = ByteArrayOutputStream()
    (response as ClassicHttpResponse).getEntity().writeTo(out)
    val result = out.toString()
    out.close()

    return result
}


fun main(args: Array<String>) {
    val javaVersion = System.getProperty("java.version")
    println("Java version: $javaVersion")
    //    println(sendHttpPost("https://httpbin.org/post", "test"))

//    Api.hello()
//    KnownProblems.downloadAllProblems()
    KnownProblems.analyzeProblems()

}