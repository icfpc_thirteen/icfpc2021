package icfpc.api

data class JsonSolutionFigure(val vertices: List<List<Int>>)
data class JsonProblemFigure(val edges: List<List<Int>>, val vertices: List<List<Int>>, val synedges: List<List<Int>>?)
data class JsonProblemData(
    val hole: List<List<Int>>,
    val figure: JsonProblemFigure,
    val epsilon: Int,
    val bonuses: List<JsonProblemBonus>
)

data class JsonProblemBonus(val bonus: String, val problem: Int, val position: List<Int>) {
    companion object {
        const val GLOBALIST = "GLOBALIST"
        const val BREAK_A_LEG = "BREAK_A_LEG"
    }

}