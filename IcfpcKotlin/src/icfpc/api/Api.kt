package icfpc.api

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import icfpc.KnownProblems.getSolutionFileName
import icfpc.geom.*
import org.apache.hc.client5.http.config.RequestConfig
import org.apache.hc.client5.http.fluent.Request
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.*
import java.io.ByteArrayOutputStream
import java.io.File

object Api {
    private val ApiKey = "6f527204-8480-40ec-afad-119b4d9660b0"
    private val BaseServerUrl = "https://poses.live"

    val mapper = jacksonObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    val simpleWriter = mapper.writer()
    val prettyWriter = mapper.writerWithDefaultPrettyPrinter()

    private fun sendHttpPost(fullServerUrl: String, body: String): String {
        val request = Request.post(fullServerUrl)
            .useExpectContinue()
            .version(HttpVersion.HTTP_1_1)
            .setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + ApiKey)
            .bodyString(body, ContentType.TEXT_PLAIN)

        val requestConfig = RequestConfig.custom().setCircularRedirectsAllowed(true).build()
        val client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build()

        val response = request.useExpectContinue()
            .execute(client)
            .returnResponse()

        val status = response.code

        if (status != HttpStatus.SC_OK) {
            throw RuntimeException("Failed to send request. Status code: $status")
        }
        val out = ByteArrayOutputStream()
        (response as ClassicHttpResponse).getEntity().writeTo(out)
        val result = out.toString()
        out.close()

        return result
    }

    private fun sendHttpGet(fullServerUrl: String): String {
        val request = Request.get(fullServerUrl)
            .useExpectContinue()
            .version(HttpVersion.HTTP_1_1)
            .setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + ApiKey)


        val requestConfig = RequestConfig.custom().setCircularRedirectsAllowed(true).build()
        val client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build()

        val response = request.useExpectContinue()
            .execute(client)
            .returnResponse()

        val status = response.code

        if (status != HttpStatus.SC_OK) {
            throw RuntimeException("Failed to send request. Status code: $status")
        }
        val out = ByteArrayOutputStream()
        (response as ClassicHttpResponse).getEntity().writeTo(out)
        val result = out.toString()
        out.close()

        return result
    }

    fun hello() {
        val res = sendHttpGet(BaseServerUrl + "/api/hello")
        println(res)
    }

    fun getProblemRaw(id: Int): String {
        val jsonString = sendHttpGet(BaseServerUrl + "/api/problems/${id}")
//        println(jsonString)
        return jsonString
    }

    private fun parseProblemJson(id: Int, jsonString: String): Problem {
        val rawProblem: JsonProblemData = mapper.readValue(jsonString)
        return Problem.fromRawProblem(id, rawProblem)
    }

    fun getProblem(id: Int): Problem {
        val jsonString = getProblemRaw(id)
        return parseProblemJson(id, jsonString)
    }

    fun submitSolution(id: Int, solution: SolutionFigure) : Boolean {
        val json = solution.toJson()
        val file = File(getSolutionFileName(id, solution.dislikes))

        var skip = false;
        val lst = File(file.parent).list()
        if (lst != null) {
            for (fn in lst) {
                if (fn.startsWith("sol${id}_") && fn.endsWith(".json")) {
                    var existingScore = fn.substring("sol${id}_".length);
                    existingScore = existingScore.substring(0, existingScore.length - 5); // remove .json
                    if (Integer.parseInt(existingScore) < solution.dislikes) {
                        println("Not sending dislikes=${solution.dislikes}, because found locally dislikes=${existingScore} or smaller")
                        skip = true;
                        break;
                    }
                }
            }
        }
        file.writeText(json);
        if (!skip) {
            submitSolution(id, solution.dislikes, json)
            try {
                Runtime.getRuntime().exec("git add ${file.parent}/*.json");
            } catch (ex: Throwable) {

            }

        }
        return true
    }

    private fun submitSolution(problemId: Int, dislikes: Int, json: String) {
        val resp = sendHttpPost(BaseServerUrl + "/api/problems/${problemId}/solutions", json)
        println("Solution $problemId (dislikes=$dislikes): response is '${resp}'")
    }

    fun forceSubmitSolutionFromFile(problemId: Int, dislikes: Int) {
        val file = File(getSolutionFileName(problemId, dislikes));
        val json = file.readText();
        println("Force submitting solution from file ${file.name}")
        println("JSON: $json")
        submitSolution(problemId, dislikes, json)
    }
}