package icfpc

import com.fasterxml.jackson.module.kotlin.readValue
import icfpc.api.Api
import icfpc.api.JsonProblemData
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.sg.ChainImprover
import java.io.File
import kotlin.math.roundToInt

object KnownProblems {
    val oldMaxId1 = 59
    val oldMaxId2 = 78
    val oldMaxId3 = 88
    val oldMaxId4 = 106
    val maxId = 132

    val basePath = "./data/problems/"

//    val baseSolutionPath = "./data/solutions/"
    val baseSolutionPath = "./data/solutions_sg/"

    val bestSolutionStats = listOf<BestSolutionInfo>(
        BestSolutionInfo(1, 1107, 624),
        BestSolutionInfo(2, 751, 136),
        BestSolutionInfo(3, 1035, 199),
        BestSolutionInfo(4, 399, 0),
        BestSolutionInfo(5, 326, 4),
        BestSolutionInfo(6, 9781, 2655),
        BestSolutionInfo(7, Int.MAX_VALUE, 0),
        BestSolutionInfo(8, 1385, 892),
        BestSolutionInfo(9, 2759, 0),
        BestSolutionInfo(10, 510, 7),
        BestSolutionInfo(11, 0, 0),
        BestSolutionInfo(12, 0, 0),
        BestSolutionInfo(13, 0, 0),
        BestSolutionInfo(14, 116, 116),
        BestSolutionInfo(15, 0, 0),
        BestSolutionInfo(16, 0, 0),
        BestSolutionInfo(17, 0, 0),
        BestSolutionInfo(18, 0, 0),
        BestSolutionInfo(19, 5318, 5318),
        BestSolutionInfo(20, 0, 0),
        BestSolutionInfo(21, 0, 0),
        BestSolutionInfo(22, 0, 0),
        BestSolutionInfo(23, 0, 0),
        BestSolutionInfo(24, 0, 0),
        BestSolutionInfo(25, 0, 0),
        BestSolutionInfo(26, 0, 0),
        BestSolutionInfo(27, 2637, 2076),
        BestSolutionInfo(28, 3816, 3216),
        BestSolutionInfo(29, 3361, 3114),
        BestSolutionInfo(30, 1837, 1837),
        BestSolutionInfo(31, 10457, 3533),
        BestSolutionInfo(32, 1894, 1511),
        BestSolutionInfo(33, 2362, 2362),
        BestSolutionInfo(34, 0, 0),
        BestSolutionInfo(35, 0, 0),
        BestSolutionInfo(36, 1444, 1279),
        BestSolutionInfo(37, 2613, 1674),
        BestSolutionInfo(38, 0, 0),
        BestSolutionInfo(39, 0, 0),
        BestSolutionInfo(40, 3768, 3768),
        BestSolutionInfo(41, 0, 0),
        BestSolutionInfo(42, 1705, 1655),
        BestSolutionInfo(43, 0, 0),
        BestSolutionInfo(44, 10941, 8706),
        BestSolutionInfo(45, 6429, 6379),
        BestSolutionInfo(46, 0, 0),
        BestSolutionInfo(47, 0, 0),
        BestSolutionInfo(48, 4333, 3040),
        BestSolutionInfo(49, 0, 0),
        BestSolutionInfo(50, 3682, 3299),
        BestSolutionInfo(51, Int.MAX_VALUE, 0),
        BestSolutionInfo(52, 0, 0),
        BestSolutionInfo(53, 3041, 0),
        BestSolutionInfo(54, Int.MAX_VALUE, 0),
        BestSolutionInfo(55, 0, 0),
        BestSolutionInfo(56, 3426, 3021),
        BestSolutionInfo(57, Int.MAX_VALUE, 4293),
        BestSolutionInfo(58, 4140, 2171),
        BestSolutionInfo(59, 0, 0),
        BestSolutionInfo(60, 1261, 73),
        BestSolutionInfo(61, 18609, 12587),
        BestSolutionInfo(62, 29353, 2865),
        BestSolutionInfo(63, Int.MAX_VALUE, 0),
        BestSolutionInfo(64, 145599, 0),
        BestSolutionInfo(65, Int.MAX_VALUE, 0),
        BestSolutionInfo(66, 9071, 7585),
        BestSolutionInfo(67, 0, 0),
        BestSolutionInfo(68, 240770, 0),
        BestSolutionInfo(69, 2938, 705),
        BestSolutionInfo(70, Int.MAX_VALUE, 0),
        BestSolutionInfo(71, 154480, 5230),
        BestSolutionInfo(72, 2306, 0),
        BestSolutionInfo(73, 476, 0),
        BestSolutionInfo(74, 167068, 2383),
        BestSolutionInfo(75, 303346, 0),
        BestSolutionInfo(76, 6892, 0),
        BestSolutionInfo(77, Int.MAX_VALUE, 0),
        BestSolutionInfo(78, 44641, 149),
        BestSolutionInfo(79, Int.MAX_VALUE, 21269),
        BestSolutionInfo(80, Int.MAX_VALUE, 0),
        BestSolutionInfo(81, Int.MAX_VALUE, 0),
        BestSolutionInfo(82, Int.MAX_VALUE, 1122),
        BestSolutionInfo(83, Int.MAX_VALUE, 3349),
        BestSolutionInfo(84, 1947, 0),
        BestSolutionInfo(85, Int.MAX_VALUE, 0),
        BestSolutionInfo(86, Int.MAX_VALUE, 5354),
        BestSolutionInfo(87, 27705, 2686),
        BestSolutionInfo(88, 11445, 9908),
        BestSolutionInfo(89, 97749, 82795),
        BestSolutionInfo(90, 2027966, 0),
        BestSolutionInfo(91, 26665, 12777),
        BestSolutionInfo(92, 59831, 33754),
        BestSolutionInfo(93, Int.MAX_VALUE, 0),
        BestSolutionInfo(94, 18746, 1506),
        BestSolutionInfo(95, 40383, 5281),
        BestSolutionInfo(96, Int.MAX_VALUE, 13649),
        BestSolutionInfo(97, Int.MAX_VALUE, 0),
        BestSolutionInfo(98, 49431, 28409),
        BestSolutionInfo(99, Int.MAX_VALUE, 5803),
        BestSolutionInfo(100, 43361, 3425),
        BestSolutionInfo(101, Int.MAX_VALUE, 7581),
        BestSolutionInfo(102, 112962, 59868),
        BestSolutionInfo(103, Int.MAX_VALUE, 47952),
        BestSolutionInfo(104, 44923, 14650),
        BestSolutionInfo(105, 2429515, 0),
        BestSolutionInfo(106, 1489113, 0),
        BestSolutionInfo(107, 90062, 1315),
        BestSolutionInfo(108, 32340, 18274),
        BestSolutionInfo(109, Int.MAX_VALUE, 53),
        BestSolutionInfo(110, 25100, 11833),
        BestSolutionInfo(111, Int.MAX_VALUE, 15564),
        BestSolutionInfo(112, 46102, 19511),
        BestSolutionInfo(113, Int.MAX_VALUE, 9360),
        BestSolutionInfo(114, 1829189, 0),
        BestSolutionInfo(115, 85787, 36780),
        BestSolutionInfo(116, 60964, 4099),
        BestSolutionInfo(117, Int.MAX_VALUE, 28703),
        BestSolutionInfo(118, 34063, 3781),
        BestSolutionInfo(119, 40293, 18972),
        BestSolutionInfo(120, 50233, 9926),
        BestSolutionInfo(121, 28506, 11879),
        BestSolutionInfo(122, 123210, 0),
        BestSolutionInfo(123, 20784, 13035),
        BestSolutionInfo(124, Int.MAX_VALUE, 13548),
        BestSolutionInfo(125, 590384, 0),
        BestSolutionInfo(126, 293518, 2141),
        BestSolutionInfo(127, 19710, 16597),
        BestSolutionInfo(128, 66520, 9833),
        BestSolutionInfo(129, 29225, 7992),
        BestSolutionInfo(130, Int.MAX_VALUE, 6470),
        BestSolutionInfo(131, Int.MAX_VALUE, 15748),
        BestSolutionInfo(132, Int.MAX_VALUE, 14517),
    )


    private fun getProblemFileName(id: Int) = basePath + "${id}.json"
    fun getSolutionFileName(id: Int) = baseSolutionPath + "sol${id}.json"
    fun getSolutionFileName(id: Int, dislikes: Int) = baseSolutionPath + "sol${id}_${dislikes}.json"

    data class KnownSolution(val problemId: Int, val solFile: File, val dislikes: Int)

    fun getBestKnownSolutionFile(id: Int): KnownSolution? {
        val lst = File(baseSolutionPath).list() ?: return null
        var bestSolution: KnownSolution? = null
        var bestDislikes = Int.MAX_VALUE
        for (fn in lst) {
            if (fn.startsWith("sol${id}_") && fn.endsWith(".json")) {
                var existingScoreString = fn.substring("sol${id}_".length);
                existingScoreString = existingScoreString.substring(0, existingScoreString.length - 5); // remove .json
                val dislikes = existingScoreString.toInt()
                if (dislikes < bestDislikes) {
                    bestSolution = KnownSolution(id, File(baseSolutionPath + fn), dislikes)
                    bestDislikes = dislikes
                }
            }
        }
        return bestSolution
    }

    fun downloadAllProblems() {
        for (i in 1..maxId) {
            val json = Api.getProblemRaw(i)
            val file = File(getProblemFileName(i))
//            file.writeText(json)
            val rawProblem = Api.mapper.readValue<JsonProblemData>(json)
            val prettyJson = Api.prettyWriter.writeValueAsString(rawProblem)
            file.writeText(prettyJson)
        }
    }

    fun loadLocalProblem(id: Int): Problem {
        val file = File(getProblemFileName(id))
        val json = file.readText()
        val problem = Problem.fromJson(id, json)
        return problem
    }

    fun analyzeProblems() {
        var totalScore = 0L
        for (i in 1..maxId) {
            val problem = loadLocalProblem(i)
            val stats = bestSolutionStats[i - 1]
            val bestFile = getBestKnownSolutionFile(i);
            val ourScore = SolutionFigure.calcScore(problem, stats.ourBest, stats.worldBest)
            val bestScore = SolutionFigure.calcScore(problem, stats.worldBest, stats.worldBest)
            totalScore += Math.ceil(ourScore).toLong()
            println(
                "#${problem.problemId} eps ${problem.epsilon} hole_cnt ${problem.hole.vertices.size} fig_verts ${problem.figure.vertices.size} fig_edges ${problem.figure.edgeIndices.size}" +
                        " |stats: ob ${stats.ourBest} wb ${stats.worldBest}, os $ourScore, bs $bestScore ratio ${(bestScore / ourScore * 1000).roundToInt()}"
            )
            if ((bestFile != null) && (bestFile.dislikes != stats.ourBest)) {
                println("!!!! different best solution for $i: file ${bestFile.dislikes} != ${stats.ourBest} from stats")
            }
        }
        println("Our total score is $totalScore")
    }

    fun forEachKnownProblemIn(range: IntRange, fn: (Problem, BestSolutionInfo) -> Unit) {
        for (i in range) {
            val problem = loadLocalProblem(i)
            val stats = bestSolutionStats[i - 1]
            fn(problem, stats)
        }
    }

    fun forEachKnownProblem(fn: (Problem, BestSolutionInfo) -> Unit) {
        forEachKnownProblemIn(1..maxId, fn)
    }

    fun forEachKnownProblemDown(fn: (Problem, BestSolutionInfo) -> Unit) {
        var i = KnownProblems.maxId
        while (i > 0) {
            val problem = KnownProblems.loadLocalProblem(i)
            val stats = KnownProblems.bestSolutionStats[i - 1]
            fn(problem, stats)
            i--
        }
    }
}


data class BestSolutionInfo(val id: Int, val ourBest: Int, val worldBest: Int) {
    fun isOurPerfect() = ourBest == 0
}