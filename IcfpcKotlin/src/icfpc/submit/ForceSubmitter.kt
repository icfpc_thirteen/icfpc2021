package icfpc.submit

import icfpc.api.Api
import java.lang.RuntimeException

fun main(args: Array<String>) {
    throw RuntimeException("Comment out and change problemId and dislikes to force submit")
    val problemId = 42
    val dislikes = 2197
    Api.forceSubmitSolutionFromFile(problemId, dislikes)
}