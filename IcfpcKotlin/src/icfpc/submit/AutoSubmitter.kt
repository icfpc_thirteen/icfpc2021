package icfpc.submit

import java.io.File

var problemDislikeMap = emptyMap<Int, Int>().toSortedMap()

fun main(args: Array<String>) {

    val MAX_PROBLEM_ID = 132

    val baseSolutionPath = "./data/solutions/"

    (1..MAX_PROBLEM_ID).forEach { problemDislikeMap[it] = -1 }

    val allSolutions = File(baseSolutionPath).list().filter { it.contains('_') }
    println("Found ${allSolutions.size} solutions")

    val solvedProblemIds = allSolutions.map { it.substringBefore('_').substringAfter("sol") }
            .map(Integer::parseInt)
            .distinct()
            .sorted()

    println("Found ${solvedProblemIds.size} solved problems")

    solvedProblemIds.forEach {
        processSolvedProblem(it, allSolutions)
    }

    val unsolvedProblemIds = (1..MAX_PROBLEM_ID).minus(solvedProblemIds)

    println()
    println("Found ${unsolvedProblemIds.size} unsolved problems")
    unsolvedProblemIds.forEach(::println)

    println()
    println("Best dislikes")
    problemDislikeMap.entries.map { println("${it.key}, ${it.value}")}
}

private fun processSolvedProblem(problemId: Int, allSolutions: List<String>) {
    println()
    val problemSolutions = allSolutions.filter { it.startsWith("sol" + problemId + '_') }
    println("Found ${problemSolutions.size} solutions for problem $problemId")

    val problemDislikes = problemSolutions.map { it.substringAfter('_').substringBefore('.') }
            .map(Integer::parseInt)

    val bestDislikes = problemDislikes.distinct().minOrNull()!!

    problemDislikeMap[problemId] = bestDislikes

    val bestSolutionName = "sol_${problemId}_$bestDislikes.json"

    println("Found best solution $bestSolutionName for problem $problemId")
    //        Api.forceSubmitSolutionFromFile(problemId, bestDislikes)
}


