package icfpc

import icfpc.geom.EdgeP
import icfpc.geom.IntPoint
import kotlin.math.sign

class Polygon {
    var vertices: ArrayList<IntPoint> = ArrayList()

    fun contains_without_edge_cases(p: IntPoint): Boolean {
        val box = boundingBox()
        if (p.x < box.first.x || p.y < box.first.y || p.x > box.second.x || p.y > box.second.y) {
            return false;
        }
        val outsidePoint = IntPoint(box.second.x + 1, p.y)
        val ray = EdgeP(p, outsidePoint)
        var intersections = 0
        for (i in 0..vertices.size-1) {
            val nexti = (i + 1) % vertices.size
            val edge = EdgeP(vertices[i], vertices[nexti])
            if (ray.intersectsWith(edge)) {
                intersections += 1
            }
        }
        return (intersections % 2 != 0)
    }

    fun contains(p: IntPoint): Boolean {
        val box = boundingBox()
        if (p.x < box.first.x || p.y < box.first.y || p.x > box.second.x || p.y > box.second.y) {
            return false;
        }
        var s = check(vertices[0], vertices[vertices.size-1], p)
        for (i in 0..vertices.size-2) {
            s *= check(vertices[i], vertices[i+1], p)
        }
        return s <= 0
    }

    fun check(a: IntPoint, b: IntPoint, middle: IntPoint): Int {
        val ax = a.x - middle.x
        val ay = a.y - middle.y
        val bx = b.x - middle.x
        val by = b.y - middle.y
        val s = (ax * by - ay * bx).sign
        if (s == 0 && (ay == 0 || by == 0) && ax * bx <= 0) return 0
        if ((ay < 0) != (by < 0)) {
            if (by < 0)
                return s
            return -s
        }
        return 1
    }
    fun boundingBox(): Pair<IntPoint, IntPoint> {
        var xMin = vertices[0].x
        var xMax = vertices[0].x
        var yMin = vertices[0].y
        var yMax = vertices[0].y
        for (v in vertices) {
            if (xMin > v.x) {
                xMin = v.x
            }
            if (xMax < v.x) {
                xMax = v.x
            }
            if (yMin > v.y) {
                yMin = v.y
            }
            if (yMax < v.y) {
                yMax = v.y
            }
        }
        return Pair(IntPoint(xMin, yMin), IntPoint(xMax, yMax))
    }
}

fun main(args: Array<String>) {
//    val edge = EdgeP(IntPoint(0, 0), IntPoint(2, 2))
//    println(edge.isInside(0.0, 0.0))
//    println(edge.isInside(1.0, 1.0))
//    println(edge.isInside(2.0, 2.0))
//    println(edge.isInside(3.0, 3.0))
//    println(edge.isInside(0.5, 0.5))
//    var p = Polygon()
//    p.vertices.add(IntPoint(0, 0))
//    p.vertices.add(IntPoint(0, 2))
//    p.vertices.add(IntPoint(2, 2))
//    p.vertices.add(IntPoint(2, 0))
//    println(p.contains(IntPoint(0, 0)))
//    println(p.contains(IntPoint(1, 1)))
//    println(p.contains(IntPoint(1, 0)))
//    println(p.contains(IntPoint(0, 1)))
//    println(p.contains(IntPoint(2, 2)))
//    println(p.contains(IntPoint(2, 0)))
//    println(p.contains(IntPoint(0, 2)))
//    println(p.contains(IntPoint(3, 3)))
//    println(p.contains(IntPoint(3, 0)))
//    println(p.contains(IntPoint(0, 3)))
    var p = Polygon()
    p.vertices.add(IntPoint(0, 0))
    p.vertices.add(IntPoint(4, 8))
    p.vertices.add(IntPoint(8, 0))
    p.vertices.add(IntPoint(4, 2))
    println(p.contains(IntPoint(0, 0))) // true
    println(p.contains(IntPoint(1, 1))) // true
    println(p.contains(IntPoint(1, 0))) // false
    println(p.contains(IntPoint(2, 2))) // true
    println(p.contains(IntPoint(2, 3))) // true
    println(p.contains(IntPoint(2, 0))) // false
    println(p.contains(IntPoint(0, 2))) // false
    println(p.contains(IntPoint(4, 2))) // true
    println(p.contains(IntPoint(3, 0))) // false
    println(p.contains(IntPoint(4, 1))) // false
}

