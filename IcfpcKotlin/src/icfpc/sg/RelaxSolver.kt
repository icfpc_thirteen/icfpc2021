package icfpc.sg

import icfpc.geom.IntPoint
import icfpc.geom.SolutionFigure
import java.util.*
import kotlin.collections.ArrayList

data class IndexPoint(val index: Int, val p: IntPoint)

class RelaxSolver(
    val sol: SolutionFigure,
    val srcPoints: List<IndexPoint>,
    val maxIters: Int = 50_000,
    val allowBad: Boolean = false
) {
    companion object {
        val shifts0 = listOf(IntPoint(1, 0), IntPoint(-1, 0), IntPoint(0, 1), IntPoint(0, -1))
        val shifts = shifts0.map { Pair(it, IntPoint(-it.x, -it.y)) }.toList()

    }

    data class QueueEl(val sol: List<IndexPoint>, val estimate: Long) {
        lateinit var from: IntPoint
    }

    private val q = PriorityQueue<QueueEl>(compareBy { it.estimate })

    fun solve(): List<IndexPoint>? {
        q.add(createQuEl(srcPoints, IntPoint(0, 0)))
        var best = q.peek()
        if (allowBad)
            println("RelaxSolver original estimate of $maxIters, best = ${best.estimate}")
        var cnt = 0
        while (!q.isEmpty()) {
            cnt++
            if (cnt > maxIters)
                break
            if (allowBad && (cnt % 10_000 == 0))
                println("RelaxSolver: $cnt of $maxIters, best = ${best.estimate}")
            val cur = q.poll()
            if (cur.estimate == 0L)
                return cur.sol
            if (cur.estimate < best.estimate)
                best = cur
            for (i in cur.sol.indices) {
                tryMoveVertex(cur, i)
            }
        }
        if (allowBad) {
            println("Returning bad, best error = ${best.estimate}")
            return best.sol
        } else
            return null
    }

    private fun tryMoveVertex(el: QueueEl, pos: Int) {
        val baseSol = el.sol
        val p0 = baseSol[pos]
        for ((sh, revSh) in shifts) {
            if (el.from == revSh)
                continue
            val mp = ArrayList(baseSol)
            val np = IndexPoint(p0.index, p0.p + sh)
            mp[pos] = np
            q.offer(createQuEl(mp, sh))
        }
    }


    private fun createQuEl(points: List<IndexPoint>, from: IntPoint): QueueEl {
        val el = QueueEl(points, estimate(points))
        el.from = from
        return el
    }

    private fun estimate(points: List<IndexPoint>): Long {
        val problem = sol.problem
        val figure = problem.figure
        val graph = figure.graph
        val map = points.map { Pair(it.index, it.p) }.toMap()
        val hole = problem.hole
        var sum = 0L
        for (ip in points) {
            val p = ip.p
            if (!hole.contains(p))
                sum += 100500
            for (oi in graph.incidence[ip.index]) {
                val newOtherPoint = map.getOrDefault(oi, sol.vertices[oi])
                val nd = p.squaredDistTo(newOtherPoint)
                val od = figure.getEdgeSqrLen(ip.index, oi)
                sum += problem.calcEdgeError(nd, od)
                val middlePoint = IntPoint((newOtherPoint.x + p.x) / 2, (newOtherPoint.y + p.y) / 2)
                if (!hole.contains(middlePoint))
                    sum += 10500
            }
        }
        return sum
    }
}