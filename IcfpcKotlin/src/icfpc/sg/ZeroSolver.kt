package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.EdgeI
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import java.util.*
import kotlin.math.abs

class ZeroSolver(
    val problem: Problem,
    val tgtDislikes: Int = 0,
    val exitAfterAttempts: Int = 130,
    val force0: Boolean = false
) {
    var best: SolutionFigure? = null
    private val startTime = System.currentTimeMillis()
    private val timeout = 3 * 60 * 1000L

    private fun isTimeout(): Boolean {
        val lb = best
        if ((lb != null) && (lb.dislikes == tgtDislikes)) {
            println("Exit ${problem.problemId} by best")
            return true
        }

        val timeout = (System.currentTimeMillis() - startTime) > timeout
        if (timeout) {
            println("Exit ${problem.problemId} by timeout")
        }
        return timeout
    }

    private fun checkBetter(pis: SolutionFigure) {
        if (pis.validate(true).isNotEmpty()) {
            println("Tried to check a bad solution")
            return
        }
        val lb = best
        if ((lb == null) || (pis.dislikes < lb.dislikes)) {
            best = pis
            println("Best sol dis=${pis.dislikes}")
            pis.saveToProgressFile()
        }
    }

    fun solve(): SolutionFigure? {
        val vertsCnt = problem.figure.vertices.size
        val holeCnt = problem.hole.vertices.size
        if ((vertsCnt > 20) || (holeCnt > 30)) {
            println("Zero solver: skip too big problem ${problem.problemId}, vertsCnt=$vertsCnt, holeCnt=$holeCnt")
            return null
        }
        println("Zero solver: solving ${problem.problemId}, vertsCnt=$vertsCnt, holeCnt=$holeCnt")

//        val visitOrder = List(vertsCnt) { it }
        val visitOrder = problem.figure.graph.topologicalOrder(0)
        val recData = RecData(
            visitOrder, MutableList(vertsCnt) { null }, BitSet(holeCnt), 0, 0, mutableListOf(),
            mapOf<Int, Int>(
                Pair(0, 1),
                Pair(1, 2),
                Pair(6, 4),
                Pair(11, 5),
                Pair(12, 6),
                Pair(9, 7),
                Pair(8, 8),
                Pair(5, 9),
                Pair(2, 10),
            )
        )
        recAssignHole(recData, 0)
        return best
    }

    private fun checkEdges(recData: RecData, pos: Int): Boolean {
        val np1 = recData.fixedVerts[pos]!!
        val op1 = problem.figure.vertices[pos]
        val allowed = problem.epsilon / Problem.EpsDiv
        for (i2 in problem.figure.graph.incidence[pos]) {
            val np2 = recData.fixedVerts[i2] ?: continue

            val op2 = problem.figure.vertices[i2]
            val od = op1.squaredDistTo(op2)
            val nd = np1.squaredDistTo(np2)
            val sdiff = nd - od
            val diff = abs(sdiff)
            if (diff.toDouble() / od > allowed)
                return false
            if (!problem.checkNewEdge(np1, np2))
                return false
        }
        return true
    }

    private fun recAssignHole(recData: RecData, recPos: Int) {
//        println("recAssignHole at $pos: $recData")
        if (isTimeout())
            return
        val holeVertices = problem.hole.vertices
        if ((recData.usedHolesCnt == holeVertices.size) ||
            (recPos == recData.fixedVerts.size)
        ) {
            val luhc = recData.usedHolesCnt
//            if (luhc == holeVertices.size) {
//                println("${problem.problemId} good potential solution ${recData.fixedVerts}")
//            }

            if (force0) {
                // only try potentially 0-solutions
                if (luhc == holeVertices.size)
                    recFillOtherVerts(recData, 0)
            } else {
                if (recData.usedVertsCnt >= 2)
                    recFillOtherVerts(recData, 0)
            }
            return
        }

        val forceHoleI = recData.forcedBinds.getOrDefault(recPos, -1)
        val tgtIndex = recData.visitOrder[recPos]
        for (hi in holeVertices.indices) {
            if (forceHoleI != -1 && hi != forceHoleI)
                continue
            if (recData.usedHoles[hi])
                continue
            recData.fixedVerts[tgtIndex] = holeVertices[hi]
            if (!checkEdges(recData, tgtIndex)) {
                recData.fixedVerts[tgtIndex] = null
                continue
            }

            recData.usedHoles[hi] = true
            recData.usedHolesCnt += 1
            recData.usedVertsCnt += 1
            recData.history.add(tgtIndex)

            recAssignHole(recData, recPos + 1)

            recData.history.removeLast()
            recData.fixedVerts[tgtIndex] = null
            recData.usedVertsCnt -= 1
            recData.usedHolesCnt -= 1
            recData.usedHoles[hi] = false
        }
//        if (recData.fixedVerts[tgtIndex] != null)
//            throw RuntimeException("AAAAAAAAAAAAAA")
        // skip
        recAssignHole(recData, recPos + 1)
    }

    private fun recFillOtherVerts(recData: RecData, pos: Int) {
//        if (pos == 0)
//            println("Try fill rest in ${recData}")
        if (isTimeout())
            return
        if (pos == recData.fixedVerts.size) {
            // found a solution!
            val sol = SolutionFigure(problem, recData.fixedVerts.map { it!! })
            checkBetter(sol)
            return
        }
        if (recData.fixedVerts[pos] != null) {
            recFillOtherVerts(recData, pos + 1)
            return
        }
        var cnt = 0
        recData.history.add(pos)
        for (p in problem.hole.containsCache.vertices) {
            recData.fixedVerts[pos] = p
            if (checkEdges(recData, pos)) {
                recFillOtherVerts(recData, pos + 1)
                cnt += 1
                if (cnt >= exitAfterAttempts)
                    break
            }
        }
        recData.fixedVerts[pos] = null
        recData.history.removeLast()
    }

    data class RecData(
        val visitOrder: List<Int>,
        val fixedVerts: MutableList<IntPoint?>,
        val usedHoles: BitSet,
        var usedVertsCnt: Int,
        var usedHolesCnt: Int,
        val history: MutableList<Int>,
        val forcedBinds: Map<Int, Int>
    ) {
    }

}

fun solveProblemZero(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
    if (!force) {
        if (stats.isOurPerfect()) {
            println("Skip ${problem.problemId}/${stats.id} because we already have a perfect solution")
            return
        }
        if (stats.ourBest == stats.worldBest)
            return
//        if (problem.problemId <= 5)
//            return
    }
    val solver = if (stats.worldBest == 0)
        ZeroSolver(problem, stats.worldBest, 100, true)
    else
        ZeroSolver(problem, stats.worldBest, 3, false)
    val ans = solver.solve()
    println("zero solver for ${problem.problemId}, ${ans?.dislikes}, ob=${stats.ourBest}/wb=${stats.worldBest}: ${ans}")
    if (ans != null) {
        ans.saveToFile(KnownProblems.getSolutionFileName(problem.problemId, ans.dislikes))
        try {
            if (ans.dislikes < stats.ourBest) {
                println("!!! Submitting ${problem.problemId}: ${ans.dislikes} > ob=${stats.ourBest}")
                Api.submitSolution(problem.problemId, ans)
            }
        } catch (ex: Exception) {
            println("Failed to upload ${problem.problemId}: ${ex}")
        }
    }
}

fun main(args: Array<String>) {
    if (1 == 1) {
//        val problemId = 2
//        val problemId = 46
//        val problemId = 84
//        val problemId = 49
//        val problemId = 51
//        val problemId = 59
//        val problemId = 54
//        val problemId = 67
//        val problemId = 84
        val problemId = 51
        val problem = KnownProblems.loadLocalProblem(problemId)
        solveProblemZero(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
        KnownProblems.forEachKnownProblem { problem, stats -> solveProblemZero(problem, stats, false) }
//        KnownProblems.forEachKnownProblemIn(KnownProblems.oldMaxId2 + 1..KnownProblems.maxId) { problem, stats ->
//            solveProblemZero(
//                problem,
//                stats,
//                false
//            )
//        }
    }
}
