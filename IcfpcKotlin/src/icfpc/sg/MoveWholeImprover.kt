package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.geom.Box
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import java.lang.Integer.max
import java.lang.Integer.min

class MoveWholeImprover(origSolution: SolutionFigure, tgtDislikes: Int = 0) : BaseImprover(origSolution, tgtDislikes) {
    companion object {
        fun improve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
            improveEx(problem, stats, force, submit = true) { sol -> MoveWholeImprover(sol) }
        }
    }

    override val timeout = 3 * 60 * 1000L

    override val requireInitialValid: Boolean = false

    override fun improveImpl() {
        val rots = origSolution.genRotations(true).toList()
        for (bs in rots)
            tryFit(bs)
    }

    fun tryFit(baseSol: SolutionFigure) {
//        val range = 500
//        val xRange = -range..range
//        val yRange = -range..range

        val solBox = Box.fromVertices(baseSol.vertices)
        val holeBox = problem.hole.box
        val dxMin = holeBox.xMin - solBox.xMin
        val dxMax = holeBox.xMax - solBox.xMax
        val dyMin = holeBox.yMin - solBox.yMin
        val dyMax = holeBox.yMax - solBox.yMax
        val xRange = min(dxMin, dxMax)..max(dxMin, dxMax)
        val yRange = min(dyMin, dyMax)..max(dyMin, dyMax)
        println("#${problem.problemId} fitting with range ${(xRange.last - xRange.first) * (yRange.last - yRange.first)} = $xRange x $yRange")

        for (dx in xRange) {
            if (isTimeout())
                break
            for (dy in yRange) {
                val newSol = baseSol.moveBy(dx, dy)
                if (newSol.fastValid()) {
                    checkBetter(newSol, false)
                }
            }
        }

    }
}


fun main(args: Array<String>) {
    if (1 == 1) {
        val problemId = 5
        val problem = KnownProblems.loadLocalProblem(problemId)
        MoveWholeImprover.improve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
        KnownProblems.forEachKnownProblem { problem, stats -> MoveWholeImprover.improve(problem, stats, false) }
//        KnownProblems.forEachKnownProblemIn(KnownProblems.oldMaxId2 + 1..KnownProblems.maxId) { problem, stats ->
//            MoveImprover.improve(
//                problem,
//                stats,
//                false
//            )
//        }
    }
}
