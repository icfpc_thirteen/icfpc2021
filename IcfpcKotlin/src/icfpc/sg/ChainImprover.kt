package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure

class ChainImprover(
    origSolution: SolutionFigure,
    tgtDislikes: Int = 0,
    val impFacts: List<(SolutionFigure) -> BaseImprover>
) :
    BaseImprover(origSolution, tgtDislikes) {
    companion object {
        val defaultChain: List<(SolutionFigure) -> BaseImprover> = listOf(
            { sol -> MoveWholeImprover(sol) },
            { sol -> LegsImprover(sol) },
            { sol -> EarsImprover(sol) },
            { sol -> LegsImprover(sol) },
            { sol -> MoveWholeImprover(sol) },
            { sol -> MoveVertexImprover(sol) },
        )

        fun improve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
            improveEx(problem, stats, force, submit = true) { sol -> ChainImprover(sol, impFacts = defaultChain) }
//            improveEx(problem, stats, force, submit = false) { sol -> ChainImprover(sol, impFacts = defaultChain) }
        }
    }

    override val timeout = 5 * 60 * 1000L
    override val requireInitialValid: Boolean = impFacts.map { it(origSolution).requireInitialValid }.any { it == true }

    override fun improveImpl() {
        var curSol = origSolution
        for (impFact in impFacts) {
            val imp = impFact(curSol)
            println("#${problem.problemId} starting child ${imp.javaClass.simpleName}")
            imp.improve()
            val next = imp.best
            if (next != null) {
                if (next.dislikes < curSol.dislikes) {
                    println("#${problem.problemId} child ${imp.javaClass.simpleName} improved to ${next.dislikes} from ${curSol.dislikes}")
                    checkBetter(next)
                    curSol = next
                } else
                    println("#${problem.problemId} child ${imp.javaClass.simpleName} has not improved ${curSol.dislikes}")
            }
        }
    }


}


fun main(args: Array<String>) {
    if (1 == 1) {
//        val problemId = 1
        val problemId = 122
        val problem = KnownProblems.loadLocalProblem(problemId)
        val legs = problem.figure.graph.legs
        println("${legs.size}: $legs")
        ChainImprover.improve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
//        KnownProblems.forEachKnownProblem { problem, stats -> ChainImprover.improve(problem, stats, false) }

//        KnownProblems.forEachKnownProblemIn(1..60) { problem, stats -> ChainImprover.improve(problem, stats, false) }
//        KnownProblems.forEachKnownProblemIn(61..100) { problem, stats -> ChainImprover.improve(problem, stats, false) }

//    KnownProblems.forEachKnownProblemIn(104..KnownProblems.maxId) { problem, stats ->
//            ChainImprover.improve(
//                problem,
//                stats,
//                false
//            )
//        }

//        KnownProblems.forEachKnownProblemDown { problem, stats ->
//            ChainImprover.improve(
//                problem,
//                stats,
//                false
//            )
//        }

//        for (i in listOf<Int>(5,121,123,127,128,129)) {
//        for (i in listOf<Int>(5,121,123,128,129)) {
        for (i in listOf<Int>(120,118,119,121,123,128,129)) {
            val problem = KnownProblems.loadLocalProblem(i)
            val stats = KnownProblems.bestSolutionStats[i - 1]
            ChainImprover.improve(
                problem,
                stats,
                false
            )
        }
    }
}
