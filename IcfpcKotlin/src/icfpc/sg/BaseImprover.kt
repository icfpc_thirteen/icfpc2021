package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure

abstract class BaseImprover(val origSolution: SolutionFigure, val tgtDislikes: Int = 0) {
    companion object {
        fun improveEx(
            problem: Problem,
            stats: BestSolutionInfo,
            force: Boolean,
            submit: Boolean,
            factory: (SolutionFigure) -> BaseImprover
        ) {
            if (!force) {
                if (stats.isOurPerfect()) {
                    println("Skip ${problem.problemId}/${stats.id} because we already have a perfect solution")
                    return
                }
                if (stats.ourBest == stats.worldBest)
                    return
//        if (problem.problemId <= 5)
//            return
            }
            val solutionInfo = KnownProblems.getBestKnownSolutionFile(problem.problemId)
            var origSolution: SolutionFigure
            if (solutionInfo == null) {
//                println("Skip ${problem.problemId}/${stats.id} because there is no known solution")
//                return
                println("Use original ${problem.problemId}/${stats.id} because there is no known solution")
                origSolution = SolutionFigure.fromProblem(problem)
            } else {
                origSolution = SolutionFigure.loadFromSolutionFile(problem, solutionInfo.solFile.path)!!
            }

            val improver = factory(origSolution)
            val ans = improver.improve()
            if (ans == null) {
                println("Can't find a valid solution for ${problem.problemId}")
                return
            }
            if (ans.dislikes >= origSolution.dislikes) {
                println("Can't improve solution for ${problem.problemId}, dis = ${origSolution.dislikes}")
                return
            }

            val improverName = improver.javaClass.simpleName
            println("'$improverName' improver for ${problem.problemId}, ${ans?.dislikes}, ob=${stats.ourBest}/wb=${stats.worldBest}: ${ans}")
            ans.saveToFile(KnownProblems.getSolutionFileName(problem.problemId, ans.dislikes))
            try {
                if (ans.dislikes < stats.ourBest) {
                    println("!!! Submitting #${problem.problemId} '${improverName}': ${ans.dislikes} > ob=${stats.ourBest}")
                    if (submit)
                        Api.submitSolution(problem.problemId, ans)
                }
            } catch (ex: Exception) {
                println("Failed to upload ${problem.problemId}: ${ex}")
            }
        }
    }

    private val solverName = this.javaClass.simpleName


    val problem = origSolution.problem
    var best: SolutionFigure? = null
    private val startTime = System.currentTimeMillis()

    protected abstract val timeout: Long

    protected fun isTimeout(): Boolean {
        val lb = best
        if ((lb != null) && (lb.dislikes == tgtDislikes)) {
            println("Exit ${problem.problemId} by best")
            return true
        }

        val timeout = (System.currentTimeMillis() - startTime) > timeout
        if (timeout) {
            println("Exit ${problem.problemId} by timeout")
        }
        return timeout
    }

    protected fun checkBetter(pis: SolutionFigure, saveProgress: Boolean = true) {
        if (!pis.valid()) {
            println("Tried to check a bad solution by ${solverName}")
            return
        }
        val lb = best
        if ((lb == null) || (pis.dislikes < lb.dislikes)) {
            best = pis
            println("#${problem.problemId} Best sol dis=${pis.dislikes} by ${solverName}")
            if (saveProgress)
                pis.saveToProgressFile()
        }
    }

    fun improve(): SolutionFigure? {
        val valid = origSolution.valid()
        if (valid) {
            println("Start $solverName for ${problem.problemId} with dis= ${origSolution.dislikes}")
            checkBetter(origSolution, false)
        } else {
            if (requireInitialValid) {
                println("#${problem.problemId} $solverName can't improve an invalid solution")
                return null
            } else
                println("!Start $solverName for ${problem.problemId} with an invalid solution")
        }

        improveImpl()
        val lb = best
        lb?.saveToProgressFile()
        return lb
    }

    abstract val requireInitialValid: Boolean

    protected abstract fun improveImpl()
}