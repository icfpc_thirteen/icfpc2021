package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.Box
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.graph.FlipDir
import java.lang.Exception
import kotlin.math.roundToInt


class FlipSolver(val problem: Problem, val useRots: Boolean) {
    private val startSolution: SGPartialSolution = SGPartialSolution.fromProblem(problem)
    private val startTime = System.currentTimeMillis()
    private val timeout = 3 * 60 * 1000L

    private fun isTimeout(): Boolean {
        val timeout = (System.currentTimeMillis() - startTime) > timeout
        if (timeout) {
            println("Exit ${problem.problemId} by timeout")
        }
        return timeout
    }

    fun solve(): SolutionFigure? {
        val flipDirs = mutableListOf<FlipDir>()
        val onlyEdges = false
//        val onlyEdges = true
        if (onlyEdges) {
            for (e in problem.figure.edgeIndices) {
                val c = problem.figure.graph.getFlipDisconnected(e.i1, e.i2)
                if (c != null)
                    flipDirs.add(c)
            }
        } else {
            for (i1 in problem.figure.vertices.indices)
                for (i2 in i1 + 1 until problem.figure.vertices.size) {
                    val c = problem.figure.graph.getFlipDisconnected(i1, i2)
                    if (c != null)
                        flipDirs.add(c)
                }
        }
        println("Known flip directions: ${flipDirs.size} for #${problem.problemId}")

        var best: SolutionFigure? = null
        try {
            var prevLevel = if (useRots)
                startSolution.genRotations(true).toList() else
                listOf<SGPartialSolution>(startSolution)
            for (ll in 1..10) {
                println("level ${ll} of #${problem.problemId}, size=${prevLevel.size}: time=${System.currentTimeMillis() - startTime}")
                val lastLevel = prevLevel.size > 500_000
                val nextLevel = mutableListOf<SGPartialSolution>()
                for (sol in prevLevel) {
                    best = solveOneFlip(sol, flipDirs, best, if (lastLevel) null else nextLevel)
                    if (isTimeout())
                        break
                }
                if (lastLevel) {
                    println("Breaking #${problem.problemId} as this is a too big level #${ll}, pls=${prevLevel.size}")
                    break
                }
                prevLevel = nextLevel
                if (isTimeout())
                    break
            }
        } catch (ex: OutOfMemoryError) {
            println("Solving ${problem.problemId} failed OOM: ${ex}")
        }

        if (best != null)
            best.fitsIntoHole()
        return best
    }

    class BestContainer(var best: SolutionFigure? = null) {
        fun checkBetter(pds: SGPartialSolution, pis: SolutionFigure) {
            val lb = best
            if ((lb == null) || (pis.dislikes < lb.dislikes)) {
                best = pis
                println("Best flip dis=${pis.dislikes}: '${pds.path}'")
                pis.saveToProgressFile()
            }
        }
    }

    private fun checkRots(pds: SGPartialSolution, bestContainer: BestContainer) {
        for (rotated in pds.genRotations()) {
            val pis = tryFit(rotated)
            if (pis != null) {
                bestContainer.checkBetter(rotated, pis)
            }
        }
    }

    private fun solveOneFlip(
        baseSol: SGPartialSolution,
        flipDirs: List<FlipDir>,
        best0: SolutionFigure?,
        nextLevel: MutableList<SGPartialSolution>?
    ): SolutionFigure? {
        val bestContainer = BestContainer(best0)
        for (dir in flipDirs) {
            val pds1 = baseSol.flip(dir)
            if (pds1 != null) {
                nextLevel?.add(pds1)
                checkRots(pds1, bestContainer)
            }

            val dir2 = dir.switch()
            val pds2 = baseSol.flip(dir2)
            if (pds2 != null) {
                nextLevel?.add(pds2)
                checkRots(pds2, bestContainer)
            }
        }

        return bestContainer.best
    }


    fun tryFit(pds: SGPartialSolution): SolutionFigure? {
        val vertices = ArrayList<IntPoint>(pds.vertices.size)
        for (dp in pds.vertices) {
            vertices.add(IntPoint(dp.x.roundToInt(), dp.y.roundToInt()))
        }
        val baseInt = SolutionFigure(pds.problem, vertices)
        if (baseInt.validate().isNotEmpty())
            return null

        val baseBox = Box.fromVertices(baseInt.vertices)
        val containsCache = pds.problem.hole.containsCache
        if ((baseBox.width > containsCache.box.width) || (baseBox.height > containsCache.box.height))
            return null
        val baseShift = IntPoint(baseBox.xMin - containsCache.box.xMin, baseBox.yMin - containsCache.box.yMin)
        val shiftedVertices = ArrayList<IntPoint>(pds.vertices.size)
        for (i in vertices.indices) {
            vertices[i] = vertices[i] - baseShift
            shiftedVertices.add(vertices[i])
        }
        var best: SolutionFigure? = null
        var bestDislikes = Int.MAX_VALUE


        for (dx in 0..(containsCache.box.width - baseBox.width)) {
            for (dy in 0..(containsCache.box.height - baseBox.height)) {
                val shift = IntPoint(dx, dy)
                var good = true
                for (i in vertices.indices) {
                    shiftedVertices[i] = vertices[i] + shift
                    if (!containsCache.contains(shiftedVertices[i])) {
                        good = false
                        break
                    }
                }
                if (!good)
                    continue
                val cur = SolutionFigure(pds.problem, ArrayList(shiftedVertices))
                if (!cur.fitsIntoHole())
                    continue
                val curDislikes = cur.dislikes
                if ((best == null) || (curDislikes < bestDislikes)) {
                    best = cur
                    bestDislikes = curDislikes
//                    return best
                }
            }
        }
        return best
    }

}

fun solveProblemFlip(problem: Problem, stats: BestSolutionInfo) {
    if (stats.isOurPerfect()) {
        println("Skip ${problem.problemId}/${stats.id} because we already have a perfect solution")
        return
    }
    if (stats.ourBest == stats.worldBest)
        return
//    if (problem.problemId <= 5)
//        return
    val solver = FlipSolver(problem, false)
    val ans = solver.solve()
    println("flip solver for ${problem.problemId}, ${ans?.dislikes}, ob=${stats.ourBest}/wb=${stats.worldBest}: ${ans}")
    if (ans != null) {
//        ans.saveToFile(KnownProblems.getSolutionFileName(problemId))
        ans.saveToFile(KnownProblems.getSolutionFileName(problem.problemId, ans.dislikes))
        try {
            if (ans.dislikes < stats.ourBest) {
                println("!!! Submitting ${problem.problemId}: ${ans.dislikes} > ob=${stats.ourBest}")
                Api.submitSolution(problem.problemId, ans)
            }
        } catch (ex: Exception) {
            println("Failed to upload ${problem.problemId}: ${ex}")
        }
    }
}

fun main(args: Array<String>) {
    if (1 == 1) {
        val problemId = 106
        val problem = KnownProblems.loadLocalProblem(problemId)
        solveProblemFlip(problem, KnownProblems.bestSolutionStats[problemId - 1])
    } else {
//        KnownProblems.forEachKnownProblem { problem, stats -> solveProblemFlip(problem, stats) }
        KnownProblems.forEachKnownProblemIn(KnownProblems.oldMaxId4 + 1..KnownProblems.maxId) { problem, stats -> solveProblemFlip(problem, stats) }
    }
}
