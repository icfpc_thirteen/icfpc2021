package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.graph.FlipDir

class EarsImprover(origSolution: SolutionFigure, tgtDislikes: Int = 0, val maxOuterIter: Int = 10) :
    BaseImprover(origSolution, tgtDislikes) {
    companion object {

        fun improve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
//            improveEx(problem, stats, force, submit = true) { sol -> EarsImprover(sol) }
            improveEx(problem, stats, force, submit = false) { sol -> EarsImprover(sol) }
        }
    }

    override val timeout = 6 * 60 * 1000L

    //    private val q = PriorityQueue<QueueEl>(compareBy({ it.sol.dislikes }, { it.badCnt }))

    override val requireInitialValid: Boolean = true

//    val earSizeThreshold = 8
//    val earSizeThreshold = 12
    val earSizeThreshold = 14

    override fun improveImpl() {
        val ears = mutableListOf<FlipDir>()

        for (i1 in problem.figure.vertices.indices)
            for (i2 in i1 + 1 until problem.figure.vertices.size) {
                val c = problem.figure.graph.getFlipDisconnected(i1, i2) ?: continue
                if (c.c1.size <= earSizeThreshold)
                    ears.add(c)
                    if (c.c2.size <= earSizeThreshold)
                    ears.add(c.switch())
            }

        if (ears.size == 0) {
            println("${problem.problemId} has no ears to improve")
            return
        }
        var curSol = origSolution
        val failuresCache = HashSet<Pair<Int, Int>>()
        for (outerIter in 1..maxOuterIter) {
            val bestBefore = curSol.dislikes
            for (earIndex in ears.indices) {
                val ear = ears[earIndex]
                if (isTimeout())
                    return
                var nextSol = curSol
                val p1 = curSol.vertices[ear.i1]
                val p2 = curSol.vertices[ear.i2]
                val d12 = problem.figure.graph.getMaxDistance(ear.i1, ear.i2)
                val hvs = problem.hole.vertices.filter { it.distTo(p1) < 1.8 * d12 || it.distTo(p2) < 1.8 * d12 }
                var good = false
                for (hi in hvs.indices) {
                    val key = Pair(earIndex, hi)
                    if (key in failuresCache)
                        continue
                    val tgtHV = hvs[hi]
                    val moved = tryMoveEarToPoint(origSolution, ear, tgtHV)
                    if (moved == null) {
                        failuresCache.add(key)
                        continue
                    }
                    if (moved.dislikes < nextSol.dislikes) {
                        nextSol = moved
//                        checkBetter(nextSol, outerIter > 1)
                        checkBetter(nextSol, true)
                        good = true
                    }
                }
                if (good) {
                    println("#${problem.problemId} loop $outerIter improved to ${nextSol.dislikes} by moving ear $ear")
                }
                curSol = nextSol
                checkBetter(curSol)
            }
            if (curSol.dislikes == bestBefore) {
                println("Exit by no improvement beyond $bestBefore")
                return
            }
        }
        println("#${problem.problemId} legs reached max out iter ${maxOuterIter}")
    }


    private fun tryMoveEarToPoint(sol: SolutionFigure, ear: FlipDir, hv: IntPoint): SolutionFigure? {
        val hole = sol.problem.hole
        val points = mutableListOf<IndexPoint>()
        for (i in ear.c1.indices) {
            val pi = ear.c1[i]
            points.add(IndexPoint(pi, hv))
        }

        val solver = RelaxSolver(sol, points)
        val nl = solver.solve() ?: return null

        val newSolPoints = ArrayList(sol.vertices)
        for (ip in nl) {
            newSolPoints[ip.index] = ip.p
        }
        val movedSol = SolutionFigure(sol.problem, newSolPoints)
        if (movedSol.fastValid())
            return movedSol
        else
            return null
    }

}

fun main(args: Array<String>) {
    if (1 == 1) {
//        val problemId = 1
        val problemId = 129
        val problem = KnownProblems.loadLocalProblem(problemId)
        val legs = problem.figure.graph.legs
        println("${legs.size}: $legs")
        EarsImprover.improve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
        KnownProblems.forEachKnownProblem { problem, stats -> EarsImprover.improve(problem, stats, false) }
    }
}
