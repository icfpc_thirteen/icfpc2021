package icfpc.sg

import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.graph.FlipDir
import kotlin.math.*


data class SGPartialSolution(val problem: Problem, val vertices: List<DoublePoint>, val path: String) {
    companion object {
        fun fromProblem(problem: Problem): SGPartialSolution {
            return SGPartialSolution(problem, problem.figure.vertices.map { DoublePoint.fromInt(it) }, "")
        }
    }

    fun flip(direction: FlipDir): SGPartialSolution? {
        val bp1 = vertices[direction.i1]
        val bp2 = vertices[direction.i2]
        if (bp1.squaredDistTo(bp2) < 1e-3)
            return null
        val line = DoubleLine(bp1, bp2)
        val newVertices = ArrayList(vertices) // force copy
        for (i in direction.c1) {
            newVertices[i] = line.mirror(newVertices[i])
        }
//        val suffix = "flipped ${direction.c1.size} by ${direction.i1}/${direction.i2}, $bp1, $bp2"
        val suffix = "flipped ${direction.c1.size} by ${direction.i1}/${direction.i2}"
        val newPath = if (path.length > 0) path + "| " + suffix else suffix
        return SGPartialSolution(problem, newVertices, newPath)
    }

    fun genRotations(includeMoreRotations: Boolean = false): Sequence<SGPartialSolution> = sequence<SGPartialSolution> {
        yield(this@SGPartialSolution)
        yield(rotate90())
        yield(rotate270())
        yield(mirrorDXY())
        yield(mirrorXY())
        yield(mirrorX())
        yield(mirrorY())
        if (includeMoreRotations) {
            val range = 360
            for (i in 0..range) {
                val a = i * Math.PI / range
                yield(rotateArbitrary(a))
            }
        }
    }

    fun rotateArbitrary(angleRads: Double): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p ->
                DoublePoint(
                    p.x * cos(angleRads) + p.y * sin(angleRads),
                    p.y * cos(angleRads) - p.x * sin(angleRads)
                )
            },
            path + "| rotate by ${angleRads}"
        )

    }


    fun rotate90(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(p.y, -p.x) },
            path + "| rotate90"
        )
    }

    fun rotate270(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(-p.y, p.x) },
            path + "| rotate270"
        )
    }

    fun mirrorDXY(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(p.y, p.x) },
            path + "| mirrorDXY"
        )
    }

    fun mirrorXY(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(-p.x, -p.y) },
            path + "| mirrorXY"
        )
    }

    fun mirrorX(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(-p.x, p.y) },
            path + "| mirrorX"
        )
    }

    fun mirrorY(): SGPartialSolution {
        return SGPartialSolution(
            problem,
            vertices.map { p -> DoublePoint(p.x, -p.y) },
            path + "| mirrorY"
        )
    }
}

data class DoubleLine(val p1: java.awt.geom.Point2D.Double, val p2: java.awt.geom.Point2D.Double) {
    val a0 = p2.y - p1.y
    val b0 = -(p2.x - p1.x)
    val c0 = -a0 * p1.x - b0 * p1.y
    val m = sqrt(a0 * a0 + b0 * b0)
    val a = a0 / m
    val b = b0 / m
    val c = c0 / m

    fun mirror(p: java.awt.geom.Point2D.Double): DoublePoint {
        val d = a * p.x + b * p.y + c
        val x1 = p.x - 2 * a * d
        val y1 = p.y - 2 * b * d
        return DoublePoint(x1, y1)
    }

}

class DoublePoint(x: kotlin.Double, y: kotlin.Double) : java.awt.geom.Point2D.Double(x, y) {

    init {
        if (x.isNaN())
            throw IllegalArgumentException("x is NaN")
        if (y.isNaN())
            throw IllegalArgumentException("y is NaN")
    }



    operator fun plus(o: DoublePoint): DoublePoint {
        return DoublePoint(x + o.x, y + o.y)
    }

    fun squaredDistTo(other: DoublePoint): kotlin.Double {
        return (this.x - other.x) * (this.x - other.x) + (this.y - other.y) * (this.y - other.y)
    }

    fun distTo(other: DoublePoint): kotlin.Double {
        return Math.sqrt((this.x - other.x).toDouble() * (this.x - other.x).toDouble() + (this.y - other.y).toDouble() * (this.y - other.y).toDouble())
    }

    fun toInt(): IntPoint {
        return IntPoint(x.roundToInt(), y.roundToInt())
    }

    companion object {
        fun fromInt(ip: IntPoint): DoublePoint {
            return DoublePoint(ip.x.toDouble(), ip.y.toDouble())
        }
    }
}
