package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import java.util.*
import kotlin.collections.ArrayList

class MoveVertexImprover(origSolution: SolutionFigure, tgtDislikes: Int = 0, val maxAllowedBad: Int = 5) :
    BaseImprover(origSolution, tgtDislikes) {
    companion object {
        val shifts = listOf(IntPoint(1, 0), IntPoint(-1, 0), IntPoint(0, 1), IntPoint(0, -1))

        fun improve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
            improveEx(problem, stats, force, submit = true) { sol -> MoveVertexImprover(sol) }
//            improveEx(problem, stats, force, submit = false) { sol -> MoveVertexImprover(sol) }
        }
    }

    override val timeout = 4 * 60 * 1000L

    //    private val q = PriorityQueue<QueueEl>(compareBy({ it.sol.dislikes }, { it.badCnt }))
    private val q = PriorityQueue<QueueEl>(compareBy { it.sol.dislikes + it.badCnt * 1500 })

    data class QueueEl(val sol: SolutionFigure, val badCnt: Int) {
        companion object {
            fun createInit(sol: SolutionFigure): QueueEl {
                val res = QueueEl(sol, 0)
                res.from = 0
                val canMove = BitSet()
                for (i in sol.vertices.indices)
                    canMove[i] = true
                res.canMove = canMove
                return res

            }

            fun create(sol: SolutionFigure, badCnt: Int, from: Int, canMove: BitSet): QueueEl {
                val res = QueueEl(sol, badCnt)
                res.from = from
                res.canMove = canMove
                return res
            }
        }

        var from: Int = 0
        var canMove: BitSet? = null
    }

    override val requireInitialValid: Boolean = false

    override fun improveImpl() {
        q.add(QueueEl.createInit(origSolution))
        val graph = problem.figure.graph
        var cnt = 0
        while (q.isNotEmpty()) {
            if (isTimeout())
                break
            val el = q.poll()
            val curSol = el.sol
            if (++cnt % 50_000 == 0) {
                println("#${problem.problemId} cnt=$cnt checking new, qs=${q.size} base ${curSol.dislikes} / ${el.badCnt}")
            }
            val newCanMove = BitSet(origSolution.vertices.size)
            for (pos in origSolution.vertices.indices) {
                if (el.canMove!![pos] || graph.incidence[el.from].contains(pos))
                    tryMoveVertex(el, pos, newCanMove)
            }
        }
    }


    fun tryMoveVertex(el: QueueEl, pos: Int, newCanMove: BitSet) {
        val baseSol = el.sol
        val p0 = baseSol.vertices[pos]
        for (sh in shifts) {
            val mp = ArrayList(baseSol.vertices)
            val np = p0 + sh
            mp[pos] = np
            val ns = SolutionFigure(baseSol.problem, mp)
            val valid = ns.fastValid()
            if (valid) {
                newCanMove[pos] = true
                checkBetter(ns)
                if ((ns.dislikes < baseSol.dislikes) ||
                    ((ns.dislikes <= baseSol.dislikes) && el.badCnt > 0)
                ) {
                    q.offer(QueueEl.create(ns, 0, pos, newCanMove))
                }
            } else {
                if ((ns.dislikes < baseSol.dislikes) && (el.badCnt < maxAllowedBad))
                    q.offer(QueueEl.create(ns, el.badCnt + 1, pos, newCanMove))
            }
        }
    }

}

fun main(args: Array<String>) {
    if (1 == 1) {
        val problemId = 115
        val problem = KnownProblems.loadLocalProblem(problemId)
        MoveVertexImprover.improve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
//        KnownProblems.forEachKnownProblem { problem, stats -> MoveVertexImprover.improve(problem, stats, false) }
        KnownProblems.forEachKnownProblemIn(94..KnownProblems.maxId) { problem, stats -> MoveVertexImprover.improve(problem, stats, false) }
    }
}
