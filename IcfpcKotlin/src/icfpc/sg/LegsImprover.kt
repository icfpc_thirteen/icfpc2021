package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.graph.Leg
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

class LegsImprover(origSolution: SolutionFigure, tgtDislikes: Int = 0, val maxOuterIter: Int = 10) :
    BaseImprover(origSolution, tgtDislikes) {
    companion object {

        fun improve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
            improveEx(problem, stats, force, submit = true) { sol -> LegsImprover(sol) }
//            improveEx(problem, stats, force, submit = false) { sol -> LegsImprover(sol) }
        }
    }

    override val timeout = 4 * 60 * 1000L

    //    private val q = PriorityQueue<QueueEl>(compareBy({ it.sol.dislikes }, { it.badCnt }))

    override val requireInitialValid: Boolean = true

    override fun improveImpl() {
        val legs = problem.figure.graph.legs
        if(legs.size == 0) {
            println("${problem.problemId} has no legs to improve")
            return
        }
        var curSol = origSolution
        val failuresCache = HashSet<Pair<Int, Int>>()
        for (outerIter in 1..maxOuterIter) {
            val bestBefore = curSol.dislikes
            for (li in legs.indices) {
                val l = legs[li]
                if (isTimeout())
                    return
                var nextSol = curSol
                val pl = curSol.vertices[l.startIndex]
                val hvs = problem.hole.vertices.filter { it.distTo(pl) < 1.8 * l.maxPath.dist }
                var good = false
                for (hi in hvs.indices) {
                    val key = Pair(li, hi)
                    if (key in failuresCache)
                        continue
                    val tgtHV = hvs[hi]
                    val moved = tryMoveLegToPoint(origSolution, l, tgtHV)
                    if (moved == null) {
                        failuresCache.add(key)
                        continue
                    }
                    if (moved.dislikes < nextSol.dislikes) {
                        nextSol = moved
                        checkBetter(nextSol, true)
                        good = true
                    }
                }
                if (good) {
                    println("#${problem.problemId} loop $outerIter improved to ${nextSol.dislikes} by moving leg $l")
                }
                curSol = nextSol
                checkBetter(curSol)
            }
            if (curSol.dislikes == bestBefore) {
                println("Exit by no improvement beyond $bestBefore")
                return
            }
        }
        println("#${problem.problemId} legs reached max out iter ${maxOuterIter}")
    }

    private fun improveImpl1() {
        val legs = problem.figure.graph.legs
        val dists = problem.figure.graph.maxDistanceCache
        val l = legs[0]
        println(l)
        val pl = origSolution.vertices[l.startIndex]
        val hvs = problem.hole.vertices.filter { it.distTo(pl) < 1.8 * l.maxPath.dist }
        println(pl)
        println("${hvs.size}: $hvs  ")
        val tgtHV = hvs[8]
        println(tgtHV)
        val moved = tryMoveLegToPoint(origSolution, l, tgtHV)
        println("Moved = $moved")
        moved?.saveToProgressFile()
    }

    private fun tryMoveLegToPoint(sol: SolutionFigure, l: Leg, hv: IntPoint): SolutionFigure? {
        val hole = sol.problem.hole
        val legStart = sol.vertices[l.startIndex]
        val points = mutableListOf<IndexPoint>()
        val used = HashSet<Int>()
        val pathSize = l.maxPath.path.size
        for (i in l.maxPath.path.indices) {
            val pi = l.maxPath.path[i]
            val k2 = i + 1
            val k1 = pathSize - k2
            val p1 = IntPoint(
                (legStart.x * k1 + hv.x * k2) / pathSize,
                (legStart.y * k1 + hv.y * k2) / pathSize
            )
            if (hole.contains(p1)) {
                points.add(IndexPoint(pi, p1))
            } else {
                points.add(IndexPoint(pi, hv))
            }
            used.add(pi)
        }
        for (pp in l.legVerts) {
            if (pp in used)
                continue
            points.add(IndexPoint(pp, hv))
        }

        val solver = RelaxSolver(sol, points)
        val nl = solver.solve() ?: return null

        val newSolPoints = ArrayList(sol.vertices)
        for (ip in nl) {
            newSolPoints[ip.index] = ip.p
        }
        val movedSol = SolutionFigure(sol.problem, newSolPoints)
        if (movedSol.fastValid())
            return movedSol
        else
            return null
    }

}

fun main(args: Array<String>) {
    if (1 == 0) {
//        val problemId = 9
        val problemId = 126
        val problem = KnownProblems.loadLocalProblem(problemId)
        val legs = problem.figure.graph.legs
        println("${legs.size}: $legs")
        LegsImprover.improve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
        KnownProblems.forEachKnownProblem { problem, stats -> LegsImprover.improve(problem, stats, false) }
    }
}
