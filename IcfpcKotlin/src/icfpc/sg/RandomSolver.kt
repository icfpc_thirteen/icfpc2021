package icfpc.sg

import icfpc.BestSolutionInfo
import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import kotlin.random.Random

class RandomSolver(val problem: Problem, val tgtDislikes: Int = 0) {
    companion object {
        fun solve(problem: Problem, stats: BestSolutionInfo, force: Boolean) {
            if (!force) {
                if (stats.isOurPerfect()) {
                    println("Skip ${problem.problemId}/${stats.id} because we already have a perfect solution")
                    return
                }
                if (stats.ourBest == stats.worldBest)
                    return
//        if (problem.problemId <= 5)
//            return
            }
            val solver = RandomSolver(problem, stats.worldBest)
            val ans = solver.solve()
            println("random solver for ${problem.problemId}, ${ans?.dislikes}, ob=${stats.ourBest}/wb=${stats.worldBest}: ${ans}")
            if (ans != null) {
                ans.saveToFile(KnownProblems.getSolutionFileName(problem.problemId, ans.dislikes))
                try {
                    if (ans.dislikes < stats.ourBest) {
                        println("!!! Submitting ${problem.problemId}: ${ans.dislikes} > ob=${stats.ourBest}")
//                        Api.submitSolution(problem.problemId, ans)
                    }
                } catch (ex: Exception) {
                    println("Failed to upload ${problem.problemId}: ${ex}")
                }
            }
        }

    }

    private val startSolution: SolutionFigure = SolutionFigure.fromProblem(problem)
    var best: SolutionFigure? = null
    private val startTime = System.currentTimeMillis()
    private val timeout = 3 * 60 * 1000L

    private fun isTimeout(): Boolean {
        val lb = best
        if ((lb != null) && (lb.dislikes == tgtDislikes)) {
            println("Exit ${problem.problemId} by best")
            return true
        }

        val timeout = (System.currentTimeMillis() - startTime) > timeout
        if (timeout) {
            println("Exit ${problem.problemId} by timeout")
        }
        return timeout
    }

    private fun checkBetter(pis: SolutionFigure) {
        if (pis.validate(true).isNotEmpty()) {
            println("Tried to check a bad solution")
            return
        }
        val lb = best
        if ((lb == null) || (pis.dislikes < lb.dislikes)) {
            best = pis
            println("Best sol dis=${pis.dislikes}")
            pis.saveToProgressFile()
        }
    }

    fun solve(): SolutionFigure? {
        println("${problem.problemId} random solver start")
        val skipHoles = setOf<Int>(3, 4, 5, 7, 8)
        val rndPoints = mutableListOf<IndexPoint>()
        val rnd = Random(0)
        val hole = problem.hole
        val hl = hole.vertices.size

        val binds = mapOf<Int, Int>(
            Pair(0, 1),
            Pair(1, 2),
            Pair(3, 3), //bad but try
            Pair(4, 3), //bad but try
            Pair(6, 4),
            Pair(11, 5),
            Pair(12, 6),
            Pair(9, 7),
            Pair(8, 8),
            Pair(5, 0),
//            Pair(5, 9),
            Pair(2, 0),
        )

        for (i in startSolution.vertices.indices) {
            if (i in binds.keys) {
                val hi = binds[i]!!
                rndPoints.add(IndexPoint(i, hole.vertices[hi]))
            } else {
                var hi = rnd.nextInt(hl)
                while (hi in skipHoles) {
                    hi = rnd.nextInt(hl)
                }
//                rndPoints.add(IndexPoint(i, hole.vertices[hi]))
                rndPoints.add(IndexPoint(i, IntPoint(27 + i, 51 - i)))
            }
        }
        val s0 = SolutionFigure(startSolution.problem, rndPoints.map { it.p })
        s0.saveToFile(KnownProblems.baseSolutionPath + "sol${problem.problemId}_start.json1")


        val solver = RelaxSolver(startSolution, rndPoints, 200_000, allowBad = true)
//        val solver = RelaxSolver(startSolution, rndPoints,50_000)
        val nl = solver.solve() ?: return null

        val newSolPoints = ArrayList(startSolution.vertices)
        for (ip in nl) {
            newSolPoints[ip.index] = ip.p
        }
        val movedSol = SolutionFigure(startSolution.problem, newSolPoints)
        println("Best found ${movedSol.dislikes}")
        movedSol.saveToProgressFile()
        checkBetter(movedSol)

        return best
    }

}

fun main(args: Array<String>) {
    if (1 == 1) {
//        val problemId = 1
        val problemId = 51
        val problem = KnownProblems.loadLocalProblem(problemId)
        val legs = problem.figure.graph.legs
        println("${legs.size}: $legs")
        RandomSolver.solve(problem, KnownProblems.bestSolutionStats[problemId - 1], true)
    } else {
        KnownProblems.forEachKnownProblem { problem, stats -> RandomSolver.solve(problem, stats, false) }
    }
}
