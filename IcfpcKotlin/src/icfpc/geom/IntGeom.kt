package icfpc.geom

import icfpc.api.Api
import icfpc.api.JsonSolutionFigure
import kotlin.math.*
import com.fasterxml.jackson.module.kotlin.readValue
import icfpc.KnownProblems
import icfpc.graph.Graph
import icfpc.sg.DoublePoint
import java.awt.geom.Point2D
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

data class IntPoint(val x: Int, val y: Int) {
    operator fun plus(o: IntPoint): IntPoint {
        return IntPoint(x + o.x, y + o.y)
    }

    operator fun minus(o: IntPoint): IntPoint {
        return IntPoint(x - o.x, y - o.y)
    }

    fun squaredDistTo(other: IntPoint): Int {
        return (this.x - other.x) * (this.x - other.x) + (this.y - other.y) * (this.y - other.y)
    }

    fun distTo(other: IntPoint): Double {
        return Math.sqrt((this.x - other.x).toDouble() * (this.x - other.x).toDouble() + (this.y - other.y).toDouble() * (this.y - other.y).toDouble())
    }

    fun toDouble(): Point2D.Double {
        return Point2D.Double(x.toDouble(), y.toDouble());
    }

    companion object {
        fun fromDouble(pt : Point2D.Double) : IntPoint {
            return IntPoint(pt.x.roundToInt(), pt.y.roundToInt())
        }
        fun distance(pt : Point2D.Double, pt2 : Point2D.Double) : Double {
            val dx = pt.x - pt2.x;
            val dy = pt.y - pt2.y;
            return Math.sqrt(dx*dx+dy*dy)
        }
    }

}

data class EdgeI(
    val i1: Int, val i2: Int, val syn: Boolean = false,
    var length: Double = 0.0,
    var freeHanging: Boolean = false,
    var error: Double = 0.0,
    var islandId: Int = -1
) {
    var points: EdgeP? = null

    fun otherIdx(eix: Int): Int {
        if (eix == i1) return i2 else return i1
    }

    override fun equals(other: Any?): Boolean {
        if (other is EdgeI) {
            return i1 == other.i1 && i2 == other.i2
        }
        return false;
    }

    override fun hashCode(): Int {
        return i1 * 1000 + i2;
    }

    override fun toString(): String {
        return "Edge{$i1,$i2}"
    }
}

data class EdgeP(val startPoint: IntPoint, val endPoint: IntPoint) {
    val sqrLen = startPoint.squaredDistTo(endPoint)
    var a = 0.0
    var b = 0.0
    val horizontal = startPoint.y == endPoint.y
    var vertical = false

    init {
        if (endPoint.x - startPoint.x != 0) {
            a = (endPoint.y - startPoint.y).toDouble() / (endPoint.x - startPoint.x)
            b = startPoint.y - a * startPoint.x
        } else {
            vertical = true
        }
    }

    fun intersectsWith(o: EdgeP, includeEndPoints: Boolean = true): Boolean {
        var x: Double
        var y: Double
        if (!this.vertical && !o.vertical) {
            // check if both vectors are parallel. If they are parallel and don't coinside then no intersection point will exist
            if (this.a - o.a == 0.0) {
                if (this.b - o.b == 0.0) { // edges coincide
                    return isInside(o.startPoint, includeEndPoints) || isInside(o.endPoint, includeEndPoints)
                }
                return false
            }
            x = ((o.b - this.b) / (this.a - o.a)) // x = (b2-b1)/(a1-a2)
            y = o.a * x + o.b // y = a2*x+b2
        } else if (this.vertical && !o.vertical) {
            x = this.startPoint.x.toDouble()
            y = o.a * x + o.b
        } else if (!this.vertical && o.vertical) {
            x = o.startPoint.x.toDouble()
            y = this.a * x + this.b
        } else {
            return false
        }

        if (o.isInside(x, y, includeEndPoints) && this.isInside(x, y, includeEndPoints)) {
            return true
        }

        return false
    }

    fun isInside(p: IntPoint, includeEndPoints: Boolean = true): Boolean {
        // TODO(tilarids): Use epsilon for comparison?
        var on = (p.y - startPoint.y).toDouble() == a * (p.x - startPoint.x)
        if (vertical) {
            on = p.x == startPoint.x
        }
        if (!on)
            return false
        val xMin = min(startPoint.x, endPoint.x)
        val xMax = max(startPoint.x, endPoint.x)
        val yMin = min(startPoint.y, endPoint.y)
        val yMax = max(startPoint.y, endPoint.y)
        if (includeEndPoints) {
            return xMin <= p.x && p.x <= xMax &&
                    yMin <= p.y && p.y <= yMax

        } else {
            return xMin < p.x && p.x < xMax &&
                    yMin < p.y && p.y < yMax
        }
    }

    fun isInside(p: DoublePoint, includeEndPoints: Boolean = true): Boolean {
        return isInside(p.x, p.y, includeEndPoints)
    }

    fun isInside(px: Double, py: Double, includeEndPoints: Boolean = true): Boolean {
        // TODO(tilarids): Use epsilon for comparison?
        val eps = 1e-4
        var on = abs((py - startPoint.y) - a * (px - startPoint.x)) < eps
        if (vertical) {
            on = abs(px - startPoint.x) < eps
        }
        if (!on)
            return false
        val isStart = abs(startPoint.x - px) < eps && abs(startPoint.y - py) < eps
        val isEnd = abs(endPoint.x - px) < eps && abs(endPoint.y - py) < eps
        if (isStart || isEnd)
            return includeEndPoints

        val xMin = min(startPoint.x, endPoint.x)
        val xMax = max(startPoint.x, endPoint.x)
        val yMin = min(startPoint.y, endPoint.y)
        val yMax = max(startPoint.y, endPoint.y)
        if (includeEndPoints || vertical) {
            if (!(xMin <= px && px <= xMax))
                return false
        } else {
            if (!(xMin < px && px < xMax))
                return false
        }

        if (includeEndPoints || horizontal) {
            return yMin <= py && py <= yMax

        } else {
            return yMin < py && py < yMax
        }
    }

}

data class SolutionFigure(val problem: Problem, var vertices: List<IntPoint>) {
    companion object {
        fun loadFromSolutionFile(problem: Problem, filePath: String): SolutionFigure? {
            val file = File(filePath)
            if (file.exists()) {
                val json = file.readText()
                return loadFromSolutionJson(problem, json)
            } else {
                return null;
            }
        }

        fun loadFromSolutionJson(problem: Problem, jsonString: String): SolutionFigure {
            val rawFigure = Api.mapper.readValue<JsonSolutionFigure>(jsonString)
            return SolutionFigure(problem, rawFigure.vertices.map { arr -> IntPoint(arr[0], arr[1]) })
        }

        fun fromProblem(problem: Problem): SolutionFigure {
            return SolutionFigure(problem, problem.figure.vertices)
        }

        fun calcScore(problem: Problem, dislikes: Int, dislikes_best: Int = 0): Double {
            if (dislikes == Int.MAX_VALUE)
                return 0.0
            return 1000 * log2(problem.figure.edgeIndices.size.toDouble() * problem.figure.vertices.size * problem.hole.vertices.size / 6) * sqrt(
                (dislikes_best + 1).toDouble() / (dislikes + 1)
            )
        }

    }

    fun moveBy(dx: Int, dy: Int): SolutionFigure {
        return SolutionFigure(problem, vertices.map { p -> IntPoint(p.x + dx, p.y + dy) })
    }

    fun totalError(): Double {
        return validate(false).sumByDouble { it.error }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    fun genRotations(includeMoreRotations: Boolean): Sequence<SolutionFigure> = sequence<SolutionFigure> {
        yield(this@SolutionFigure)
        yield(rotate90())
        yield(rotate270())
        yield(mirrorDXY())
        yield(mirrorXY())
        yield(mirrorX())
        yield(mirrorY())
        if (includeMoreRotations) {
            val range = 24
            for (i in 0..range) {
                val a = i * Math.PI / range
                yield(rotateArbitrary(a))
            }
        }
    }

    fun rotateArbitrary(angleRads: Double): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p ->
                IntPoint(
                    round(p.x * cos(angleRads) + p.y * sin(angleRads)).toInt(),
                    round(p.y * cos(angleRads) - p.x * sin(angleRads)).toInt()
                )
            }
        )

    }

    fun rotate90(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(p.y, -p.x) }
        )
    }

    fun rotate270(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(-p.y, p.x) },
        )
    }

    fun mirrorDXY(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(p.y, p.x) }
        )
    }

    fun mirrorXY(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(-p.x, -p.y) },
        )
    }

    fun mirrorX(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(-p.x, p.y) },
        )
    }

    fun mirrorY(): SolutionFigure {
        return SolutionFigure(
            problem,
            vertices.map { p -> IntPoint(p.x, -p.y) }
        )
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    fun toJson(): String {
        val jsonObj = JsonSolutionFigure(vertices.map { p -> listOf(p.x, p.y) })
//        val jsonObj = JsonSolutionFigure(vertices.mapIndexed { i, p -> listOf(i, p.x, p.y) })
        val json = Api.prettyWriter.writeValueAsString(jsonObj)
        return json
    }

    fun saveToProgressFile() {
        val path = KnownProblems.getSolutionFileName(problem.problemId, this.dislikes)
        saveToFile(path)
    }

    fun saveToFile(filePath: String) {
        val file = File(filePath)
        file.writeText(this.toJson())
    }

    fun checkEdge(ei: EdgeI): Boolean {
        val p1 = vertices[ei.i1]
        val p2 = vertices[ei.i2]
        return problem.checkNewEdge(p1, p2)
    }

    fun fitsIntoHole(): Boolean {
        for (ei in problem.figure.edgeIndices) {
            if (!checkEdge(ei))
                return false
        }
        return true
    }

    val dislikes: Int by lazy { calcDislikes() }

    fun calcDislikes(): Int {
        var sumD = 0
        for (h in problem.hole.vertices) {
            var minD = h.squaredDistTo(vertices[0])
            for (v in vertices) {
                if (minD > h.squaredDistTo(v)) {
                    minD = h.squaredDistTo(v)
                }
            }
            sumD += minD
        }
        return sumD
    }

    fun valid(): Boolean {
        if (!fitsIntoHole() || !validate().isEmpty()) {
            return false
        }
        return true;
    }

    fun fastValid(): Boolean = !hasBadEdges() && fitsIntoHole()

    fun score(dislikes_best: Int = 0): Double {
        if (!valid()) {
            return -1.0;
        }
        return calcScore(problem, dislikes, dislikes_best)
    }

    fun validate(printBad: Boolean = false): List<EdgeI> {
        val bads = mutableListOf<EdgeI>()
        val epsLong = problem.epsilon.toLong()
        val allowed = problem.epsilon / Problem.EpsDiv
        for (ei in problem.figure.edgeIndices) {
            val op1 = problem.figure.vertices[ei.i1]
            val op2 = problem.figure.vertices[ei.i2]
            val od = op1.squaredDistTo(op2)
            val np1 = vertices[ei.i1]
            val np2 = vertices[ei.i2]
            if (np1.x == -1000000 || np2.x == -1000000) {
//                bads.add(ei)
                continue;
            }
            val nd = np1.squaredDistTo(np2)
            val sdiff = nd - od
            val diff = abs(sdiff)
            // use integer (Long!) math
            if (diff * Problem.EpsDivLong > epsLong * od) {
                if (printBad) {
                    println(
                        "Bad edge ${ei.i1}-${ei.i2}: len=${nd}(${Math.sqrt(nd.toDouble())})," +
                                " orig len = ${od}(${Math.sqrt(od.toDouble())}), diff = ${sdiff}, allowed = $allowed"
                    )
                    println("np1(${ei.i1})=${np1}, np2(${ei.i2})=${np2} | op1=$op1, op2=$op2")
                }
                ei.error = diff.toDouble() / od - allowed;
                bads.add(ei)
            }
        }
        return bads
    }

    fun hasBadEdges(printFirstBad: Boolean = false): Boolean {
        val epsLong = problem.epsilon.toLong()
        val allowed = problem.epsilon / Problem.EpsDiv
        val figure = problem.figure
        val figureVertices = figure.vertices
        for (ei in figure.edgeIndices) {
            val op1 = figureVertices[ei.i1]
            val op2 = figureVertices[ei.i2]
            val od = op1.squaredDistTo(op2)
            val np1 = vertices[ei.i1]
            val np2 = vertices[ei.i2]
            val nd = np1.squaredDistTo(np2)
            val sdiff = nd - od
            val diff = abs(sdiff)
            // use integer (Long!) math
            if (diff * Problem.EpsDivLong > epsLong * od) {
//            if (diff.toDouble() / od > allowed) {
                if (printFirstBad) {
                    println("First bad edge ${ei.i1}-${ei.i2}: len=${nd}, orig len = ${od}, diff = ${sdiff}/${diff.toDouble() / od}, allowed = $allowed")
                    println("np1=${np1}, np2=${np2} | op1=$op1, op2=$op2")
                }
                return true;
            }
        }
        return false
    }

    // distance from outside points to nearest hole vertex
    fun totalOutsideError(): Double {
        val outsiders = vertices.filter { !fitsIntoHole() }
        val dists = outsiders.map { op ->
            problem.hole.vertices.map { it.distTo(op) }.minOrNull()!!
        }.sum()
        return dists;
    }
}

data class Figure(val edgeIndices: List<EdgeI>, var vertices: List<IntPoint>) {
    val edgePoints: List<EdgeP> = edgeIndices.map { ei -> EdgeP(vertices[ei.i1], vertices[ei.i2]) }
    val graph by lazy { Graph.fromEdges(vertices.size, edgeIndices) }

    private val edgePointsMap: Map<Long, EdgeI> =
        edgeIndices.map { ei -> Pair(makeEdgeKey(ei.i1, ei.i2), ei) }.toMap()

    fun getEdgeSqrLen(i1: Int, i2: Int): Int {
        val k = makeEdgeKey(i1, i2)
        return edgePointsMap[k]!!.points!!.sqrLen
    }

    private fun makeEdgeKey(i1: Int, i2: Int): Long {
        if (i1 < i2)
            return i1 * 1_000_000L + i2
        else
            return i2 * 1_000_000L + i1
    }


    // for each point -> indices of points it connects to
    lateinit var exitsToPoints: ArrayList<ArrayList<Int>>;

    // for each point -> indices of edges it connects to
    lateinit var exitsToEdges: ArrayList<ArrayList<Int>>;


    // for each edge, points which are attached to start node (indices)
    lateinit var edgeThirdPoints: ArrayList<ArrayList<Int>>;

    // for each edge, points which are attached to second node (indices)
    lateinit var edgeFourthPoints: ArrayList<ArrayList<Int>>;

    // polar angle between edge i1->i2  and the point (i1->p3)
    lateinit var edgeThirdRo: ArrayList<ArrayList<Double>>;

    // polar length (i1 -> p3)
    lateinit var edgeThirdLen: ArrayList<ArrayList<Double>>;

    // for each edge, all edges pointing to 3rd points
    lateinit var edgeThirdExit: ArrayList<ArrayList<Int>>;

    // polar angle between edge i1->i2  and the point (i1->p4)
    lateinit var edgeFourthRo: ArrayList<ArrayList<Double>>;

    // polar length (i1 -> p4)
    lateinit var edgeFourthLen: ArrayList<ArrayList<Double>>;

    // for each edge, all edges pointing to 4rd point
    lateinit var edgeFourthExit: ArrayList<ArrayList<Int>>;


    lateinit var verticeOnBorder: ArrayList<Boolean>
    lateinit var borderVertices: ArrayList<Int>

    val islands: ArrayList<List<Int>> by lazy {
        val rv = calculateIslands(0.rangeTo(vertices.size).toList(), true)
        calculateIslandsTree(rv);
        rv
    }

    class IslandsTree(val island : Int, val children: ArrayList<IslandsTree>, var  parent: IslandsTree?, var edgeId: Int, var enabled : Boolean = true) {

    }

    var islandsTree: IslandsTree = IslandsTree(-1, ArrayList(), null, -1)

    var markedEdges: List<Int> = listOf()

    init {
        exitsToPoints = ArrayList<ArrayList<Int>>()
        exitsToEdges = ArrayList<ArrayList<Int>>()
        for (i in 0 until vertices.size) {
            exitsToPoints.add(ArrayList<Int>())
            exitsToEdges.add(ArrayList<Int>())
        }
        for (eix in 0 until edgeIndices.size) {
            val ei = edgeIndices[eix]
            val p1 = vertices[ei.i1]
            val p2 = vertices[ei.i2]
            ei.points = EdgeP(vertices[ei.i1], vertices[ei.i2])
            ei.length = p1.distTo(p2)
            exitsToPoints[ei.i1].add(ei.i2);
            exitsToPoints[ei.i2].add(ei.i1);
            exitsToEdges[ei.i1].add(eix);
            exitsToEdges[ei.i2].add(eix);
        }
        fun angleToAxis(p1: IntPoint, p2: IntPoint): Double {
            // y axis is inverted.
            var angle = Math.atan2((p1.y - p2.y).toDouble(), (p2.x - p1.x).toDouble())
            if (angle < 0) {
                angle += 2 * Math.PI
            }
            return angle
        }
        for (eix in 0..vertices.size - 1) {
            val angles = (0..exitsToPoints[eix].size - 1).map({ it ->
                angleToAxis(
                    vertices[eix],
                    vertices[exitsToPoints[eix][it]]
                )
            })
            val idxs = (0..exitsToPoints[eix].size - 1).sortedWith(compareBy({ angles[it] }))
            val pts = idxs.map({ ei -> exitsToPoints[eix][ei] })
            exitsToPoints[eix] = ArrayList(pts)
            val edges = idxs.map({ ei -> exitsToEdges[eix][ei] })
            exitsToEdges[eix] = ArrayList(edges)
        }
        calculateVerticeOnBorder()
        calculateFreeHanging()

        //println("Islands:\n$islands")
    }

    fun shakedVertices(d: Int): List<IntPoint> {
        return vertices.map { IntPoint(it.x + (Math.random() * d).toInt(), it.y + (Math.random() * d).toInt()) }
    }

    fun calculateIslands(enabledPoints: List<Int>, useEdgesOnly: Boolean): ArrayList<List<Int>> {
        val disabledPoints = HashSet<Int>()
        disabledPoints.addAll(0.rangeTo(vertices.size))
        disabledPoints.removeAll(enabledPoints);    // zapresheno vse chto ne razresheno
        val rv = ArrayList<List<Int>>()
        if (enabledPoints.size > 3) {
            graph.flipDisconnectedCache.clear()
            val attempts = ArrayList<Pair<Int, Int>>()
            if (useEdgesOnly) {
                for (e in edgeIndices) {
                    attempts.add(Pair(e.i1, e.i2))
                }
            } else {
                for (e1 in vertices.indices) {
                    for (e2 in vertices.indices) {
                        if (e2 > e1) {
                            attempts.add(Pair(e1, e2))
                        }
                    }
                }
            }
            for (p in attempts) {
                if (disabledPoints.contains(p.first) || disabledPoints.contains(p.second)) {
                    continue;
                }
                val c = graph.getFlipDisconnected(p.first, p.second, disabledPoints)
                if (c != null) {
                    if (c.c1.size == 0 || c.c2.size == 0) {
                        // skip
                    } else {
                        val leftPart = ArrayList<Int>(c.c1)
                        val rightPart = ArrayList<Int>(c.c2)
                        leftPart.add(p.first); leftPart.add(p.second);
                        rightPart.add(p.first); rightPart.add(p.second);
                        val leftIslands = calculateIslands(leftPart, useEdgesOnly);
                        val rightIslands = calculateIslands(rightPart, useEdgesOnly);
                        rv.addAll(leftIslands);
                        rv.addAll(rightIslands);
                        return rv;
                    }
                }
            }
        }
        rv.add(enabledPoints.toList())
        return rv;
    }

    private fun calculateIslandsTree(islands: java.util.ArrayList<List<Int>>) {
        var placedEdges = java.util.HashSet<EdgeI>()
        var placedPoints = ArrayList<IntPoint?>(vertices.map { null })
        val doneIslands = java.util.HashSet<Int>()
        var root : IslandsTree? = null;
        for (iter in 0 until islands.size) {
//            println("Placed edges: ${placedEdges}")
            val all = java.util.ArrayList<Pair<Int, Box>>()
            for (island in islands.indices) {
                if (!doneIslands.contains(island)) {
                    val set = islands[island].toSet()
                    val subedges = edgeIndices.filter { set.contains(it.i1) && set.contains(it.i2) }
//                    println("Testing subedges: $subedges of island: ${islands[island]}");
                    if (placedEdges.isEmpty() || subedges.any { placedEdges.contains(it) }) {
                        val subs = subsetOfVertices(islands[island])
                        val bb = Hole.boundingBox(subs)
                        val p = Pair(island, bb)
                        all.add(p)
                    }
                }
            }
            val maxx = Collections.max(all, { i1, i2 ->
                i1.second.maxDim() - i2.second.maxDim()
            })
            val set = java.util.HashSet(islands[maxx.first])
            val subedges = edgeIndices.filter { set.contains(it.i1) && set.contains(it.i2) }

            if (root == null) {
                root = IslandsTree(maxx.first, ArrayList(), null, -1)
            } else {
                val singleEdge = placedEdges.intersect(subedges)
                if (singleEdge.size != 1) { // something should exist.
                    System.err.println("Assertion failed in calculateIslandsTree")
                    return;
                }
                val joinEdge = singleEdge.first()
                val joinEdgeI = edgeIndices.indexOf(joinEdge)
                if (joinEdgeI == 1) { // something should exist.
                    System.err.println("Assertion failed in calculateIslandsTree")
                    return;
                }
                var node = findIslandInTree(root, joinEdge.islandId);
                if (node == null) {
                    System.err.println("WTF in calculateIslandsTree");
                    return;
                } else {
                    node.children.add(IslandsTree(maxx.first, ArrayList(), node, joinEdgeI))
                }
            }
            placedEdges.addAll(subedges);
            subedges.forEach {if (it.islandId < 0) it.islandId = maxx.first }
            doneIslands.add(maxx.first)
        }
        islandsTree = root!!
    }

    private fun findIslandInTree(root: Figure.IslandsTree, islandId: Int): IslandsTree? {
        if (root.island == islandId) {
            return root;
        }
        for(t in root.children) {
            val q = findIslandInTree(t, islandId);
            if (q != null) return q;
        }
        return null;
    }

    private fun calculateFreeHanging() {
        for (i in 0..edgeIndices.size - 1) {
            var used_edges = ArrayList<Boolean>()
            for (j in 0..edgeIndices.size - 1) {
                used_edges.add(false)
            }
            used_edges[i] = true
            val startVertIdx = edgeIndices[i].i1
            val endVertIdx = edgeIndices[i].i2
            var pathFound = false
            var queue = ArrayList<Int>()
            queue.add(startVertIdx)
            while (!queue.isEmpty() && !pathFound) {
                var nqueue = ArrayList<Int>()
                for (curIdx in queue) {
                    if (curIdx == endVertIdx) {
                        pathFound = true
                        break
                    }
                    for (eix in exitsToEdges[curIdx]) {
                        if (!used_edges[eix]) {
                            nqueue.add(edgeIndices[eix].otherIdx(curIdx))
                            used_edges[eix] = true
                        }
                    }
                }
                queue = nqueue;
            }
            if (!pathFound) {
                edgeIndices[i].freeHanging = true
            }
        }
    }

    private fun calculateVerticeOnBorder() {  // expects the exits to be calculated already
        verticeOnBorder = ArrayList<Boolean>()
        borderVertices = ArrayList<Int>()
        for (i in 0 until vertices.size) {
            verticeOnBorder.add(false)
        }
        var minI = 0
        for (eix in 1..vertices.size - 1) {
            if (vertices[minI].x < vertices[eix].x) {
                continue
            }
            if (vertices[minI].x > vertices[eix].x || vertices[minI].y > vertices[eix].y) {
                minI = eix
            }
        }
        verticeOnBorder[minI] = true
        borderVertices.add(minI)
        var prevI = minI
        var curI = exitsToPoints[minI][exitsToPoints[minI].size - 1]
        while (!verticeOnBorder[curI]) {
            verticeOnBorder[curI] = true
            borderVertices.add(curI)
            val nextIdx = (exitsToPoints[curI].indexOf(prevI) + 1) % exitsToPoints[curI].size
            prevI = curI
            curI = exitsToPoints[curI][nextIdx]
        }
    }

    fun calculateEdgeThirdPoints() {
        edgeThirdPoints = ArrayList(edgeIndices.size)
        edgeThirdRo = ArrayList(edgeIndices.size)
        edgeThirdLen = ArrayList(edgeIndices.size)
        edgeThirdExit = ArrayList(edgeIndices.size)
        edgeFourthPoints = ArrayList(edgeIndices.size)
        edgeFourthRo = ArrayList(edgeIndices.size)
        edgeFourthLen = ArrayList(edgeIndices.size)
        edgeFourthExit = ArrayList(edgeIndices.size)
        for (ei in 0 until edgeIndices.size) {
            edgeThirdPoints.add(ArrayList())
            edgeThirdRo.add(ArrayList())
            edgeThirdLen.add(ArrayList())
            edgeThirdExit.add(ArrayList())
            edgeFourthPoints.add(ArrayList())
            edgeFourthRo.add(ArrayList())
            edgeFourthLen.add(ArrayList())
            edgeFourthExit.add(ArrayList())
            val edge = edgeIndices[ei];
            val p1 = edge.i1;
            val p2 = edge.i2;
            for (p3 in 0 until vertices.size) {
                if (p3 != p1 && p3 != p2) {
                    val ei1 = exitsToPoints[p3].indexOf(p1);        // point is connected by i1 of this edge
                    if (ei1 != -1) {
                        if (!edgeFourthPoints[ei].contains(p3)) {
                            edgeThirdPoints[ei].add(p3)             // accesslible via this edge

                            // have two vectors, normalizing by p1
                            val x1 = (vertices[p2].x - vertices[p1].x).toDouble();
                            val y1 = (vertices[p2].y - vertices[p1].y).toDouble();
                            val x2 = (vertices[p3].x - vertices[p1].x).toDouble();
                            val y2 = (vertices[p3].y - vertices[p1].y).toDouble();

                            // len of the pointing vector (towards p3)
                            val len2 = Math.sqrt(x2 * x2 + y2 * y2);
                            // angle of the pointing vector (toward p3) relative to edge
                            val angle = Math.acos((x1 * x2 + y1 * y2) / (Math.sqrt(x1 * x1 + y1 * y1) * len2))

                            edgeThirdRo[ei].add(angle);
                            edgeThirdLen[ei].add(len2);
                            edgeThirdExit[ei].add(exitsToEdges[p3][ei1])  // storing which exit edges are connected to third point
                        }
                    }


                    // Same for fourth point

                    val ei2 = exitsToPoints[p3].indexOf(p2);        // point is connected by i2 of this edge
                    if (ei2 != -1) {
                        if (!edgeThirdPoints[ei].contains(p3)) {        // add to fourth point only if missing from third point
                            edgeFourthPoints[ei].add(p3)

                            // have two vectors, for 4th point origin of coordinates will be i2 of edge (p2)
                            val x1 = (vertices[p2].x - vertices[p1].x).toDouble();
                            val y1 = (vertices[p2].y - vertices[p1].y).toDouble();
                            val x2 = (vertices[p3].x - vertices[p1].x).toDouble();
                            val y2 = (vertices[p3].y - vertices[p1].y).toDouble();

                            // len of the pointing vector (towards p3)
                            val len2 = Math.sqrt(x2 * x2 + y2 * y2);
                            // angle of the pointing vector (toward p3) relative to edge
                            val angle = Math.acos((x1 * x2 + y1 * y2) / (Math.sqrt(x1 * x1 + y1 * y1) * len2))

                            edgeFourthRo[ei].add(angle);
                            edgeFourthLen[ei].add(len2);
                            edgeFourthExit[ei].add(exitsToEdges[p3][ei2])
                        }
                    }

                }
            }
        }
    }

    fun edgeIdxFromExits(exit1: Int, exit2: Int): Int {
        for (eix in exitsToEdges[exit1]) {
            if ((edgeIndices[eix].i1 == exit1 && edgeIndices[eix].i2 == exit2) ||
                (edgeIndices[eix].i2 == exit1 && edgeIndices[eix].i1 == exit2)
            ) {
                return eix
            }
        }
        return -1
    }

    // Uncomment to continue working on faces generator
    fun faces(): ArrayList<ArrayList<EdgeI>> {
        var faces = ArrayList<ArrayList<EdgeI>>()
        var open_edges = ArrayList<Boolean>()
        for (i in 0..edgeIndices.size - 1) {
            open_edges.add(true)
        }
        for (i in 0..edgeIndices.size - 1) {
            if (open_edges[i] && !edgeIndices[i].freeHanging) {
                // start tracing
                var included_vertices = ArrayList<Boolean>()
                var face_vertices = ArrayList<Int>()

                for (j in 0..vertices.size - 1) {
                    included_vertices.add(false)
                }
                included_vertices[edgeIndices[i].i1] = true
                face_vertices.add(edgeIndices[i].i1)
                var prevI = edgeIndices[i].i1
                var curI = edgeIndices[i].i2
                while (!included_vertices[curI]) {
                    included_vertices[curI] = true
                    face_vertices.add(curI)
                    val nextIdx = (exitsToPoints[curI].indexOf(prevI) + 1) % exitsToPoints[curI].size
                    prevI = curI
                    curI = exitsToPoints[curI][nextIdx]
                }
                var face = ArrayList<EdgeI>()
                for (j in 0 until face_vertices.size) {
                    val nextj = (j + 1) % face_vertices.size
                    val edgeIdx = edgeIdxFromExits(face_vertices[j], face_vertices[nextj])
                    if (edgeIdx >= edgeIndices.size || edgeIdx < 0) {
                        System.err.println("[Faces calculation error], ignore");
                        return faces;
                    }
                    face.add(edgeIndices[edgeIdx])
                    open_edges[edgeIdx] = false
                }
                faces.add(face)
            }
        }
        return faces
    }

    // returns all triangles represented by vertices indices
    fun triangles(): ArrayList<Triple<Int, Int, Int>> {
        var added_edges = HashSet<Pair<Int, Int>>()
        var border_edges = HashSet<Pair<Int, Int>>()
        for (i in 0..borderVertices.size - 1) {
            val nexti = (i + 1) % borderVertices.size
            border_edges.add(Pair(borderVertices[i], borderVertices[nexti]))
            border_edges.add(Pair(borderVertices[nexti], borderVertices[i]))
        }
        var triangles = ArrayList<Triple<Int, Int, Int>>()
        for (iv in 0..vertices.size - 1) {
            val pts = exitsToPoints[iv]
            for (i in 0..pts.size - 1) {
                val nexti = (i + 1) % pts.size
                if (border_edges.contains(Pair(iv, exitsToPoints[iv][i])) && border_edges.contains(
                        Pair(
                            iv,
                            exitsToPoints[iv][nexti]
                        )
                    )
                ) {
                    continue
                } else if (added_edges.contains(Pair(iv, exitsToPoints[iv][i])) &&
                    added_edges.contains(Pair(exitsToPoints[iv][i], exitsToPoints[iv][nexti])) &&
                    added_edges.contains(Pair(iv, exitsToPoints[iv][nexti]))
                ) {
                    continue
                } else {
                    triangles.add(Triple(iv, exitsToPoints[iv][i], exitsToPoints[iv][nexti]))
                    added_edges.add(Pair(iv, exitsToPoints[iv][i]))
                    added_edges.add(Pair(exitsToPoints[iv][i], iv))
                    added_edges.add(Pair(exitsToPoints[iv][i], exitsToPoints[iv][nexti]))
                    added_edges.add(Pair(exitsToPoints[iv][nexti], exitsToPoints[iv][i]))
                    added_edges.add(Pair(iv, exitsToPoints[iv][nexti]))
                    added_edges.add(Pair(exitsToPoints[iv][nexti], iv))
                }
            }
        }
        return triangles
    }

    fun trianglesFigure(): Figure {
        var newEdgeIndices = ArrayList<EdgeI>()
        // TODO(tilarids): Support synthetics.
        for (triangle in triangles()) {
            if (triangle.first != triangle.second) {
                newEdgeIndices.add(EdgeI(triangle.first, triangle.second, syn = false))
            }
            if (triangle.first != triangle.third) {
                newEdgeIndices.add(EdgeI(triangle.first, triangle.third, syn = false))
            }
            if (triangle.third != triangle.second) {
                newEdgeIndices.add(EdgeI(triangle.third, triangle.second, syn = false))
            }
        }
        return Figure(newEdgeIndices, vertices)
    }

    fun subsetOfVertices(i: List<Int>): List<IntPoint> = i.map { vertices[it] }
}

data class Hole(val vertices: List<IntPoint>) {
    val box: Box = boundingBox(vertices)

    companion object {
        fun boundingBox(vertices: List<IntPoint>): Box {
            return Box.fromVertices(vertices)
        }
    }

    data class ContainsCache(val vertices: Set<IntPoint>, val bitSet: BitSet) {
        val box = Box.fromVertices(vertices)
        val boxW = box.xMax - box.xMin + 1;
        val boxH = box.yMax - box.yMin + 1;
        fun contains(p: IntPoint): Boolean = contains(p.x, p.y)

        fun contains(x: Int, y: Int): Boolean {
            if (x < box.xMin || y < box.yMin || x > box.xMax || y > box.yMax) return false;
            val off = (x - box.xMin) + boxW * (y - box.yMin);
            val b1 = bitSet.get(off);
//            val b2 = vertices.contains(p)
//            if (b1 != b2)
//                throw RuntimeException("Assertion");
            return b1;
        }
    }

    val containsCache: ContainsCache by lazy {
        val vertices = HashSet<IntPoint>()
        val bitmap = BitSet((box.xMax - box.xMin + 1) * (box.yMax - box.yMin + 1));
        var bix = 0;
        for (y in box.yMin..box.yMax) {
            for (x in box.xMin..box.xMax) {
                val p = IntPoint(x, y)
                if (containsSlow(p)) {
                    vertices.add(p)
                    bitmap.set(bix);
                }
                bix++;
            }
        }
        return@lazy ContainsCache(vertices, bitmap)
    }

    fun contains(p: IntPoint): Boolean {
        return containsCache.contains(p)
    }

    fun contains(x: Int, y: Int): Boolean {
        return containsCache.contains(x, y)
    }

    fun containsSlow(p: IntPoint): Boolean {
        var s = checkPoints(vertices[0], vertices[vertices.size - 1], p)
        for (i in 0..vertices.size - 2) {
            s *= checkPoints(vertices[i], vertices[i + 1], p)
        }
        return s <= 0
    }

    private fun checkPoints(a: IntPoint, b: IntPoint, middle: IntPoint): Int {
        val ax = a.x - middle.x
        val ay = a.y - middle.y
        val bx = b.x - middle.x
        val by = b.y - middle.y
        val s = (ax * by - ay * bx).sign
        if (s == 0 && (ay == 0 || by == 0) && ax * bx <= 0) return 0
        if ((ay < 0) != (by < 0)) {
            if (by < 0)
                return s
            return -s
        }
        return 1
    }

}

data class Box(val xMin: Int, val xMax: Int, val yMin: Int, val yMax: Int) {
    val width: Int = xMax - xMin
    val height: Int = yMax - yMin

    fun maxDim(): Int {
        return Math.max(width, height)
    }

    companion object {
        fun fromVertices(vertices: Collection<IntPoint>): Box {
            var xMin = 0;
            var xMax = 0;
            var yMin = 0;
            var yMax = 0;
            for (v in vertices) {
                if (v.x > -1000000) {
                    xMin = v.x
                    xMax = v.x
                    yMin = v.y
                    yMax = v.y
                    break;
                }
            }
            for (v in vertices) {
                if (v.x > -1000000) {
                    if (xMin > v.x) {
                        xMin = v.x
                    }
                    if (xMax < v.x) {
                        xMax = v.x
                    }
                    if (yMin > v.y) {
                        yMin = v.y
                    }
                    if (yMax < v.y) {
                        yMax = v.y
                    }
                }
            }
            return Box(xMin, xMax, yMin, yMax)
        }
    }

}

