package icfpc.geom

import com.fasterxml.jackson.module.kotlin.readValue
import icfpc.api.Api
import icfpc.api.JsonProblemData
import java.awt.geom.Point2D
import kotlin.math.abs
import kotlin.math.roundToInt

data class Problem(val problemId: Int, val hole: Hole, val figure: Figure, val epsilon: Int) {

    val epsilonFull = epsilon / EpsDiv;

    // this checks if new line (x1,y1)--(x2,y2) has length, which differs from given len within epsilon
    // returns <= 0 if OK
    fun checkEpsilon(x1: Double, y1: Double, x2: Double, y2: Double, len: Double): Double {
        val dx = x2 - x1;
        val dy = y2 - y1;
        val checkLen = dx * dx + dy * dy;
        return Math.abs(checkLen / (len * len) - 1) - epsilonFull
    }


    fun calcEdgeError(nd: Int, od: Int): Int {
        val sdiff = nd - od
        val diff = abs(sdiff)
        val epsLong = epsilon.toLong()
        if (diff * EpsDivLong > epsLong * od) {
            return diff
        } else
            return 0
    }


    fun fitPoint(double: Point2D.Double): Point2D.Double? {
        val test = IntPoint(double.x.roundToInt(), double.y.roundToInt());
        if (hole.contains(test)) {
            return Point2D.Double(test.x.toDouble(), test.y.toDouble());
        }
        for (dx in -1..1) {
            for (dy in -1..1) {
                val test2 = IntPoint(double.x.roundToInt() + dx, double.y.roundToInt() + dy);
                if (hole.contains(test2)) {
                    return Point2D.Double(test2.x.toDouble(), test2.y.toDouble());
                }
            }
        }
        return null
    }

    fun checkNewEdge(p1: IntPoint, p2: IntPoint): Boolean {
        if (!hole.contains(p1) || !hole.contains(p2)) {
            return false
        }
        val problemEdge = EdgeP(p1, p2)

        val minX = Math.min(p1.x, p2.x);
        val minY = Math.min(p1.y, p2.y);
        val maxX = Math.max(p1.x, p2.x);
        val maxY = Math.max(p1.y, p2.y);


        var prevHV = hole.vertices[0]
        for (i in 0 until hole.vertices.size) {
            val nexti = (i + 1) % hole.vertices.size
            val hv1 = prevHV;       // reduce index access
            val hv2 = hole.vertices[nexti];
            prevHV = hv2;
            if ((hv1 == p1 && hv2 == p2) ||
                (hv1 == p2 && hv2 == p1)
            ) {
                return true
            }
            if (hv1.x > maxX && hv2.x > maxX) continue;
            if (hv1.y > maxY && hv2.y > maxY) continue;
            if (hv1.x < minX && hv2.x < minX) continue;
            if (hv1.y < minY && hv2.y < minY) continue;
            val problemWall = EdgeP(hv1, hv2)
            if (problemEdge.intersectsWith(problemWall, includeEndPoints = false)) {
                return false
            }
        }

        // after the loop that can exit with true if we exactly match the hole edge
        // ugly hack but should work to cover the case when both ends on the edges of the
        // hole but the segment is fully outside
        if (!hole.contains((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)) {
            return false
        }

        return true; // valid one
    }


    companion object {
        val EpsDiv: Double = 1_000_000.0
        val EpsDivLong: Long = 1_000_000L

        fun fromRawProblem(id: Int, rawProblem: JsonProblemData): Problem {
            val hole = Hole(rawProblem.hole.map { arr -> IntPoint(arr[0], arr[1]) })
            val syn = HashSet<String>()
            rawProblem.figure.synedges?.forEach { arr -> syn.add("${arr[0]}-${arr[1]}") }
            val figure = Figure(rawProblem.figure.edges.map { arr ->
                EdgeI(
                    arr[0],
                    arr[1],
                    syn.contains("${arr[0]}-${arr[1]}")
                )
            },
                rawProblem.figure.vertices.map { arr -> IntPoint(arr[0], arr[1]) })
            return Problem(id, hole, figure, rawProblem.epsilon)
        }

        fun fromJson(id: Int, json: String): Problem {
            val rawProblem = Api.mapper.readValue<JsonProblemData>(json)
            return fromRawProblem(id, rawProblem)
        }
    }

}