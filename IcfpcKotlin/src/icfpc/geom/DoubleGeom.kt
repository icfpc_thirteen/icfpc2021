package icfpc.geom

//fun distanceBetweenPointAndLine()
// see https://en.wikipedia.org/wiki/Cross_product

fun vectorProductAbs(a1: Double, a2: Double, b1: Double, b2: Double): Double {
    return a1 * b2 - a2 * b1;
}