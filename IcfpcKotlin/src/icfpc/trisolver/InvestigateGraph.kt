package icfpc.trisolver

import icfpc.KnownProblems
import icfpc.geom.Problem
import icfpc.graph.FlipDir

class InvestigateGraph(val problem: Problem) {

    fun run() {
        val all = ArrayList<Int>();
        all.addAll(0.rangeTo(problem.figure.vertices.size))
//        all.clear()
//        all.addAll(listOf(10, 14, 11, 19, 15, 23, 24, 20, 16, 21))
//        val allIslands = problem.figcalculateIslands(all, false);
//        problem.calculateIslands()
        println(problem.figure.islands);
    }


}

@Suppress("UNUSED_PARAMETER")
fun main(args: Array<String>) {
    val problemId = sanProblem
    val problem = KnownProblems.loadLocalProblem(problemId)
    InvestigateGraph(problem).run()

}
