package icfpc.trisolver

import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.Figure
import icfpc.geom.IntPoint
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.vis.VisComponent
import io.vavr.collection.TreeMap
import io.vavr.collection.TreeSet
import io.vavr.kotlin.getOrNull
import io.vavr.kotlin.toVavrStream
import java.awt.geom.Point2D
import java.awt.image.BufferedImage
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.math.PI
import kotlin.math.roundToInt


class PartialSolution(
    val prob: Problem,
    val pointFixed: io.vavr.collection.TreeMap<Int, Point2D.Double>,
    val edgeFixed: io.vavr.collection.TreeSet<Int>,
    val edgeAvailable: io.vavr.collection.TreeSet<Int>,      // edges which have tried all points, don't try
) : Comparable<PartialSolution> {
    // this checks if new point being placed, does not violate needed distances to existing points.
    fun tryPlacePoint(prob: Problem, vertexNo: Int, coords: IntPoint, edge1: Int): PartialSolution? {
        if (pointFixed.containsKey(vertexNo))
            throw RuntimeException("Point Already placed in PartialSolution")
        val exitsToPoints = prob.figure.exitsToPoints[vertexNo]
        val exitsToEdges = prob.figure.exitsToEdges[vertexNo]
        for (ex in 0 until exitsToPoints.size) {
            val e = exitsToPoints[ex];
            val pf = pointFixed.getOrNull(e)
            if (pf != null) {
                if (prob.checkEpsilon(pf.x, pf.y, coords.x.toDouble(), coords.y.toDouble(), prob.figure.edgeIndices[exitsToEdges[ex]].length) > 0) {
                    // this placement would violate distance to already placed point.
                    return null;
                }
                if (!prob.checkNewEdge(IntPoint(pf.x.roundToInt(), pf.y.roundToInt()), coords)) {
                    return null; // can't place
                }
            }
        }
        return PartialSolution(
            prob,
            pointFixed.put(vertexNo, Point2D.Double(coords.x.toDouble(), coords.y.toDouble())),
            edgeFixed.add(edge1), edgeAvailable.add(edge1)
        )
    }

    override fun compareTo(other: PartialSolution): Int {
        return this.pointFixed.size() - other.pointFixed.size()
    }

    fun toFigure(): Figure {
        val pts = List<IntPoint>(prob.figure.vertices.size, { index ->
            val pp = pointFixed.getOrNull(index)
            if (pp != null) {
                IntPoint(pp.x.roundToInt(), pp.y.roundToInt())
            } else {
                IntPoint(-1000000, -1000000)
            }
        })
        return Figure(prob.figure.edgeIndices, pts)
    }

    fun toSolutionFigure(): SolutionFigure {
        val pts = List<IntPoint>(prob.figure.vertices.size, { index ->
            val pp = pointFixed.getOrNull(index)
            if (pp != null) {
                IntPoint(pp.x.roundToInt(), pp.y.roundToInt())
            } else {
                IntPoint(-1000000, -1000000)
            }
        })
        prob.figure.markedEdges = edgeAvailable.toJavaList()
        return SolutionFigure(prob, pts);
    }

    fun findAnyFreshEdge(): Int {
        if (edgeAvailable.isEmpty) return -1
//        val ix = (Math.random() * edgeAvailable.size()).roundToInt() % edgeAvailable.size();
//        return edgeAvailable.elementAt(ix)
        return edgeAvailable.first()
    }

    fun closeEdge(fromEdge: Int): PartialSolution {
        return PartialSolution(prob, pointFixed, edgeFixed, edgeAvailable.remove(fromEdge))
    }

    // will return closed, if edge is done
    // edge is done when all connected points are fixed.
    fun maybeCloseEdge(fromEdge: Int): PartialSolution {
        for (testPoint in prob.figure.edgeThirdPoints[fromEdge]) {
            if (!pointFixed.containsKey(testPoint)) {
                return this;
            }
        }
        for (testPoint in prob.figure.edgeFourthPoints[fromEdge]) {
            if (!pointFixed.containsKey(testPoint)) {
                return this;
            }
        }
        return closeEdge(fromEdge)
    }

}

fun view_(ps: PartialSolution): BufferedImage {
    val p = Problem(-1, ps.prob.hole, ps.toFigure(), ps.prob.epsilon)
    p.figure.islands.clear()
//    val p = ps.prob;
    val vc = VisComponent();
    vc.setData(p)
    val bi = BufferedImage(1200, 800, BufferedImage.TYPE_INT_RGB)
    vc.paintIntoGraphics(bi.createGraphics(), bi.width, bi.height)
    return bi;
}

fun view_(ps: SolutionFigure): BufferedImage {
//    val p = ps.prob;
    val vc = VisComponent();
    vc.setData(ps.problem, ps)
    val bi = BufferedImage(1200, 800, BufferedImage.TYPE_INT_RGB)
    vc.paintIntoGraphics(bi.createGraphics(), bi.width, bi.height)
    return bi;
}


var maxPoints = 0
var maxIters = 0

fun addMirrors(
    ps: PartialSolution,
    fromEdge: Int,
    isFourthPoint: Boolean,     // origin point of angle being formed between edge and away point.
    x1: Double,
    y1: Double,     // farther point of base edge
    x2: Double,
    y2: Double,
    origLength: Double,
    awayPoints: ArrayList<Int>,
    awayExits: ArrayList<Int>,
    awayRo: ArrayList<Double>,
    awayLen: ArrayList<Double>
): ArrayList<PartialSolution> {
    val nqueue = ArrayList<PartialSolution>()
    val dx1 = x2 - x1;
    val dy1 = y2 - y1;
    for (ix in 0 until awayPoints.size) {        // third points
        val pointNo = awayPoints[ix];
        val already = ps.pointFixed.getOrNull(pointNo)
        if (already == null) {
            val ro = awayRo[ix]
            val len = awayLen[ix]

            val dx2 = dx1 * len / origLength
            val dy2 = dy1 * len / origLength
            if (origLength == 0.0) {
                println("origLength == 0.0")
                continue;
            }

            // and now rotate it by ro
            val dx3 = dx2 * Math.cos(ro) - dy2 * Math.sin(ro);
            val dy3 = dy2 * Math.cos(ro) + dx2 * Math.sin(ro);
            // now move back from 0-based
            val dx4 = dx3 + x1;                     // first position of 3rd pointg
            val dy4 = dy3 + y1;

            // and now rotate it by -ro
            val dx5 = dx2 * Math.cos(-ro) - dy2 * Math.sin(-ro);
            val dy5 = dy2 * Math.cos(-ro) + dx2 * Math.sin(-ro);
            // now move back from 0-based
            val dx6 = dx5 + x1;                     // mirror position of 3rd pointg
            val dy6 = dy5 + y1;
            var addedS1 = false;
            var addedS2 = false;
            for (_dx in -1..1) {
                for (_dy in -1..1) {
                    if (!addedS1) {
                        val thirdPoint = IntPoint(dx4.roundToInt() + _dx, dy4.roundToInt() + _dy);
                        if (ps.prob.hole.contains(thirdPoint) && ps.prob.checkEpsilon(x1, y1, thirdPoint.x.toDouble(), thirdPoint.y.toDouble(), len) <= 0) {
                            val ps2 = ps.tryPlacePoint(ps.prob, awayPoints[ix], thirdPoint, awayExits[ix]);
                            if (ps2 != null) {
                                nqueue.add(ps2)
                                addedS1 = true;
                            }
                        }
                    }
                    if (!addedS2) {
                        val thirdPointMirror = IntPoint(dx6.roundToInt() + _dx, dy6.roundToInt() + _dy);
                        if (ps.prob.hole.contains(thirdPointMirror) && ps.prob.checkEpsilon(x1, y1, thirdPointMirror.x.toDouble(), thirdPointMirror.y.toDouble(), len) <= 0) {
                            val ps2 = ps.tryPlacePoint(ps.prob, awayPoints[ix], thirdPointMirror, awayExits[ix]);
                            if (ps2 != null) {
                                nqueue.add(ps2)
                                addedS2 = true;
                            }
                        }
                    }
                    if (addedS1 && addedS2) {
                        // added something
                        break;
                    }
                }
                if (addedS1 && addedS2) {
                    // added something
                    break;
                }
            }
        }
    }
    return nqueue
}

var debugg = false;

fun trySolveFor(prob: Problem, ps0: PartialSolution): ArrayList<PartialSolution> {

    var queue = LinkedList<PartialSolution>()
    queue.add(ps0);
    val figure = prob.figure
    var niters = 0
    var rv = ArrayList<PartialSolution>()
    var ctm = System.currentTimeMillis();
    var thisMaxPoints = 0;
    var razvod = ArrayList<PartialSolution>()
    maxPoints = 0;
    while (queue.size > 0) {
        niters++;
        val pst = queue.remove()
        if (queue.size > 5000) {
            val randFactor = 1.25 * (1 + (queue.size - 5000) / 5000);
            if (Math.random() * randFactor > 1) {
                if (Math.random() < 0.001) {
                    razvod.add(pst);
                }
                continue;
            }
        }
        if (thisMaxPoints > 60 && queue.size < 10 && niters % 10 == 0 && razvod.size > 0) {
//            val rebirth = ((Math.random() * 10 * razvod.size)).toInt() % razvod.size
//            queue.add(razvod[rebirth]);
//            razvod.removeAt(rebirth);
//            println("Add razvod");
            return rv;
        }
        var ps = pst
        while (true) {
            val fromEdge = ps.findAnyFreshEdge()
            if (fromEdge < 0)
                break;
            if (ps.pointFixed.size() == figure.vertices.size) {
                rv.add(ps);
                return rv;
//                break;
            }

            val thisEdge = figure.edgeIndices[fromEdge];
            val pi1 = ps.pointFixed.getOrNull(thisEdge.i1)!!
            val pi2 = ps.pointFixed.getOrNull(thisEdge.i2)!!
            val x1 = pi1.x;
            val x2 = pi2.x;
            val y1 = pi1.y;
            val y2 = pi2.y;
            // 0-based edge (by coordinates):
            // increasing it to have length
            val origLength = figure.edgeIndices[fromEdge].length
            if (origLength <= 0.0) {
                throw java.lang.RuntimeException("edge length = 0")
            }


            val nq1 = addMirrors(ps, fromEdge, false, x1, y1, x2, y2, origLength, figure.edgeThirdPoints[fromEdge], figure.edgeThirdExit[fromEdge], figure.edgeThirdRo[fromEdge], figure.edgeThirdLen[fromEdge])
            val nq2 = addMirrors(ps, fromEdge, true, x1, y1, x2, y2, origLength, figure.edgeFourthPoints[fromEdge], figure.edgeFourthExit[fromEdge], figure.edgeFourthRo[fromEdge], figure.edgeFourthLen[fromEdge])
            queue.addAll(nq1);
            queue.addAll(nq2);
            if (ps.pointFixed.size() > maxPoints) {
                maxPoints = ps.pointFixed.size()
                println("points=${ps.pointFixed.size()} of ${figure.vertices.size}, queuesize=${queue.size}")
            }
            if (ps.pointFixed.size() > thisMaxPoints) {
                thisMaxPoints = ps.pointFixed.size()
            }
            if (debugg) {
                println("Extracted length ${pst.pointFixed.size()}, added ${nq1.size + nq2.size} attempts from edge: $fromEdge")
            }
            if (nq1.size == 0 && nq2.size == 0) {
                ps = ps.closeEdge(fromEdge);
            } else {
                break;
            }
        }
    }
    debugg = false
//    println("niters=${niters} time=${System.currentTimeMillis()-ctm }")
    return rv
}


fun scoreSolution(ps: PartialSolution): Double {
    return 1.0 / ps.toSolutionFigure().dislikes
}

fun optimizeSlide(ps: PartialSolution): PartialSolution {
    val solfig = ps.toSolutionFigure()
    var bestScore = solfig.calcDislikes()
    var bestVertixes = solfig.vertices
    val origVertices = solfig.vertices
    for (dx in -10..10) {
        for (dy in -10..10) {
            val dest = ArrayList<IntPoint>()
            for (pt in origVertices) {
                dest.add(IntPoint(pt.x + 5 * dx, pt.y + 5 * dy))
            }
            val newFig = SolutionFigure(ps.prob, dest);
            if (newFig.fastValid()) {
                val newdis = newFig.calcDislikes()
                if (newdis < bestScore) {
                    bestScore = newdis;
                    bestVertixes = dest
                }
            }
        }
    }
    var newArr = ps.pointFixed.reject { k, v -> true }
    for (q in 0 until bestVertixes.size) {
        newArr = newArr.put(q, bestVertixes[q].toDouble())
    }
    return PartialSolution(ps.prob, newArr, ps.edgeFixed, ps.edgeAvailable);
}

fun solveProblem(prob: Problem) {
    println("Generating start positions...")
    val maxhv = prob.hole.vertices.size              // max hole vertices
    val maxfe = prob.figure.edgeIndices.size        // max figure edges
    var best: PartialSolution? = null
    var bestScore = 0.0
    prob.figure.calculateEdgeThirdPoints()
    val sols = ArrayList<PartialSolution>()

    // along the edges
    for (hv in 0..maxhv - 1) {       // hole vertex (edge)
        val hv0 = prob.hole.vertices[hv]                // hole vertex
        val hv1 = prob.hole.vertices[(hv + 1) % maxhv]
        for (fe in 0..maxfe - 1) {      // figure edge
            val fv0 = prob.figure.edgePoints[fe].startPoint     // figure vertex
            val fv1 = prob.figure.edgePoints[fe].endPoint
            if (fv0.squaredDistTo(fv1) <= hv0.squaredDistTo(hv1)) { // edge is long enough ?

                val feOnly = TreeSet.empty<Int>().add(fe);

                // here we create initial partial solutions for few variants

                val edgeToEdgeRatio = hv0.distTo(hv1) / fv0.distTo(fv1)        // how longer is hole edge

                val p1 = prob.fitPoint(Point2D.Double(hv0.x.toDouble(), hv0.y.toDouble()))
                val p2 = prob.fitPoint(
                    Point2D.Double(
                        hv0.x.toDouble() + (hv1.x.toDouble() - hv0.x.toDouble()) / edgeToEdgeRatio,
                        hv0.y.toDouble() + (hv1.y.toDouble() - hv0.y.toDouble()) / edgeToEdgeRatio
                    )
                )
                if (p1 != null && p2 != null) {
                    sols.add(
                        PartialSolution(
                            prob,
                            TreeMap.empty<Int, Point2D.Double>()
                                .put(prob.figure.edgeIndices[fe].i1, p1)
                                .put(prob.figure.edgeIndices[fe].i2, p2),
                            feOnly, feOnly
                        )
                    )
                    sols.add(
                        PartialSolution(
                            prob,
                            TreeMap.empty<Int, Point2D.Double>()
                                .put(prob.figure.edgeIndices[fe].i1, p2)
                                .put(prob.figure.edgeIndices[fe].i2, p1),
                            feOnly, feOnly
                        )
                    )
                }
                val p3 = prob.fitPoint(Point2D.Double(hv1.x.toDouble(), hv1.y.toDouble()))
                val p4 = prob.fitPoint(
                    Point2D.Double(
                        hv1.x.toDouble() + (hv0.x.toDouble() - hv1.x.toDouble()) / edgeToEdgeRatio,
                        hv1.y.toDouble() + (hv0.y.toDouble() - hv1.y.toDouble()) / edgeToEdgeRatio
                    )
                )
                if (p3 != null && p4 != null) {
                    sols.add(
                        PartialSolution(
                            prob,
                            TreeMap.empty<Int, Point2D.Double>()
                                .put(prob.figure.edgeIndices[fe].i1, p3)
                                .put(prob.figure.edgeIndices[fe].i2, p4),
                            feOnly, feOnly
                        )
                    )
                    sols.add(
                        PartialSolution(
                            prob,
                            TreeMap.empty<Int, Point2D.Double>()
                                .put(prob.figure.edgeIndices[fe].i1, p4)
                                .put(prob.figure.edgeIndices[fe].i2, p3),
                            feOnly, feOnly
                        )
                    )
                }
            }
        }
    }

    println("Generating random start positions... (${sols.size} fixed)")
    // random things
    for (fe in maxfe - 1 downTo 0) {      // figure edge
        val feOnly = TreeSet.empty<Int>().add(fe);
        val fv0 = prob.figure.edgePoints[fe].startPoint     // figure vertex
        val fv1 = prob.figure.edgePoints[fe].endPoint
        val dist = fv0.distTo(fv1);
        val FULLCOUNT = 50
        var count = FULLCOUNT;
        var attempts = 0
        while (count > 0) {
            attempts++;
            if (attempts > 1000 && count == FULLCOUNT) {
                break;
            }
            val box = prob.hole.box;
            val rx = (box.xMin + Math.random() * (box.xMax - box.xMin)).roundToInt()
            val ry = (box.yMin + Math.random() * (box.yMax - box.yMin)).roundToInt()
            val pt = IntPoint(rx, ry);
            if (prob.hole.contains(pt)) {
                val angle = Math.random() * 2 * PI;
                val ex = rx + dist * Math.cos(angle)
                val ey = ry + dist * Math.sin(angle)
                val et = IntPoint(ex.toInt(), ey.toInt());
                if (prob.hole.contains(et)) {
                    if (prob.checkEpsilon(rx.toDouble(), ry.toDouble(), et.x.toDouble(), et.y.toDouble(), dist) <= 0) {
                        if (prob.checkNewEdge(pt, et)) {
                            sols.add(
                                PartialSolution(
                                    prob,
                                    TreeMap.empty<Int, Point2D.Double>()
                                        .put(prob.figure.edgeIndices[fe].i1, pt.toDouble())
                                        .put(prob.figure.edgeIndices[fe].i2, et.toDouble()),
                                    feOnly, feOnly,
                                )
                            )
                            sols.add(
                                PartialSolution(
                                    prob,
                                    TreeMap.empty<Int, Point2D.Double>()
                                        .put(prob.figure.edgeIndices[fe].i1, et.toDouble())
                                        .put(prob.figure.edgeIndices[fe].i2, pt.toDouble()),
                                    feOnly, feOnly,
                                )
                            )
                            count--;
                        }
                    }
                }
            }
        }
    }

    println("Shuffling... (${sols.size} total)")
    Collections.shuffle(sols)
    println("Solving... (${sols.size} total)")
    val counter = AtomicInteger(0)
    val start = System.currentTimeMillis();
    var lastScores = ArrayList<Int>()
    sols.toVavrStream().toJavaParallelStream().map { x ->
        val c = counter.incrementAndGet();
        if (c % 100 == 0) {
            var ls = synchronized(sols) {
                val ls = lastScores;
                lastScores = ArrayList<Int>();
                ls
            }
            var minn = 0;
            var maxx = 0;
            var aver = 0.0;
            var cnt = ls.size
            if (ls.size > 0) {
                minn = ls.min()!!
                maxx = ls.max()!!
                aver = ls.average()
            }
            println("${System.currentTimeMillis() - start} Doing ${c} of ${sols.size}... was: min=$minn max=$maxx cnt=$cnt aver=$aver")
        }
        var maybeSols = trySolveFor(prob, x);
        for (s in maybeSols) {
            val theScore0 = scoreSolution(s);
            val s0 = optimizeSlide(s);
            val theScore = scoreSolution(s0);
            synchronized(sols) {
                lastScores.add((1 / theScore).toInt())
                if (theScore > bestScore) {
                    println("Best score so far=${1 / theScore} <- ${1 / theScore0}")
                    best = s0
                    bestScore = theScore;
                    Api.submitSolution(prob.problemId, s0.toSolutionFigure());
                }
            }
        }
        0.0
    }.count();
    val abest = best;
    if (abest != null) {
        println("Found solution! ${abest.toSolutionFigure().dislikes}")
        Api.submitSolution(prob.problemId, abest.toSolutionFigure());
//        best.toSolutionFigure().dislikes
    }
}


@Suppress("UNUSED_PARAMETER")
fun main(args: Array<String>) {
    val problemId = 86
    runTriSolver(problemId)
}

fun runTriSolver(problemId: Int) {
    println("Running TriSolver for problem $problemId")
    solveProblem(KnownProblems.loadLocalProblem(problemId))
}

fun runTriSolver(problemIdRange: IntRange = 1..78) {
    println("Running TriSolver for problems $problemIdRange")
    problemIdRange.forEach { runTriSolver(it) }
}