package icfpc.trisolver

import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.*
import icfpc.sg.DoubleLine
import io.vavr.collection.TreeMap
import io.vavr.collection.TreeSet
import java.awt.geom.Point2D

class Unwrapper(val problem: Problem) {

    val utils: AppendSolver = AppendSolver(problem)

    fun foldOnce(path: ArrayList<Figure.IslandsTree>, foldIndex: Int, ps: PartialSolution) : PartialSolution {
        val lastPoints = collectPointsOfChildren(path[foldIndex + 1])
        val lastMinusEdge = lastPoints.subtract(collectPoints(listOf(path[foldIndex])))
        val edge = problem.figure.edgeIndices[path[foldIndex + 1].edgeId]
        val dl = DoubleLine(ps.pointFixed[edge.i1].get(), ps.pointFixed[edge.i2].get())

        var newPoints = lastMinusEdge.fold(ps.pointFixed) { acc, pt ->
            acc.put(pt, dl.mirror(ps.pointFixed.get(pt).get()))
        }
        val ps1 = PartialSolution(problem, newPoints, ps.edgeFixed, ps.edgeAvailable)
        return ps1;
    }


    fun unwrap(inputFig: SolutionFigure): SolutionFigure {

        val points = inputFig.vertices.withIndex().fold(TreeMap.empty<Int, Point2D.Double>()) { acc, iv -> acc.put(iv.index, iv.value.toDouble()) }
        var ps = PartialSolution(
            problem, points,
            problem.figure.edgeIndices.indices.fold(TreeSet.empty(), { acc, v -> acc.add(v) }),
            TreeSet.empty()
        )

        for(r in 0..10) {
            println("Roll: $r")
            val newPs = rollOnce(ps)
            if (newPs != null) {
                ps = newPs
            } else {
                // tree will be marked
            }
        }
        return ps.toSolutionFigure()
    }

    private fun rollOnce(ps: PartialSolution): PartialSolution? {
        val tmp = problem.figure.islands; // lazy init
        val islandsTree = problem.figure.islandsTree
        val deepest = findMaxDeptsh(islandsTree)
        var scan: Figure.IslandsTree? = deepest.second;
        val path = ArrayList<Figure.IslandsTree>()
        while (scan != null) {
            path.add(0, scan)
            scan = scan.parent
        }


        val limit = path.size - 2
        var bestFig: SolutionFigure? = null
        var bestScore = ps.toSolutionFigure().dislikes;
        val l = System.currentTimeMillis();
        for (a in 0..10000) {
            if (System.currentTimeMillis() > l + 5000 && bestFig != null || System.currentTimeMillis() > l + 15000) {
                println("Break?")
                break;
            }
            val switchPoints = (0 until limit).filter { Math.random() < 0.5 }
            val psOut = switchPoints.fold(ps, { acc, fp -> foldOnce(path, fp, acc) })
            val fig0 = placeBetter(psOut.toSolutionFigure())
            if (fig0.fitsIntoHole()) {
                val fig1 = utils.shakeInvalids(fig0)
                if (fig1 != null) {
                    var score = fig1.dislikes
                    if (score < bestScore) {
                        println("new best score: $score, switchPoints=$switchPoints of path len ${(0 until limit)} ")
                        val fig = fig1
                        score = fig.dislikes
                        bestScore = score
                        bestFig = fig
                    }
                }
            }
        }
        for (i in path.size - 3 until path.size) {
            if (i > 0) {
                path[i].enabled = false
            }
        }
        if (bestFig != null) {
            val result = PartialSolution(
                problem, bestFig.vertices.withIndex().fold(TreeMap.empty<Int, Point2D.Double>()) { acc, iv -> acc.put(iv.index, iv.value.toDouble()) },
                problem.figure.edgeIndices.indices.fold(TreeSet.empty(), { acc, v -> acc.add(v) }),
                TreeSet.empty()
            )
            return result;
        } else {
            return null;
        }
    }
    fun rotate(f: SolutionFigure, d: Double, mayBeOutside: Boolean) : SolutionFigure {
        val myPoints = f.vertices
        val figureBox = Hole.boundingBox(myPoints)
        val at = java.awt.geom.AffineTransform();
        at.rotate(d, (figureBox.xMin + figureBox.width/2).toDouble(), (figureBox.yMin + figureBox.height/2).toDouble())
        val np = myPoints.map { IntPoint.fromDouble(at.transform(it.toDouble(), null)as Point2D.Double)  }
        val sf = SolutionFigure(f.problem, np);
        return placeBetter(sf, mayBeOutside);
    }

    fun placeBetter(f: SolutionFigure, mayBeOutside : Boolean = false) : SolutionFigure {
//        println("Place better...")
        val myPoints = f.vertices
        val fitBox = problem.hole.box
        val figureBox = Hole.boundingBox(myPoints)
//                println("set=${set} myPoints=${myPointsBeforeFitting} box=${figureBox.maxDim()}");
        val initialOffsetX = fitBox.xMin - figureBox.xMin;
        val initialOffsetY = fitBox.yMin - figureBox.yMin;
        val allowDX = fitBox.width - figureBox.width;
        val allowDY = fitBox.height - figureBox.height;
        var bestOffset = IntPoint(-1000000, -1000000);
        var bestDislikes = 1000000000;
        val step = 1;
        for (tryX0 in 0..allowDX/step) {
            for (tryY0 in 0..allowDY/step) {
                val tryX = tryX0 * step;
                val tryY = tryY0 * step;
                val offset = IntPoint(tryX + initialOffsetX, tryY + initialOffsetY)
                var pass = mayBeOutside;
                if (!pass)
                    pass = f.problem.figure.edgeIndices.all { problem.checkNewEdge(myPoints[it.i1] + offset, myPoints[it.i2] + offset) };
                if (pass) {
                    var dislikes = 0
                    for (h in problem.hole.vertices) {
                        var minD = h.squaredDistTo(myPoints[0] + offset)
                        for (v in myPoints) {
                            val nd = h.squaredDistTo(v + offset)
                            if (minD > nd) {
                                minD = nd
                            }
                        }
                        dislikes += minD
                    }
                    if (dislikes < bestDislikes) {
                        bestOffset = offset;
                        bestDislikes = dislikes;
                    }
                }
            }
        }
        if (bestOffset.x != -1000000) {
//            println("Place better: dislikes = $bestDislikes")
            return SolutionFigure(f.problem, f.vertices.map { it + bestOffset })
        } else {
            return f;
        }

    }

    private fun collectPoints(subList: List<Figure.IslandsTree>): Set<Int> {
        return subList.flatMap { problem.figure.islands[it.island] }.toSet()
    }

    private fun collectPointsOfChildren(node: Figure.IslandsTree): Set<Int> {
        val mySet = HashSet<Int>(problem.figure.islands[node.island])
        mySet.addAll(node.children.flatMap { collectPointsOfChildren(it) })
        return mySet
    }

    private fun findMaxDeptsh(islandsTree: Figure.IslandsTree): Pair<Int, Figure.IslandsTree> {
        if (islandsTree.children.isEmpty())
            return Pair(0, islandsTree)
        val depths = islandsTree.children.filter { it.enabled }.map { findMaxDeptsh(it) }
        val rv = depths.maxByOrNull { it.first }
        if (rv == null) {
            return Pair(0, islandsTree)
        }
        return Pair(1 + rv.first, rv.second)
    }

    fun rotateOptimize(solutionFigure: SolutionFigure, allowOutside: Boolean = true): SolutionFigure {
        var bestError =100000.0;
        var bestAngle =-1.0
        for(q in 0..359) {
            val angle = 2 * Math.PI / 360 * q
            val t = rotate(solutionFigure, angle, allowOutside)
            if (allowOutside) {
                val err = t.totalOutsideError()
                if (err < bestError) {
                    bestError = err;
                    bestAngle = angle;
                }
            } else {
                if (t.valid()) {
                    val err = t.dislikes.toDouble()
                    if (err < bestError) {
                        bestError = err;
                        bestAngle = angle;
                    }
                }
            }
        }
        if (bestAngle < 0)
            return solutionFigure;
        val rv = rotate(solutionFigure, bestAngle, allowOutside)
        println("rotate optimized err=${solutionFigure.totalOutsideError()} -> ${rv.totalOutsideError()}")
        return rv
    }


}

@Suppress("UNUSED_PARAMETER")
fun main(args: Array<String>) {

    val problemId = sanProblem
    val problem = KnownProblems.loadLocalProblem(problemId)
    KnownProblems.getBestKnownSolutionFile(problemId)?.let {
        var solutionFigure = SolutionFigure.loadFromSolutionFile(problem, KnownProblems.getSolutionFileName(problemId, it.dislikes))
        if (solutionFigure != null) {
            if (solutionFigure.validate(true).isNotEmpty()) {
                return
            }
            val origDislikes = solutionFigure.dislikes;
            println("dislikes: ${solutionFigure.dislikes}; score: ${solutionFigure?.score(2663)}")
//            problem.figure.vertices = solutionFigure.vertices
            val solver = Unwrapper(problem)
            val optimized = solver.unwrap(solutionFigure)
            val fresh = SolutionFigure(problem, optimized.vertices)
            if (fresh.validate(true).isEmpty()) {
                if (fresh.calcDislikes() < origDislikes) {
                    Api.submitSolution(problem.problemId, fresh)
                }
            } else {
                println("Not valid?")
            }
        }
    }

}
