package icfpc.trisolver

import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.IntPoint
import icfpc.geom.SolutionFigure
import java.util.ArrayList
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference

class SimpleOptimizer {

    fun optimizeShaking(inputFig: SolutionFigure): SolutionFigure {
        val utils = Unwrapper(inputFig.problem)
        var solFigure: SolutionFigure = utils.rotateOptimize(inputFig, false);
        var starting0 = solFigure.calcDislikes()
        while (true) {
            var starting = solFigure.calcDislikes()
            val origVertices = ArrayList(solFigure.vertices)
            var improved = false;
            val execService = Executors.newCachedThreadPool()
            for (p1 in solFigure.vertices.indices) {
                for (dx in -3..3)
                    for (dy in -3..3) {
                        execService.submit(Runnable {
                            val testVertices = ArrayList(solFigure.vertices)
                            testVertices[p1] = origVertices[p1] + IntPoint(dx, dy);
                            for (p2 in solFigure.vertices.indices) {
                                for (dx2 in -3..3)
                                    for (dy2 in -3..3) {
                                        testVertices[p2] = origVertices[p2] + IntPoint(dx2, dy2);
                                        val newFigure = SolutionFigure(solFigure.problem, testVertices)
                                        if (newFigure.valid() && newFigure.validate(false).isEmpty()) {
                                            val newDislikes = newFigure.calcDislikes()
                                            synchronized(execService) {
                                                if (newDislikes < starting) {
                                                    println("New dislikes = $newDislikes")
                                                    improved = true;
                                                    starting = newDislikes;
                                                    solFigure = SolutionFigure(solFigure.problem, ArrayList(testVertices));
                                                }
                                            }
                                        }
                                    }
                                testVertices[p2] = origVertices[p2]
                            }
                        })
                    }
            }
            execService.shutdown()
            execService.awaitTermination(10000, TimeUnit.SECONDS)
            if (!improved) {
                println("Finished improving: $starting0 -> $starting")
                break;
            } else {
                println("Improved: $starting...")
                solFigure = utils.rotateOptimize(solFigure, false);
            }
        }
        return solFigure
    }

    fun fitShaking(inputFig: SolutionFigure): SolutionFigure {
        var solFigure: SolutionFigure = inputFig;
        var starting0 = solFigure.totalOutsideError()
        while (true) {
            var starting = AtomicReference(solFigure.totalOutsideError())
            val origVertices = ArrayList(solFigure.vertices)
            var improved = false;
            val execService = Executors.newCachedThreadPool()
            var lastPrint = System.currentTimeMillis();
            val totalTasks = AtomicInteger(0)
            val LIM = 10
            for (p1 in solFigure.vertices.indices) {

                for (dx in -LIM..LIM)
                    for (dy in -LIM..LIM) {
                        execService.submit( {
//                            if (System.currentTimeMillis() > lastPrint + 1000) {
////                                println("P1=$p1/${solFigure.vertices.size}")
//                                lastPrint = System.currentTimeMillis();
//                            }
                            val testVertices = ArrayList(solFigure.vertices)
                            testVertices[p1] = origVertices[p1] + IntPoint(dx, dy);
                            for (p2 in p1+1 until solFigure.vertices.size) {
                                for (dx2 in -LIM..LIM)
                                    for (dy2 in -LIM..LIM) {
                                        testVertices[p2] = origVertices[p2] + IntPoint(dx2, dy2);


                                        for (p3 in p2+1 until solFigure.vertices.size) {
                                            for (dx3 in -LIM..LIM)
                                                for (dy3 in -LIM..LIM) {
                                                    testVertices[p3] = origVertices[p3] + IntPoint(dx3, dy3);

                                                    val newFigure = SolutionFigure(solFigure.problem, testVertices)
                                                    if (newFigure.validate(false).isEmpty()) {
                                                        val newError = newFigure.totalOutsideError()
                                                        if (newError < starting.get()) {
                                                            synchronized(execService) {
                                                                if (newError < starting.get()) {
                                                                    println("New error = $newError")
                                                                    improved = true;
                                                                    starting.set(newError);
                                                                    solFigure = SolutionFigure(solFigure.problem, ArrayList(testVertices));
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            testVertices[p3] = origVertices[p3]
                                        }
                                    }
                                testVertices[p2] = origVertices[p2]
                            }
                            totalTasks.decrementAndGet()
                        })
                        totalTasks.incrementAndGet()
                    }
            }
            println("Submitted")
            execService.shutdown()
            while(!execService.awaitTermination(1, TimeUnit.SECONDS)) {
                println("Tasks remaining: ${totalTasks.get()}")
            }
            println("Service Terminated.")
            if (!improved) {
                println("Finished improving: (outside err) $starting0 -> $starting")
                break;
            } else {
                println("Improved: (outside err)  $starting...")
                val utils = Unwrapper(solFigure.problem)
                solFigure = utils.rotateOptimize(solFigure)
                println("Rotated: (outside err)  ${solFigure.totalOutsideError()}...")
            }
        }
        return solFigure
    }


}

@Suppress("UNUSED_PARAMETER")
fun main(args: Array<String>) {
//    forceSubmitSolutionFromFile(98, 62576)
//    var score = fig1.dislikes
    if (false) {
        for (pro in 1..132) {
            val problem = KnownProblems.loadLocalProblem(pro)
            println("${problem.hole.vertices.size * problem.figure.vertices.size * problem.figure.edgeIndices.size}\t$pro")
            KnownProblems.getBestKnownSolutionFile(pro)?.let {
                var solutionFigure = SolutionFigure.loadFromSolutionFile(problem, KnownProblems.getSolutionFileName(pro, it.dislikes))
                if (!(solutionFigure?.valid() ?: true)) {
                    println("Invalid solution: $pro, $it.dislikes}")
                }
            }
        }
        System.exit(1)
    }
    val problemId = sanProblem
    val problem = KnownProblems.loadLocalProblem(problemId)
    val solutionFigure = KnownProblems.getBestKnownSolutionFile(problemId)?.let {
        SolutionFigure.loadFromSolutionFile(problem, KnownProblems.getSolutionFileName(problemId, it.dislikes))
    }?:SolutionFigure.fromProblem(problem)
    val solver = SimpleOptimizer()
    val origDislikes = solutionFigure.dislikes;
    if (!solutionFigure.valid()) {
        val utils2 = Unwrapper(problem)
        val t = utils2.rotateOptimize(solutionFigure)
        //val t = solutionFigure
        val fresh0 = solver.fitShaking(t)
        val fresh = SolutionFigure(fresh0.problem, fresh0.vertices)
        if (fresh.valid() && fresh.validate(true).isEmpty()) {
            if (fresh.calcDislikes() < origDislikes) {
                Api.submitSolution(problem.problemId, fresh)
            }
        } else {
            println("Not valid?")
        }
        System.exit(0)
    }
    println("dislikes: ${solutionFigure.dislikes}; score: ${solutionFigure?.score(2663)}")
//            problem.figure.vertices = solutionFigure.vertices
    val optimized = solver.optimizeShaking(solutionFigure)
    val fresh = SolutionFigure(problem, optimized.vertices)
    if (fresh.valid() && fresh.validate(true).isEmpty()) {
        if (fresh.calcDislikes() < origDislikes) {
            Api.submitSolution(problem.problemId, fresh)
        }
    } else {
        println("Not valid?")
    }

}
