package icfpc.trisolver

import icfpc.KnownProblems
import icfpc.api.Api
import icfpc.geom.*
import io.vavr.collection.TreeMap
import io.vavr.collection.TreeSet
import java.awt.geom.AffineTransform
import java.awt.geom.Point2D
import java.lang.Math.PI
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

// 107 109 111 116 120 122 124 (need half-squares) 125(unfold) 126(unfold) 128 130(hexes) 132
// dont work: 109 111
// work soso: 107 116 120 122 125 126 128
val sanProblem = 62
// optimized: 42 44 37 61


data class Step(val flipped: Boolean, val angle: Double) {
}

class AppendSolver(val problem: Problem) {

    val steps: ArrayList<Step> = ArrayList()

    init {
        val ANGLES = 64
        for (i in 0..ANGLES - 1) {
            this.steps.add(Step(false, (2 * PI / ANGLES) * i))
            this.steps.add(Step(true, (2 * PI / ANGLES) * i))
        }
    }

    fun run() {
        val islands = problem.figure.islands
        println("Islands size=${islands.size}")
        val doneIslands = HashSet<Int>()
        var placedEdges = HashSet<EdgeI>()
        var placedPoints = ArrayList<IntPoint?>(problem.figure.vertices.map { null })
        var placedPointsI = HashSet<Int>()
        var lastBestOffset = IntPoint(0, 0)
        var lastPs: PartialSolution? = null
        var lastSf: SolutionFigure? = null
        for (iter in 0..10000) {
            println("Placed edges: ${placedEdges}")
            val all = ArrayList<Pair<Int, Box>>()
            if (placedEdges.any { placedPoints[it.i1] == null || placedPoints[it.i2] == null }) {
                println("WTF");
            }
            for (island in islands.indices) {
                if (!doneIslands.contains(island)) {
                    val set = islands[island].toSet()
                    val subedges = problem.figure.edgeIndices.filter { set.contains(it.i1) && set.contains(it.i2) }
//                    println("Testing subedges: $subedges of island: ${islands[island]}");
                    if (placedEdges.isEmpty() || subedges.any { placedEdges.contains(it) }) {
                        val subs = problem.figure.subsetOfVertices(islands[island])
                        val bb = Hole.boundingBox(subs)
                        val p = Pair(island, bb)
                        all.add(p)
                    }
                }
            }
            println("Iter $iter / ${islands.size}, found islands: ${all.size}")
            if (all.size == 0) {
                if (doneIslands.size == islands.size) {
                    val sf = lastPs?.toSolutionFigure() ?: lastSf!!;
                    if (sf.validate(true).isEmpty() && sf.valid()) {
                        Api.submitSolution(problem.problemId, sf);
                    } else {
                        println("Invalid?")
                    }
                }
                abort("ALL=0, doneIslands.size=${doneIslands.size} totalIslands=${islands.size}");
            }

            var maxx = if (true) {
                Collections.max(all, { i1, i2 ->
                    i1.second.maxDim() - i2.second.maxDim()
                })
            } else {
                all[(Math.random() * 10000000).toInt() % all.size]
            }
            val optList = when {
                problem.problemId == 122 -> listOf(69, 28, 32, 31, 77, 19, 30, 25)
                problem.problemId == 65 -> listOf(20,11,3)
                else -> listOf()
            }
            if (true) {
                for (z in optList) {
                    val override = all.filter { it.first == z }
                    if (!override.isEmpty()) {
                        maxx = override.first()
                        break
                    }
                }
            }

            val pointsToFit = ArrayList(problem.figure.vertices)
            val set = HashSet(islands[maxx.first])

            doneIslands.add(maxx.first)

            val subedgesSmall = problem.figure.edgeIndices.filter { set.contains(it.i1) && set.contains(it.i2) }
            println("Chosen subedges island=${maxx.first} (box=${maxx.second.maxDim()}): $subedgesSmall ")

            var joinEdge: EdgeI? = null;
            if (!placedEdges.isEmpty()) {
                val singleEdge = placedEdges.intersect(subedgesSmall)
                if (singleEdge.size != 1) { // something should exist.
                    abort("Assertion failed")
                }
                joinEdge = singleEdge.first()
                if (placedPoints[joinEdge.i1] == null || placedPoints[joinEdge.i2] == null) {
                    placedPoints = placedPoints;
                }
            }

            set.addAll(placedPointsI)
            val subedges = problem.figure.edgeIndices.filter { set.contains(it.i1) && set.contains(it.i2) } // now all subedges

            var angleAttempt = 0;

            val MAXI = steps.size;
            for (step in steps.indices) {
                println("Step ${steps[step]}")
                if (joinEdge != null) {

                    placedPoints.forEachIndexed({ ix, p -> if (p != null) pointsToFit[ix] = p; else pointsToFit[ix] = null })
                    placePointsAcrossTheEdge(islands[maxx.first], pointsToFit, joinEdge, placedPoints, 1.0);
                    val test1 = ArrayList<IntPoint>(placedPoints.filter { it != null })
                    test1.addAll(subedgesSmall.map { pointsToFit[it.i1] })
                    test1.addAll(subedgesSmall.map { pointsToFit[it.i2] })
                    val box1 = Hole.boundingBox(test1)

                    placePointsAcrossTheEdge(islands[maxx.first], pointsToFit, joinEdge, placedPoints, -1.0);
                    val test2 = ArrayList<IntPoint>(placedPoints.filter { it != null })
                    test2.addAll(subedgesSmall.map { pointsToFit[it.i1] })
                    test2.addAll(subedgesSmall.map { pointsToFit[it.i2] })
                    val box2 = Hole.boundingBox(test2)
                    if (!steps[step].flipped) {
                        if (box1.maxDim() < box2.maxDim()) {
                            // restore to 1st case on this condition
                            placePointsAcrossTheEdge(islands[maxx.first], pointsToFit, joinEdge, placedPoints, 1.0);
                        }
                    } else {
                        if (box1.maxDim() >= box2.maxDim()) {
                            // restore to 1st case on reverse condition
                            placePointsAcrossTheEdge(islands[maxx.first], pointsToFit, joinEdge, placedPoints, 1.0);
                        }
                    }
                } else {
                    placedPoints.forEachIndexed({ ix, p -> if (!set.contains(ix)) pointsToFit[ix] = null })
                }
                val at = AffineTransform()
                at.rotate(steps[step].angle)

//                for (placedEdge in placedEdges) {
//                    println("edge len: ${placedEdge.i1} - ${placedEdge.i2} = "+pointsToFit[placedEdge.i1].distTo(pointsToFit[placedEdge.i2])+"  @@ ${pointsToFit[placedEdge.i1]}..${pointsToFit[placedEdge.i2]}")
//                }
                for (pf in pointsToFit.indices) {
                    if (pointsToFit[pf] != null) {
                        val dbl = pointsToFit[pf].toDouble();
                        var ndbl = at.transform(dbl, null);
                        pointsToFit[pf] = IntPoint(ndbl.x.roundToInt(), ndbl.y.roundToInt());
                    }
                }
//                for (placedEdge in placedEdges) {
//                    println("rot edge len: ${placedEdge.i1} - ${placedEdge.i2} = "+pointsToFit[placedEdge.i1].distTo(pointsToFit[placedEdge.i2])+" @@ ${pointsToFit[placedEdge.i1]}..${pointsToFit[placedEdge.i2]}")
//                }
                val myPointsBeforeFitting = set.map { pointsToFit[it] }
                val fitBox = problem.hole.box
                val figureBox = Hole.boundingBox(myPointsBeforeFitting.filter { it != null })
//                println("set=${set} myPoints=${myPointsBeforeFitting} box=${figureBox.maxDim()}");
                val initialOffsetX = fitBox.xMin - figureBox.xMin;
                val initialOffsetY = fitBox.yMin - figureBox.yMin;
                val allowDX = fitBox.width - figureBox.width;
                val allowDY = fitBox.height - figureBox.height;
                var bestOffset = IntPoint(-1000000, -1000000);
                var bestDislikes = 1000000000;
                for (tryX in 0..allowDX) {
                    for (tryY in 0..allowDY) {
                        val offset = IntPoint(tryX + initialOffsetX, tryY + initialOffsetY)
                        if (subedges.all { problem.checkNewEdge(pointsToFit[it.i1] + offset, pointsToFit[it.i2] + offset) }) {
                            var dislikes = 0
                            for (h in problem.hole.vertices) {
                                var minD = h.squaredDistTo(myPointsBeforeFitting[0] + offset)
                                for (v in myPointsBeforeFitting) {
                                    val nd = h.squaredDistTo(v + offset)
                                    if (minD > nd) {
                                        minD = nd
                                    }
                                }
                                dislikes += minD
                            }
                            if (dislikes < bestDislikes) {
                                bestOffset = offset;
                                bestDislikes = dislikes;
                                lastBestOffset = bestOffset;
                            }
                        }
                    }
                }
                var debugIt = false;
                if (bestOffset.x == -1000000) {
                    bestOffset = lastBestOffset;
                }
                if (debugIt || bestDislikes != 1000000000) {
                    //                view_(ps);
                    val savedPlacedEdges = HashSet(placedEdges);
                    val savedPlacedPoints = ArrayList(placedPoints);
                    placedEdges.addAll(subedgesSmall);
                    for (i in pointsToFit.indices) {
                        if (pointsToFit[i] != null) {
                            placedPoints[i] = pointsToFit[i] + bestOffset
                            placedPointsI.add(i);
                            pointsToFit[i] = placedPoints[i];
                        }
                    }
//                    for (placedEdge in placedEdges) {
//                        println("val edge len: ${placedEdge.i1} - ${placedEdge.i2} = "+pointsToFit[placedEdge.i1].distTo(pointsToFit[placedEdge.i2])+" @@ ${pointsToFit[placedEdge.i1]}..${pointsToFit[placedEdge.i2]}")
//                    }
                    lastPs = PartialSolution(problem,
                        placedPoints.foldIndexed(TreeMap.empty()) { ix, acc, ip -> if (ip != null) acc.put(ix, ip.toDouble()); else acc },
                        placedEdges.map { problem.figure.edgeIndices.indexOf(it) }.fold(TreeSet.empty(), { acc, v -> acc.add(v) }),
                        TreeSet.empty()
                    )
                    lastSf = null;
                    val solFigure = lastPs.toSolutionFigure()
                    val valid = solFigure.validate(false)
                    if (!valid.isEmpty()) {
                        println("Shaking...")
                        val fixed = shakeInvalids(solFigure)
                        if (fixed == null) {
                            println("Oops, can't improve, will attempt other approach")
                            if (step == MAXI - 1) {
                                System.exit(1);
                            } else {
                                placedEdges = savedPlacedEdges
                                placedPoints = savedPlacedPoints
                                continue
                            }
                        } else {
                            fixed.vertices.forEachIndexed({ i, p ->
                                if (placedPoints[i] != null) {
                                    placedPoints[i] = p;
                                }
                            })
                            lastPs = null;
                            lastSf = fixed;
                            println("Shaked well! continuing")
                        }
                    }
                    if (debugIt) {
                        view_(lastPs!!);
                    }
                } else {
                    println("Failed fit attempt, will continue");
                    if (step == MAXI - 1) {
                        abort("Rotate did not work, at iter=$iter/${islands.size}, island=${maxx.first}")
                    }
                    continue
                }
                println("Best score=$bestDislikes bestOffset=${bestOffset}")
                break;

            }

        }

    }

    class ShiftAction(val point: Int, val dx: Int, val dy: Int) {

    }


    fun shakeInvalids(inputFig: SolutionFigure): SolutionFigure? {
        var solFigure: SolutionFigure = inputFig;
        for (iter in 0..10000) {
            val invalid1 = solFigure.validate(false);
            if (invalid1.isEmpty())
                return solFigure;
            if (iter == 0) {
                println("Shake invalids...")
            }
            val points = HashSet<Int>()
            var bestError = 0.0;
            for (i in invalid1) {
                points.add(i.i1);
                points.add(i.i2);
                bestError = bestError + i.error;
            }
            val origVertices = ArrayList(solFigure.vertices)
            var improved = false;
            var breakAll = false;
            val execService = Executors.newCachedThreadPool()
            val l = System.currentTimeMillis()
            for (p1 in points) {
                for (dx in -3..3)
                    for (dy in -3..3) {
                        execService.submit(Runnable {
//                            println("shakeInvalids $p1 $dx $dy break=$breakAll ${System.currentTimeMillis() - l + 15000}")
                            if (!breakAll) {
                                if (System.currentTimeMillis() > l + 15000) {
                                    breakAll = true;
                                }
                                val testVertices = ArrayList(solFigure.vertices)
                                testVertices[p1] = origVertices[p1] + IntPoint(dx, dy);
                                for (p2 in points) {
                                    if (breakAll) break;
                                    for (dx2 in -3..3)
                                        for (dy2 in -3..3) {
                                            testVertices[p2] = origVertices[p2] + IntPoint(dx2, dy2);
                                            val newFigure = SolutionFigure(solFigure.problem, testVertices)
                                            val invalid = newFigure.validate(false);
                                            val totalError = invalid.foldRight(0.0, { e, acc -> acc + e.error })
                                            synchronized(execService) {
                                                if (totalError < bestError) {
//                                          println("Total error = $totalError, best was $bestError")
                                                    improved = true;
                                                    bestError = totalError;
                                                    solFigure = SolutionFigure(solFigure.problem, ArrayList(testVertices));
                                                    if (bestError == 0.0) {
                                                        breakAll = true
                                                    }
                                                }
                                            }
                                        }
                                    testVertices[p2] = origVertices[p2]
                                }
                            }
                        })
                    }
            }
            execService.shutdown()
            execService.awaitTermination(10000, TimeUnit.SECONDS)
            if (!improved) {
                break;
            } else {
                //println("Improved: $bestError...")
            }
        }
        return null;
    }

    private fun placePointsAcrossTheEdge(myPoints: List<Int>, dest: ArrayList<IntPoint>, base: EdgeI, fixedPoints: ArrayList<IntPoint?>, mirrorAngle: Double) {
        val p1 = fixedPoints[base.i1]!!
        val p2 = fixedPoints[base.i2]!!
        val op1 = problem.figure.vertices[base.i1]
        val op2 = problem.figure.vertices[base.i2]
        val origBaseAbsAngle = Math.atan2(op2.y.toDouble() - op1.y.toDouble(), op2.x.toDouble() - op1.x.toDouble())
        val currentBaseAbsAngle = Math.atan2(p2.y.toDouble() - p1.y.toDouble(), p2.x.toDouble() - p1.x.toDouble())
        val destP = Point2D.Double()
        for (mp in myPoints) {
            val op = problem.figure.vertices[mp]
            val origPointAbsAngle = Math.atan2(op.y.toDouble() - op1.y.toDouble(), op.x.toDouble() - op1.x.toDouble())
            val pointAngle = origPointAbsAngle - origBaseAbsAngle
            val pointDist = op.distTo(op1)
            val vec = Point2D.Double(pointDist, 0.0)
            val at = AffineTransform()
            at.rotate(pointAngle * mirrorAngle + currentBaseAbsAngle)
            at.transform(vec, destP)
            destP.x += p1.x;
            destP.y += p1.y;
            dest[mp] = IntPoint(destP.x.toInt(), destP.y.toInt());
        }
    }

    private fun abort(s: String) {
        System.err.println(s);
        System.exit(1);
    }


}

@Suppress("UNUSED_PARAMETER")
fun main(args: Array<String>) {
    val problemId = sanProblem
    val problem = KnownProblems.loadLocalProblem(problemId)
    AppendSolver(problem).run()

}
