package icfpc.vis

import icfpc.KnownProblems
import icfpc.geom.Figure
import icfpc.geom.Problem
import icfpc.geom.SolutionFigure
import icfpc.trisolver.sanProblem
import java.awt.BorderLayout
import java.awt.Toolkit
import javax.swing.JFrame
import javax.swing.SwingUtilities

fun viewProb_(prob: Problem) {
    val jf = JFrame("Test");
    jf.setExtendedState(jf.getExtendedState() or JFrame.MAXIMIZED_BOTH);
    jf.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
    jf.layout = BorderLayout()
    val vc = VisComponent();
    vc.setData(prob)
    jf.add(vc, BorderLayout.CENTER);
    jf.invalidate()
    jf.isVisible = true
}

fun main(args: Array<String>) {
    var problemId = 51
    val isSan = (System.getenv("USER") ?: "XXX").equals("san")
    if (isSan) {
        problemId = sanProblem

    }
    var prob = KnownProblems.loadLocalProblem(problemId)
    if (isSan) {
//        prob = Problem(prob.problemId, prob.hole, Figure(prob.figure.edgeIndices, prob.figure.shakedVertices(20)), prob.epsilon)
    }

    var solutionFigure = KnownProblems.getBestKnownSolutionFile(problemId)?.let {
        SolutionFigure.loadFromSolutionFile(prob, KnownProblems.getSolutionFileName(problemId, it.dislikes))
    }?: SolutionFigure.fromProblem(prob)
    solutionFigure?.validate(true)
    println("dislikes: ${solutionFigure?.dislikes}; score: ${solutionFigure?.score(697)}")
    val jf = JFrame("Test");
    if (isSan) {
//        solutionFigure = SolutionFigure.fromProblem(prob)
        jf.setExtendedState(jf.getExtendedState() or JFrame.MAXIMIZED_BOTH);
        jf.setSize(Toolkit.getDefaultToolkit().screenSize.width*90/100, Toolkit.getDefaultToolkit().screenSize.height*90/100)
        jf.setLocation(0, 0)
    } else {
        jf.setSize(800, 600)
    }
    jf.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    jf.layout = BorderLayout()
    val vc = VisComponent()
    if (isSan) {
//    val newprob = Problem(
//            prob.problemId,
//            prob.hole,
//            prob.figure.trianglesFigure(),
//            prob.epsilon
//    )
//        prob.figure.markedEdges = listOf(45, 46, 49, 50, 52, 55, 56, 57, 59, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 77, 78, 80, 81)
        val faces = prob.figure.faces()
        vc.setData(prob, null);
    } else {
        vc.setData(prob, solutionFigure);
    }
//    vc.setData(newprob, null);
//    vc.setData(prob)
    jf.add(vc, BorderLayout.CENTER);
    SwingUtilities.invokeLater {
        jf.isVisible = true
        jf.invalidate()
    }
//    KnownProblems.analyzeProblems()

}