package icfpc.vis

import icfpc.geom.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.geom.AffineTransform
import java.awt.geom.Line2D
import java.awt.geom.Path2D
import java.awt.geom.Point2D
import javax.swing.AbstractAction
import javax.swing.Action
import javax.swing.JComponent
import javax.swing.KeyStroke
import kotlin.math.roundToInt
import kotlin.random.Random

class VisComponent : JComponent() {
    var problem: Problem? = null
    var solution: SolutionFigure? = null
    var badEdges: List<EdgeI>? = null
    var pinnedVertices = ArrayList<Int>()
    var selectedVertIdx: Int = 0
    var scale = 1000.0
    companion object {
        val TRANSLATE_X = 40.0
        val TRANSLATE_Y = 40.0

    }
    init {
//        val vc = this
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("C"), "C pressed");
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("V"), "V pressed");
        this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("S"), "S pressed");

        this.getActionMap().put("C pressed", object: AbstractAction() {
           override fun actionPerformed(ae: ActionEvent) {
               val vc = ae.source
               if (vc !is VisComponent) return
               vc.selectedVertIdx = vc.findVertIdx(vc.mousePosition)
               println("selectedVertIdx = $selectedVertIdx")
           }
        })
        this.getActionMap().put("V pressed", object: AbstractAction() {
           override fun actionPerformed(ae: ActionEvent) {
               val vc = ae.source
               if (vc !is VisComponent) return
               val holeIdx = vc.findHoleIdx(vc.mousePosition)
               println("holeIdx = $holeIdx")
               vc.moveVertTo(selectedVertIdx, vc.problem!!.hole.vertices[holeIdx])
               vc.pinVert(selectedVertIdx)
               vc.repaint()
           }
        })
        this.getActionMap().put("S pressed", object: AbstractAction() {
           override fun actionPerformed(ae: ActionEvent) {
               val vc = ae.source
               if (vc !is VisComponent) return
               vc.solution!!.validate(true)

               println("Dislikes: ${vc.solution!!.dislikes}; valid: ${vc.solution!!.valid()}")
               println(vc.solution!!.toJson())
           }
        })

    }

    private fun pinVert(vi: Int) {
        pinnedVertices.add(vi)
    }

    private fun moveVertTo(vi: Int, pt: IntPoint) {
        if (pinnedVertices.isEmpty()) {
            // just translate.
            val vs = solution!!.vertices
            var newvs = ArrayList<IntPoint>()

            val dx = pt.x-vs[vi].x
            val dy = pt.y-vs[vi].y
            for (v in vs) {
                newvs.add(IntPoint(v.x + dx, v.y+dy))
            }
            solution!!.vertices = newvs
        } else if (pinnedVertices.size == 1) {
            // just rotate.
            val vs = solution!!.vertices
            var newvs = ArrayList<IntPoint>()
            val olddx = vs[pinnedVertices[0]].x-vs[vi].x
            val olddy = vs[pinnedVertices[0]].y-vs[vi].y
            val old_angle = Math.atan2(olddy.toDouble(), olddx.toDouble())
            val newdx = vs[pinnedVertices[0]].x-pt.x
            val newdy = vs[pinnedVertices[0]].y-pt.y
            val new_angle = Math.atan2(newdy.toDouble(), newdx.toDouble())
            val at = AffineTransform.getRotateInstance(new_angle-old_angle)
            for (v in vs) {
                val newpt = at.transform(Point2D.Double(v.x.toDouble(), v.y.toDouble()), null)
                newvs.add(IntPoint(newpt.x.toInt(), newpt.y.toInt()))
            }
            solution!!.vertices = newvs
            pinnedVertices.clear()
        }
    }
    private fun pixelToWorld(pos: Point): Point2D {
        val at = AffineTransform.getTranslateInstance(VisComponent.TRANSLATE_X, VisComponent.TRANSLATE_Y)
        at.concatenate(AffineTransform.getScaleInstance(scale, scale))
        return at.inverseTransform(Point2D.Double(pos.x.toDouble(), pos.y.toDouble()), null)!!
    }

    private fun findHoleIdx(mousePos: Point): Int {
        val pt = pixelToWorld(mousePos)
        var minD = pt.distance(problem!!.hole.vertices[0].x.toDouble(), problem!!.hole.vertices[0].y.toDouble())
        var idx: Int = 0
        for (i in 0..problem!!.hole.vertices.size-1) {
            val d = pt.distance(problem!!.hole.vertices[i].x.toDouble(), problem!!.hole.vertices[i].y.toDouble())
            if (minD > d) {
                minD = d
                idx = i
            }
        }
        return idx

    }

    private fun findVertIdx(mousePos: Point): Int {
        val pt = pixelToWorld(mousePos)
        var minD = pt.distance(solution!!.vertices[0].x.toDouble(), solution!!.vertices[0].y.toDouble())
        var idx: Int = 0
        for (i in 0..solution!!.vertices.size-1) {
            val d = pt.distance(solution!!.vertices[i].x.toDouble(), solution!!.vertices[i].y.toDouble())
            if (minD > d) {
                minD = d
                idx = i
            }
        }
        return idx
    }

    fun setData(problem: Problem, solution: SolutionFigure? = null) {
        this.problem = problem
        this.solution = solution
        this.badEdges = solution?.validate()
        repaint()
    }

    override fun paintComponent(g: Graphics?) {
        paintIntoGraphics(g!!, this.width, this.height)
    }

    fun drawArrow(g0: Graphics2D, x1: Double, y1: Double, x2: Double, y2: Double) {
        val g = g0.create() as Graphics2D
        val dx = x2 - x1
        val dy = y2 - y1
        val angle = Math.atan2(dy, dx)
        val len = Math.sqrt(dx * dx + dy * dy)
        val at = AffineTransform.getTranslateInstance(x1, y1)
        at.concatenate(AffineTransform.getRotateInstance(angle))
        g.transform(at)

        // Draw horizontal arrow starting in (0, 0)
        g.draw(Line2D.Double(0.0, 0.0, len, 0.0))
        val ARR_SIZE = 1.5
        val p = Path2D.Double();
        p.moveTo(len, 0.0);
        p.lineTo(len - ARR_SIZE, -ARR_SIZE);
        p.lineTo(len - ARR_SIZE, ARR_SIZE);
        p.lineTo(len, 0.0);
        p.closePath()
        g.fill(p);
        //g.fillPolygon(intArrayOf(len.toInt(), len.toInt() - ARR_SIZE, len.toInt() - ARR_SIZE, len.toInt()), intArrayOf(0, -ARR_SIZE, ARR_SIZE, 0), 4)
        g.dispose()
    }

    fun paintIntoGraphics(g: Graphics, tw: Int, th: Int) {
        g.color = Color.white;
        g.fillRect(0, 0, tw, th)
        val h = th.toDouble() * 0.9
        val w = tw.toDouble() * 0.9
        val lp = problem ?: return

        val hole = lp.hole
        val dx = (hole.box.width).toDouble()
        val dy = (hole.box.height).toDouble()
        scale = Math.min(scale, Math.min(w / dx, h / dy))
        var verts = solution?.vertices
        if (verts == null) {
            verts = lp.figure.vertices
        }
        val fiBox = Hole.boundingBox(verts)
        val vdx = (fiBox.width).toDouble()
        val vdy = (fiBox.height).toDouble()
        scale = Math.min(scale, Math.min(w / vdx, h / vdy))


        val g2 = (g as Graphics2D).create() as Graphics2D
        g2.translate(VisComponent.TRANSLATE_X, VisComponent.TRANSLATE_Y)
        g2.scale(scale, scale);

        g2.setStroke(BasicStroke(2f));
        val hove = hole.vertices
        val nvert = hove.size
        for (p in 0 until nvert) {
            val line = Line2D.Double(
                hove[p].x.toDouble(), hove[p].y.toDouble(),
                hove[(p + 1) % (nvert)].x.toDouble(),
                hove[(p + 1) % (nvert)].y.toDouble()
            )
            g2.color = Color.black;
            g2.draw(line)
            g2.font = g.font.deriveFont(30f/scale.toFloat())
            g2.color = Color.red;
            g2.drawString("$p", ((line.x1 + line.x2) / 2).roundToInt(), ((line.y1 + line.y2) / 2).roundToInt())
        }

        val edg = lp.figure.edgeIndices
        for (p in edg.indices) {
            val p1 = verts[edg[p].i1]
            val p2 = verts[edg[p].i2]
            if (lp.hole.contains(p1) && lp.hole.contains(p2))
                g2.color = Color.green
            else
                g2.color = Color.magenta
            val line = Line2D.Double(
                p1.x.toDouble(), p1.y.toDouble(),
                p2.x.toDouble(), p2.y.toDouble(),
            )
            if (line.x1 <= -1000000 ) continue;
            if (line.x2 <= -1000000 ) continue;
            g2.color = Color.green.darker();
            if (lp.figure.markedEdges.contains(p)) {
                g2.setStroke(BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0.0f, floatArrayOf(1.0f), 0.0f));
            } else {
                g2.setStroke(BasicStroke(1f));
            }
            drawArrow(g2, line.x1,  line.y1, line.x2, line.y2);
            g2.font = g.font.deriveFont(20f/scale.toFloat())
            g2.color = Color.magenta;
            g2.drawString("$p", ((line.x1 + line.x2) / 2).roundToInt(), ((line.y1 + line.y2) / 2).roundToInt())
        }

        val locBadEdges = badEdges
        if (locBadEdges != null) {
            g2.color = Color.red;
            for (e in locBadEdges) {
                g2.draw(
                    Line2D.Double(
                        verts[e.i1].x.toDouble(),
                        verts[e.i1].y.toDouble(),
                        verts[e.i2].x.toDouble(),
                        verts[e.i2].y.toDouble(),
                    )
                )
            }

        }

        g2.font = g.font.deriveFont(50f/scale.toFloat())
        for (i in verts.indices) {
            val p = verts[i]
            if (p.x <= -1000000) continue;
            if (hole.contains(p)){
                g2.color = Color.blue
            } else {
                g2.color = Color.red
            }
            g2.drawString("$i", p.x, p.y)
        }

        if (lp.figure.markedEdges.isEmpty() && false) {
            g2.setStroke(BasicStroke(1f));
            for(i in lp.figure.islands) {
                g2.setStroke(BasicStroke(0.5f));
                g2.color = Color(Random.nextInt()%128 + 128, Random.nextInt()%128 + 128, Random.nextInt()%128 + 128)
                for(p in i.indices) {
                    for(q in i.indices) {
                        if (q > p) {
                            for(et in edg) {
                                if ((et.i1 == i[p] && et.i2 == i[q])|| (et.i1 ==i[q] && et.i2 == i[p])) {
                                    val p1 = verts[i[p]]
                                    val p2 = verts[i[q]]
                                    g2.drawLine(p1.x, p1.y, p2.x, p2.y);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        g2.dispose()
    }
}