![Thirteen logo](./thirteen_logo.png)

This is THIRTEEN's take on the ICFP Contest 2021 aka ICFPC 2021 https://icfpcontest2021.github.io/

Most of the code is in Kotlin and is expected to be run with the JVM **11** from the [Adopt Open JDK](https://adoptopenjdk.net/). 
You should be able to open the main project with the IntelliJ IDEA 2021.1 Community Edition, available for free at https://www.jetbrains.com/idea/download/other.html.

The original repository is hosted by GitLab at https://gitlab.com/icfpc_thirteen/icfpc2021

The project was made open-source under the [MIT license](LICENSE) after the end of the contest. The license for the specs in the [specs](specs) folder is unclear but originally they were publicly available at [https://icfpcontest2021.github.io/specification.html](https://icfpcontest2021.github.io/specification.html) and at the corresponding [Git repository](https://github.com/icfpcontest2021/icfpcontest2021.github.io).
The state of the project as of the end of the contest is tagged with the `end_of_contest` tag in the Git.


# Write-ups

## SergGr

This year was a total failure for us. One thing is that some regular members found out the schedule too late and had some other plans for that weekend. The rest of us failed to gather in one place in any significant numbers, so it was an almost 100% distributed contest which is always a bad sign for us.

The biggest mistake on our side during the contest was that we didn't produce any GUI-manual-solver at all despite the fact that about 90% of our Lightning round points had been earned by manually solving some 0-problems (i.e. the ones which we know had the best dislikes of 0) by hand first on the paper and then with a little support from our Visualizer. Turning the Visualizer into a simple manual solver GUI would not be hard and was discussed several times but somehow we failed to act on that.

We created a few solvers as TriSolver, FlipSolver, and ZeroSolver but none of them was really successful in solving arbitrary problems well. Probably the most successful of the 3 was the ZeroSolver that during the day 2 was able to re-create most of our hand-made solutions for the 0-problems and also find a few more perfect 0-solutions for a bit bigger problems as well as creating a few reasonable solutions for some "small" problems. 
None of our solvers worked well on the big problems. 
During the late day 2 and day 3 I created a few "improvers" that tried to improve some existing solutions by some small movements such as move the whole figure 1 pixel, move some vertex 1 pixel, rotate the whole figure and finally try to move the "legs" and "ears" into the direction of some hole vertex. They did improve some solutions a bit but mostly failed to provide any significant improvements of our woefully bad initial solutions. 
Again with a manual solver to produce some better initial solutions the story probably could have been really different. 