import sys
import os
import time
from twill.commands import *
from bs4 import BeautifulSoup
import urllib.request

def send_req(url):
  req = urllib.request.Request(url)
  req.add_header('Authorization', 'Bearer 6f527204-8480-40ec-afad-119b4d9660b0')
  with urllib.request.urlopen(req) as response:
    return response.read()

def get_solution(problem_id, pose_id, dislikes):
  out_file_path = 'solutions/%s_%s_%s.json' % (pose_id, problem_id, dislikes)
  if os.path.exists(out_file_path):
    return
  url = 'https://poses.live/solutions/%s/download' % pose_id
  try:
    go(url)
    data = show()
    with open(out_file_path, 'wb') as out_file:
      out_file.write(data.encode())
    time.sleep(5.0)
  except:
    pass  # eat all parse errors

def download_problem(i):
  go('https://poses.live/problems/%s' % i)
  soup = BeautifulSoup(show(), 'html.parser')
  #import pdb; pdb.set_trace()
  best_pose_id = None
  min_dislikes = None
  for tr in soup.find_all('table')[0].find_all('tr')[1:]:
    # '/solutions/7fbe46ce-85d2-43bd-99ac-3ce283c9d851'
    pose_id = tr.td.a['href'][11:]
    dislikes = tr.find_all('td')[-1].text.replace(u'❌', u'x').replace(u'⏳', 'timeout')
    # get_solution(i, pose_id, dislikes)
    if dislikes != u'x' and dislikes != u'timeout':
      if best_pose_id is None or int(dislikes) < min_dislikes:
        min_dislikes = int(dislikes)
        best_pose_id = pose_id
  if best_pose_id is not None:
    get_solution(i, best_pose_id, min_dislikes)

def main(email, password):
  go('https://poses.live/login')
  showforms()
  formclear('1')
  fv('1', 'login.email', email)
  fv('1', 'login.password', password)
  showforms()
  submit('3')
  for i in range(1, 133):
    download_problem(i)


if __name__ == '__main__':
  main(sys.argv[1], sys.argv[2])
