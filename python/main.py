import copy
import json
import math
import sys
import random

import pygame
import pymunk
import pymunk.pygame_util

pygame.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
clock = pygame.time.Clock()
space = pymunk.Space()

SCALE = None

pygame.font.init()
font = pygame.font.SysFont('Arial', 12)

def v_to_screen(v):
  return v[0] * SCALE, v[1] * SCALE

def screen_to_v(v):
  return v[0] / SCALE, v[1] / SCALE

def d_to_screen(d):
  return d * SCALE

def sign(x):
  if x > 0:
    return 1
  if x == 0:
    return 0
  return -1

def draw(prob):
  screen.fill((200, 200, 200))
  draw_options = pymunk.pygame_util.DrawOptions(screen)
  space.debug_draw(draw_options)
  dislikes = prob.figure.dislikes(prob.hole)
  screen.blit(font.render('Dislikes: %s' % dislikes, False, (0, 0, 0)), (0, 0))

def dist(v1, v2):
  return math.sqrt((v1[0] - v2[0]) ** 2 + (v1[1] - v2[1]) ** 2)

def squardIntDist(v1, v2):
  return (int(v1[0]) - int(v2[0])) ** 2 + (int(v1[1]) - int(v2[1])) ** 2

class EdgeP(object):
  def __init__(self, startPoint, endPoint):
    self.startPoint = startPoint
    self.endPoint = endPoint
    self.vertical = False
    if (endPoint[0] - startPoint[0] != 0):
      self.a = (endPoint[1] - startPoint[1]) / (endPoint[0] - startPoint[0])
      self.b = startPoint[1] - self.a * startPoint[0]
    else:
      self.vertical = True
    self.horizontal = startPoint[1] == endPoint[1]

  def intersectsWith(self, o, includeEndPoints = True):
    if not self.vertical and not o.vertical:
      # check if both vectors are parallel. If they are parallel and don't coinside then no intersection point will exist
      if (self.a - o.a == 0.0):
        if (self.b - o.b == 0.0):
          return self.isInside(o.startPoint[0], o.startPoint[1], includeEndPoints) or self.isInside(o.endPoint[0], o.endPoint[1], includeEndPoints)
        return False
        x = ((o.b - self.b) / (self.a - o.a))
        y = o.a * x + o.b
      elif (self.vertical and not o.vertical):
        x = self.startPoint[0]
        y = o.a * x + o.b
      elif (not self.vertical and o.vertical):
        x = o.startPoint[0]
        y = self.a * x + self.b
      else:
        return False

      if (o.isInside(x, y, includeEndPoints) and self.isInside(x, y, includeEndPoints)):
        return True
    return False

  # def isInside(self, p, includeEndPoints = true):
  #   on = (p[1] - startPoint[1]) == a * (p[0] - startPoint[0])
  #   if vertical:
  #     on = p[0] == startPoint[0]
  #   if not on:
  #     return False
  #   xMin = min(startPoint[0], endPoint[0])
  #   xMax = max(startPoint[0], endPoint[0])
  #   yMin = min(startPoint[1], endPoint[1])
  #   yMax = max(startPoint[1], endPoint[1])
  #   if (includeEndPoints):
  #     return xMin <= p[0] and p[0] <= xMax and yMin <= p[1] and p[1] <= yMax

  #   else:
  #     return xMin < p[0] and p[0] < xMax and yMin < p[1] and p[1] < yMax

  # def isInside(p, includeEndPoints = True):
  #   return isInside(p[0], p[1], includeEndPoints)

  def isInside(self, px, py, includeEndPoints = True):
    eps = 1e-4
    on = abs((py - self.startPoint[1]) - self.a * (px - self.startPoint[0])) < eps
    if self.vertical:
      on = abs(px - self.startPoint[0]) < eps
    if not on:
      return false
    isStart = abs(self.startPoint[0] - px) < eps and abs(self.startPoint[1] - py) < eps
    isEnd = abs(self.endPoint[0] - px) < eps and abs(self.endPoint[1] - py) < eps
    if isStart or isEnd:
      return includeEndPoints

    xMin = min(self.startPoint[0], self.endPoint[0])
    xMax = max(self.startPoint[0], self.endPoint[0])
    yMin = min(self.startPoint[1], self.endPoint[1])
    yMax = max(self.startPoint[1], self.endPoint[1])
    if (includeEndPoints or self.vertical):
      if (not(xMin <= px and px <= xMax)):
        return False
    else:
      if (not(xMin < px and px < xMax)):
        return False

    if (includeEndPoints or self.horizontal):
      return yMin <= py and py <= yMax
    else:
      return yMin < py and py < yMax

class Figure(object):
  def __init__(self, vertices, edgeIndices, epsilon):
    self.vertices = vertices
    self.original_vertices = copy.copy(vertices)
    self.edgeIndices = edgeIndices
    self.epsilon = epsilon
    self.vshapes = []
    self.joints = {}
    self.xmin = min([v[0] for v in vertices])
    self.ymin = min([v[1] for v in vertices])
    self.xmax = max([v[0] for v in vertices])
    self.ymax = max([v[1] for v in vertices])

    self.exitsToPoints = []
    for v in vertices:
      self.exitsToPoints.append([])
    for i1, i2 in edgeIndices:
      self.exitsToPoints[i1].append(i2)
      self.exitsToPoints[i2].append(i1)

  def add_bodies(self):
    for x, y in self.vertices:
      mass = 3
      radius = 1
      body = pymunk.Body()  # 1
      body.position = v_to_screen((x, y))  # 2
      shape = pymunk.Circle(body, radius)  # 3
      shape.mass = mass  # 4
      shape.friction = 1
      shape.sensor = True
      space.add(body, shape)
      self.vshapes.append(shape)

    for i1, i2 in self.edgeIndices:
      edge_dist = dist(self.vertices[i1], self.vertices[i2])
      edge_dist = d_to_screen(edge_dist)
      #joint = pymunk.SlideJoint(self.vshapes[i1].body, self.vshapes[i2].body, (0,0), (0,0), edge_dist, edge_dist) # 2
      #joint = pymunk.DampedSpring(self.vshapes[i1].body, self.vshapes[i2].body, (0,0), (0,0), edge_dist, 1, 1) # 2
      joint = pymunk.PinJoint(self.vshapes[i1].body, self.vshapes[i2].body) # 2
      self.joints[tuple(sorted([i1, i2]))] = joint
      space.add(joint)

  def find_vi(self, p):
    besti = 0
    bestd = dist(self.vshapes[0].body.position, p)
    for i in range(1, len(self.vertices)):
      newd = dist(self.vshapes[i].body.position, p)
      if newd < bestd:
        besti = i
        bestd = newd
    return besti

  def pin_vi(self, besti):
    for ei in self.exitsToPoints[besti]:
      j = self.joints[tuple(sorted([besti, ei]))]
      if j is None:
        continue
      space.remove(j)
      self.joints[tuple(sorted([besti, ei]))] = None
      joint = pymunk.PinJoint(space.static_body,
                              self.vshapes[ei].body,
                              self.vshapes[besti].body.position, (0,0))
      space.add(joint)

  def pin(self, p):
    besti = self.find_vi(p)
    return self.pin_vi(besti)

  def pin_in_hole(self, hole):
    vertices = self.get_int_vertices()

    for i in range(len(vertices)):
      if hole.contains(vertices[i]):
        self.pin_vi(i)


  def move_v(self, vi, p):
    self.vshapes[vi].body.position = p

  def dislikes(self, hole):
    if not self.valid(hole):
      return -1
    sumD = 0
    vertices = self.get_int_vertices()
    for h in hole.vertices:
      minD = squardIntDist(h, vertices[0])
      for v in vertices[1:]:
        if minD > squardIntDist(h, v):
          minD = squardIntDist(h, v)
      sumD += minD
    return sumD

  def valid(self, hole):
    return self.fitsIntoHole(hole) and self.validate()

  def validate(self):
    allowed = self.epsilon / 1000000.0
    vertices = self.get_int_vertices()

    for i1, i2 in self.edgeIndices:
      op1 = self.original_vertices[i1]
      op2 = self.original_vertices[i2]
      od = squardIntDist(op1, op2)
      np1 = vertices[i1]
      np2 = vertices[i2]
      nd = squardIntDist(np1, np2)
      sdiff = nd - od
      diff = abs(sdiff)
      if (diff / od > allowed):
        return False
    return True

  def fitsIntoHole(self, hole):
    vertices = self.get_int_vertices()
    for i1, i2 in self.edgeIndices:
      p1 = vertices[i1]
      p2 = vertices[i2]
      if not hole.checkNewEdge(p1, p2):
        return False
    return True

  def get_int_vertices(self):
    return [[int(x) for x in screen_to_v(shape.body.position)] for shape in self.vshapes]

  def dump_solution(self):
    vertices = self.get_int_vertices()
    return json.dumps({'vertices': vertices})

class Hole(object):
  def __init__(self, vertices):
    self.vertices = vertices
    self.xmin = min([v[0] for v in vertices])
    self.ymin = min([v[1] for v in vertices])
    self.xmax = max([v[0] for v in vertices])
    self.ymax = max([v[1] for v in vertices])

  def add_bodies(self):
    body = pymunk.Body(body_type = pymunk.Body.STATIC) # 1
    body.position = (0, 0)
    space.add(body)
    for i in range(len(self.vertices)):
      l = pymunk.Segment(body,
                         v_to_screen(self.vertices[i]),
                         v_to_screen(self.vertices[(i+1)%len(self.vertices)]), 1) # 2
      l.friction = 1
      space.add(l)

  def checkNewEdge(self, p1, p2):
    if (not self.contains(p1) or not self.contains(p2)):
      return False
    problemEdge = EdgeP(p1, p2)
    for i in range(len(self.vertices)):
      nexti = (i + 1) % len(self.vertices)
      problemWall = EdgeP(self.vertices[i], self.vertices[nexti])
      # this is an important special case when we exactly match the hole wall
      if ((self.vertices[i] == p1 and self.vertices[nexti] == p2) or
          (self.vertices[i] == p2 and self.vertices[nexti] == p1)):
        return True
      if (problemEdge.intersectsWith(problemWall, includeEndPoints = False)):
        return False

    # after the loop that can exit with true if we exactly match the hole edge
    # ugly hack but should work to cover the case when both ends on the edges of the
    # hole but the segment is fully outside
    if (not self.contains(((p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2))):
      return False

    return True

  def contains(self, p):
    s = self.checkPoints(self.vertices[0], self.vertices[len(self.vertices) - 1], p)
    for i in range(len(self.vertices)-1):
      s *= self.checkPoints(self.vertices[i], self.vertices[i + 1], p)
    return s <= 0

  def checkPoints(self, a, b, middle):
    ax = a[0] - middle[0]
    ay = a[1] - middle[1]
    bx = b[0] - middle[0]
    by = b[1] - middle[1]
    s = sign(ax * by - ay * bx)
    if (s == 0 and (ay == 0 or by == 0) and ax * bx <= 0):
      return 0
    if ((ay < 0) != (by < 0)):
      if (by < 0):
        return s
      return -s
    return 1

class Problem(object):
  def __init__(self, fname, solfname=None):
    source = json.load(open(fname))
    self.hole = Hole(source['hole'])
    global SCALE
    SCALE = int(SCREEN_HEIGHT / (self.hole.ymax + 5))
    self.figure = Figure(source['figure']['vertices'], source['figure']['edges'], source['epsilon'])
    if solfname:
      self.figure.vertices = json.load(open(solfname))['vertices']
    for x in [self.hole, self.figure]:
      x.add_bodies()


def main(fname, solfname=None):
  running = True
  drag_vi = None
  prob = Problem(fname, solfname)
  while running:
      # Input handling
      for event in pygame.event.get():
          if event.type == pygame.QUIT:
            running = False
          elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            running = False
          elif event.type == pygame.KEYDOWN and event.key == pygame.K_f:
            prob.figure.pin(pygame.mouse.get_pos())
          elif event.type == pygame.KEYDOWN and event.key == pygame.K_h:
            prob.figure.pin_in_hole(prob.hole)

          elif event.type == pygame.KEYDOWN and event.key == pygame.K_s:
            print("Dislikes:", prob.figure.dislikes(prob.hole))
            print(prob.figure.dump_solution())
          elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
              drag_vi = prob.figure.find_vi(event.pos)
          elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:
                drag_vi = None
          elif event.type == pygame.MOUSEMOTION:
              if drag_vi is not None:
                  prob.figure.move_v(drag_vi, event.pos)


      draw(prob)
      space.step(1/50.0)
      pygame.display.flip()
      clock.tick(50)

if __name__ == '__main__':
  main(*sys.argv[1:])
